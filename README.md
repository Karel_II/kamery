
## Install


### Apache + lets encrypt

zypper install apache certbot python-certbot python-certbot-apache

a2enmod proxy proxy_http php7 rewrite header access_compat proxy_http

certbot certonly --agree-tos --expand --authenticator webroot --installer apache -d "localhost@localdomain.cz" --webroot-path /srv/www/letsencrypt [--dry-run]


### PHP7


1. zypper install php7 php7-openssl php7-phar php7-zlib
2. zypper install php7-ctype php7-dom php7-gd php7-iconv php7-json php7-pdo php7-sqlite php7-tokenizer php7-xmlreader php7-xmlwriter php7-fileinfo php7-intl



### MRTG

rpm -i /root/mrtg-2.17.7-lp151.2.1.x86_64.rpm

### Install Composer

php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('sha384', 'composer-setup.php') === '48e3236262b34d30969dca3c37281b3b4bbe3221bda826ac6a9a62d6444cdb0dcd0615698a5cbe587c3f0fe57a54d8f5') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php --install-dir=/bin
php -r "unlink('composer-setup.php');"



## Configuration

### config.local.neon

    parameters:

        apache2:
            ServerName: "localhost.localdomain"
            ServerAlias: "remotehost"
            ServerAdmin: "localhost.localdomain"
            SSL: TRUE
            WebServer: TRUE
            ServerStatus: "/server-status.info"   

        letsEncrypt:
            DocumentRoot: "%DocumentRoot%/letsencrypt"
            ConfigDir: "/etc/certbot"
    
        cron:
            Name: "localhost"

        mrtg:
            XSize: 600
            YSize: 150
            Language: czech
            BinFile: "env LANG=C /usr/bin/mrtg"
        
        archive:
            expiration: 3 days

    legalNoticeExtension:
        expiration: '14 days'
        autoAccept: false

    googleAnalyticsExtension:
        serviceId: 'UA-XXXXX-Y'
    
    database:
        default:
            dsn: "sqlite:%appDir%/sqlLite/database.sqlite"
            user:
            password:
        archive:
            dsn: "sqlite:%appDir%/sqlLite/archive.sqlite"
            user:
            password:


### generate configs

1. php %DocumentRoot%/%InstallDir%/www/index.php Cli:Setup:apache2 --file --restart-service
2. php %DocumentRoot%/%InstallDir%/www/index.php Cli:Setup:mrtgMeteoStations --file --restart-service
3. php %DocumentRoot%/%InstallDir%/www/index.php Cli:Setup:mrtgVideoCameras  --file --restart-service
4. php %DocumentRoot%/%InstallDir%/www/index.php Cli:Setup:cron  --file --restart-service
