<?php

namespace App\Presenters;

use Nette,
    App\Control,
    App\Model;

/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter {

    /**
     * @var string 
     * @persistent 
     */
    public $lang = 'cs-CZ';

    /**
     * @var \App\Model\Repository\MenuRepository 
     * @inject
     */
    public $menuRepository;

    /**
     * @var \LegalNotice\Application\UI\ILegalNoticeControlFactory 
     * @inject
     */
    public $legalNoticeControl;

    /**
     * @var \NetteAddons\GoogleServices\Application\UI\IAnalyticsControlFactory 
     * @inject
     */
    public $googleAnalytics;

    /**
     * @var \LiveTranslator\Application\UI\ITracyControlFactory 
     * @inject
     */
    public $liveTranslatorTracy;

    /**
     *
     * @var \App\Model\Repository\CommitRepositoryLite
     * @inject  
     */
    public $commitRepositoryLite;

    /**
     * @var \LiveTranslator\Translator 
     * @inject 
     */
    public $translator;

    /**
     *
     * @var \NetteAddons\Repository\VisitorsRepository
     * @inject 
     */
    public $visitorsRepository = null;

    /**
     * CONROLS
     */
    protected function createComponentHeaderMenu() {
        $control = new \NetteBootstapMenu\Control\MenuHeaderControl();
        $control->setCollapseClass("navbar-collapse");
        $control->setTranslator($this->translator);
        return $control;
    }

    protected function createComponentBreadCrumbs() {
        $control = new \NetteBootstapMenu\Control\BreadCrumbsControl();
        $control->setTranslator($this->translator);
        return $control;
    }

    protected function createComponentFooterMenu() {
        $control = new \NetteBootstapMenu\Control\MenuFooterControl();
        $control->setTranslator($this->translator);
        return $control;
    }

    protected function createComponentMenu() {
        $control = new \NetteBootstapMenu\Control\MenuControl();
        $control->setCollapseClass("navbar-collapse");
        $control->setTranslator($this->translator);
        return $control;
    }

    protected function createComponentMeta() {
        $control = new \NetteBootstapMenu\Control\MetaRefreshControl();
        $control->setTranslator($this->translator);
        return $control;
    }

    protected function createComponentPageControl() {
        $control = new Control\PageControl();
        $control->setTranslator($this->translator);
        return $control;
    }

    protected function createComponentLegalNotice() {
        return $this->legalNoticeControl->create();
    }

    protected function createComponentGoogleAnalytics() {
        return $this->googleAnalytics->create();
    }

    protected function createComponentLiveTranslatorTracy() {
        return $this->liveTranslatorTracy->create('lang');
    }

    /**
     * GET CONROLS
     */
    protected function getComponentPageControl() {
        return $this->getComponent('pageControl');
    }

    protected function beforeRender() {
        $this->template->menu = $this->menuRepository->getMenu();

        $this->template->headerMenuBrand = $this->menuRepository->getHeaderMenuBrand();


        $this->template->menuHorizontalLeft = $this->menuRepository->getHorizontalLeftMenu();
        $this->template->menuHorizontalSearch = $this->menuRepository->getHorizontalSearchMenu();
        $this->template->menuHorizontalRight = $this->menuRepository->getHorizontalRightMenu();
        $this->template->menuFooter = $this->menuRepository->getFooterMenu();

        if (($count = $this->commitRepositoryLite->getInserted()) > 0) {
            $this->template->menuHorizontalRight[5]->setBadgeText('!');
            $this->template->menuHorizontalRight[54]->setShowMenu(TRUE);
            $this->template->menuHorizontalRight[55]->setShowMenu(TRUE);
            $this->template->menuHorizontalRight[55]->setName('Waiting for Commit')
                    ->setClass('bg-success')
                    ->setBadgeText((string) $count);
            $this->template->menuHorizontalRight[56]->setShowMenu(TRUE);
        }
        if (($count = $this->commitRepositoryLite->getCommitted()) > 0) {
            $this->template->menuHorizontalRight[5]->setBadgeText('!');
            $this->template->menuHorizontalRight[54]->setShowMenu(TRUE);
            $this->template->menuHorizontalRight[55]->setShowMenu(TRUE);
            $this->template->menuHorizontalRight[55]->setName('Waiting for Write')
                    ->setClass('bg-danger')
                    ->setBadgeText((string) $count);
        }
        $this->visitorsRepository->insertVisitor();
        return parent::beforeRender();
    }

    protected function startup() {
        $this->translator->setCurrentLanguage($this->lang);
        if (PHP_SAPI == 'cli') {
            \Tracy\Debugger::dump('View as web page!');
            $this->terminate();
        }
        return parent::startup();
    }

    public function handleCommit() {
        $this->commitRepositoryLite->setCommit();
        $this->redirect('this');
    }

    protected function createTemplate($class = NULL) {
        $template = parent::createTemplate($class);
        $template->setTranslator($this->translator);
        return $template;
    }

    // to have translated even forms add this method too
    protected function createComponent($name) {
        $component = parent::createComponent($name);
        if ($component instanceof \Nette\Forms\Form) {
            $component->setTranslator($this->translator);
        }
        return $component;
    }

}
