<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Presenters;

use App\Forms\VideoCameraFormFactory;
use App\Model\Objects\VideoCamera,
    App\Model\Objects\VideoCameras;
use App\Model\Storage\ImageStorage;
use Nette\Application\Responses\FileResponse;
use App\Model\Objects\ArchiveRecord;
use Nette\Utils\DateTime;

/**
 * Description of VideoCamerasPresenter
 *
 * @author Karel
 */
class VideoCamerasPresenter extends BasePresenter {

    /**
     * @var VideoCameraFormFactory 
     * @inject
     */
    public $videoCameraFormFactory;

    /**
     * @inject
     * @var \App\Model\Repository\VideoCamerasRepositoryLite 
     */
    public $videoCamerasRepositoryLite;

    /**
     *
     * @var ImageStorage
     * @inject  
     */
    public $imageStorage;

    /**
     * @return Nette\Application\UI\Form
     */
    protected function createComponentVideoCameraForm() {
        $form = $this->videoCameraFormFactory->create();
        $form->onSuccess[] = function ($form) {
            $form->getPresenter()->redirect('default');
        };
        return $form;
    }

    public function actionImageThumbnail($id = null, $time = null) {
        $videoCamera = VideoCamera::newObjectId($id);
        if ($this->videoCamerasRepositoryLite->fetchVideoCamera($videoCamera) && ($videoCamera->private === false || $this->user->isAllowed('videoCameras', 'private'))) {
            $image = $this->imageStorage->getThumbFile($videoCamera->name);
            $response = new FileResponse($image, null, 'image/jpg', false);
            $this->sendResponse($response);
        } else {
            $this->error();
        }
    }

    public function actionImageFull($id = null, $time = null) {
        $videoCamera = VideoCamera::newObjectId($id);
        if ($this->videoCamerasRepositoryLite->fetchVideoCamera($videoCamera) && ($videoCamera->private === false || $this->user->isAllowed('videoCameras', 'private'))) {
            $image = $this->imageStorage->getFullFile($videoCamera->name);
            $response = new FileResponse($image, null, 'image/jpg', false);
            $this->sendResponse($response);
        } else {
            $this->error();
        }
    }

    public function actionImageArchive($id = null, $time = null) {
        $videoCamera = VideoCamera::newObjectId($id);
        if ($this->videoCamerasRepositoryLite->fetchVideoCamera($videoCamera) && ($videoCamera->private === false || $this->user->isAllowed('videoCameras', 'private'))) {
            $archiveRecord = ArchiveRecord::newArchiveRecord($videoCamera, DateTime::from($time));
            $image = $this->imageStorage->getArchiveFile($videoCamera->name, $archiveRecord);
            $response = new FileResponse($image, null, 'image/jpg', false);
            $this->sendResponse($response);
        } else {
            $this->error();
        }
    }

    /**
     * @todo Remove this stuff
     * 
     */
    public function actionVideo($name = null, $time = null) {
        $this->redirect('Homepage:default');
    }

    /**
     * @todo Remove this stuff
     * 
     */
    public function actionVideoSecure($name = null, $time = null) {
        $this->redirect('Homepage:default');
    }

    public function renderDefault() {
        if ($this->user->isAllowed('videoCameras', 'private')) {
            $videoCameras = $this->videoCamerasRepositoryLite->getVideoCameras();
        } elseif ($this->user->isAllowed('videoCameras', 'public')) {
            $videoCameras = $this->videoCamerasRepositoryLite->getVideoCameras(FALSE);
        } else {
            $videoCameras = new VideoCameras();
        }
        $this->template->videoCameras = $videoCameras;
    }

    public function renderEdit($id = null) {
        if (!$this->user->isAllowed('videoCameras', 'edit')) {
            $this->redirect('Homepage:default');
        }
        if (isset($id)) {
            $videoCamera = VideoCamera::newObjectId($id);
            $this->videoCameraFormFactory->setDefaults($videoCamera);
            $this->videoCameraFormFactory->setEdit(true);
        }
    }

}
