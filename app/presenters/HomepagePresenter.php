<?php

namespace App\Presenters;

use App\Model\Objects\VideoCameras,
    App\Model\Objects\MeteoStations;

class HomepagePresenter extends BasePresenter {

    /**
     * 
     * @var \App\Model\Repository\VideoCamerasRepositoryLite 
     * @inject
     */
    public $videoCamerasRepositoryLite;

    /**
     * 
     * @var \App\Model\Repository\MeteoStationsRepositoryLite 
     * @inject
     */
    public $meteoStationsRepositoryLite;

    /**
     * @inject 
     * @var \App\Model\RepositoryRrd\MeteoStationsRepositoryRrd 
     */
    public $meteoStationsRepositoryRrd;

    protected function createComponentDataPresent() {
        $control = new \App\Control\DataPresentControl();
        $control->setTranslator($this->translator);
        return $control;
    }

    public function renderDefault() {
        if ($this->user->isAllowed('videoCameras', 'private')) {
            $videoCameras = $this->videoCamerasRepositoryLite->getVideoCameras();
        } elseif ($this->user->isAllowed('videoCameras', 'public')) {
            $videoCameras = $this->videoCamerasRepositoryLite->getVideoCameras(FALSE);
        } else {
            $videoCameras = new VideoCameras();
        }

        if ($this->user->isAllowed('meteoStations', 'private')) {
            $meteoStations = $this->meteoStationsRepositoryLite->getMeteoStations();
        } elseif ($this->user->isAllowed('meteoStations', 'public')) {
            $meteoStations = $this->meteoStationsRepositoryLite->getMeteoStations(FALSE);
        } else {
            $meteoStations = new MeteoStations();
        }

        foreach ($meteoStations as $meteoStation) {
            $meteoStation->data = $this->meteoStationsRepositoryRrd->lastUpdate($meteoStation);
        }
        $this->template->videoCameras = $videoCameras;
        $this->template->meteoStations = $meteoStations;
    }

}
