<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Presenters;

use App\Model\Objects\VideoCamera,
    App\Model\Objects\MeteoStation;

/**
 * Description of MrtgPresenter
 *
 * @author Karel
 */
class ShowPresenter extends BasePresenter {

    private $refresh = 6000;

    /**
     * @inject
     * @var \App\Model\Repository\VideoCamerasRepositoryLite 
     */
    public $videoCamerasRepositoryLite;

    /**
     * @inject
     * @var \App\Model\Repository\MeteoStationsRepositoryLite 
     */
    public $meteoStationsRepositoryLite;

    /**
     * @inject 
     * @var \App\Model\RepositoryRrd\MeteoStationsRepositoryRrd 
     */
    public $meteoStationsRepositoryRrd;

    protected function createComponentDataPresent() {
        $control = new \App\Control\DataPresentControl();
        $control->setTranslator($this->translator);
        return $control;
    }

    public function actionDefault() {
        $this->redirect('Homepage:default');
    }

    public function renderVideocamera($id = null) {
        $videoCamera = VideoCamera::newObjectId($id);
        if ($this->videoCamerasRepositoryLite->fetchVideoCamera($videoCamera) && ($videoCamera->private === false || $this->user->isAllowed('videoCameras', 'private'))) {
            $this->template->videoCamera = $videoCamera;
            $this->template->time = time();
            $this->template->refresh = $this->refresh;
        } else {
            $this->redirect('Homepage:default');
        }
    }

    public function renderMeteostation($id = null) {
        $meteoStation = MeteoStation::newObjectId($id);
        if ($this->meteoStationsRepositoryLite->fetchMeteoStation($meteoStation) && ($meteoStation->private === false || $this->user->isAllowed('meteoStations', 'private'))) {
            $this->meteoStationsRepositoryRrd->setDevice($meteoStation);
            $this->template->data = $this->meteoStationsRepositoryRrd->lastUpdate($meteoStation);
            $this->template->time = time();
            $this->template->refresh = $this->refresh;
        } else {
            $this->redirect('Homepage:default');
        }
    }

    public function renderMeteostationInpocasi($id = null) {
        $meteoStation = MeteoStation::newObjectId($id);
        if ($this->meteoStationsRepositoryLite->fetchMeteoStation($meteoStation) && ($meteoStation->private === false || $this->user->isAllowed('meteoStations', 'private'))) {
            $this->meteoStationsRepositoryRrd->setDevice($meteoStation);
            $this->template->data = $this->meteoStationsRepositoryRrd->lastUpdate($meteoStation);
            $this->template->time = time();
            $this->template->refresh = $this->refresh;
        } else {
            $this->redirect('Homepage:default');
        }
    }

    public function renderVideocameraFull($id = null) {
        /* REMOVED */
        $this->redirect(301, 'Show:Videocamera', $id);
    }

    public function handleReload() {
        if ($this->isAjax()) {
            $time = time();
            $this->payload->time = array(
                'imageresource' => $time,
                'imagepreview' => $time
            );
            $this->redrawControl('reload');
        } else {
            $this->redirect('Homepage:default');
        }
    }

}
