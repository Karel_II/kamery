<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Presenters;

use App\Model\Objects\VideoCamera,
    App\Model\Objects\MeteoStation;
use App\Model\Rrd\Helpes\GraphLineDashes,
    App\Model\RepositoryRrd\StaticRrd,
    App\Model\RepositoryRrd\OptionsRepository;
use Nette\Application\Responses\FileResponse;
use App\Model\Rrd\Options\OptionsCreate,
    App\Model\Rrd\Options\OptionsFetch,
    App\Model\Rrd\Options\OptionsUpdate,
    App\Model\Rrd\Options\OptionsGraph;

/**
 * Description of RrdPresenter
 *
 * @author karel.novak
 */
class RrdPresenter extends BasePresenter {

    private $refresh = 6000;

    /**
     * @inject
     * @var \App\Model\Repository\VideoCamerasRepositoryLite 
     */
    public $videoCamerasRepositoryLite;

    /**
     * @inject
     * @var \App\Model\Repository\MeteoStationsRepositoryLite 
     */
    public $meteoStationsRepositoryLite;

    /**
     * @inject
     * @var \App\Model\Repository\RrdSetupRepositoryLite 
     */
    public $rrdSetupRepositoryLite;

    /**
     * @inject 
     * @var \App\Model\RepositoryRrd\MeteoStationsRepositoryRrd 
     */
    public $meteoStationsRepositoryRrd;


    /**
     * @inject 
     * @var \App\Model\RepositoryRrd\VideoCamerasRepositoryRrd 
     */
    //public $videoCamerasRepositoryRrd;


    public function actionDefault() {
        $this->redirect('Homepage:default');
    }

    public function renderVideocameraGraphImage($id, $rrdGraph, $archiveIntervalId) {
        $this->terminate();
    }

    public function renderMeteostationGraphImage($id, $rrdGraph, $archiveIntervalId) {
        $meteoStation = MeteoStation::newObjectId($id);
        $options = new \App\Model\Rrd\Options\OptionsGraph();
        $options->setSize(1024, 576);
        if ($this->meteoStationsRepositoryLite->fetchMeteoStation($meteoStation) &&
                ($meteoStation->private === false || $this->user->isAllowed('meteoStations', 'private')) &&
                $this->rrdSetupRepositoryLite->fetchRrdGraphIntervalOption($options, $meteoStation, $archiveIntervalId)
        ) {
            $this->meteoStationsRepositoryRrd->setDevice($meteoStation);

            switch ($rrdGraph) {
                case "temperature":
                    $this->meteoStationsRepositoryRrd->temperatureGraph($meteoStation, $options, $meteoStation->description);
                    break;
                case "pressure":
                    $this->meteoStationsRepositoryRrd->pressureGraph($meteoStation, $options, $meteoStation->description);
                    break;
                case "wind":
                    $this->meteoStationsRepositoryRrd->windSpeedGraph($meteoStation, $options, $meteoStation->description);
                    break;
                case "humidity":
                    $this->meteoStationsRepositoryRrd->humidityGraph($meteoStation, $options, $meteoStation->description);
                    break;
                default :
                    $this->redirect('Homepage:default');
            }
        } else {
            $this->redirect('Homepage:default');
        }
        $response = new FileResponse($options->getFileGraph(), null, 'image/png', false);
        $this->sendResponse($response);
    }

    public function renderMeteostation($id = null, $rrdGraph = null) {
        $meteoStation = MeteoStation::newObjectId($id);
        if ($this->meteoStationsRepositoryLite->fetchMeteoStation($meteoStation) && ($meteoStation->private === false || $this->user->isAllowed('meteoStations', 'private'))) {
            $this->template->meteoStation = $meteoStation;
            $this->template->rrdGraphCurrent = (in_array($rrdGraph, $meteoStation->rrdGraphs)) ? $rrdGraph : NULL;
            $this->template->archiveIntervalNames = $this->rrdSetupRepositoryLite->getRrdArchiveIntervalNames($meteoStation);
            $keys = array_keys($this->template->archiveIntervalNames);
            $this->template->archiveIntervalFirst = array_shift($keys);
            $this->template->time = time();
            $this->template->refresh = $this->refresh;
        } else {
            $this->redirect('Homepage:default');
        }
    }

    public function renderVideocamera($id = null, $rrdGraph = null) {
        $videoCamera = VideoCamera::newObjectId($id);
        if ($this->videoCamerasRepositoryLite->fetchVideoCamera($videoCamera) && ($videoCamera->private === false || $this->user->isAllowed('videoCameras', 'private'))) {
            $this->template->videoCamera = $videoCamera;
            $this->template->rrdGraphCurrent = (in_array($rrdGraph, $videoCamera->rrdGraphs)) ? $rrdGraph : NULL;
            $this->template->archiveIntervalNames = $this->rrdSetupRepositoryLite->getRrdArchiveIntervalNames($videoCamera);
            $keys = array_keys($this->template->archiveIntervalNames);
            $this->template->archiveIntervalFirst = array_shift($keys);
            $this->template->time = time();
            $this->template->refresh = $this->refresh;
        } else {
            $this->redirect('Homepage:default');
        }
    }

    public function renderMeteostationInfo($id = null) {
        $meteoStation = MeteoStation::newObjectId($id);
        if ($this->meteoStationsRepositoryLite->fetchMeteoStation($meteoStation) && $this->user->isAllowed('meteoStations', 'edit')) {
            $this->template->meteoStation = $meteoStation;
            $this->template->info = $this->meteoStationsRepositoryRrd->info($meteoStation);
        } else {
            $this->redirect('Homepage:default');
        }
    }

    public function renderVideocameraInfo($id = null) {
        $videoCamera = VideoCamera::newObjectId($id);
        if ($this->videoCamerasRepositoryLite->fetchVideoCamera($videoCamera) && $this->user->isAllowed('videoCameras', 'edit')) {
            $this->template->videoCamera = $videoCamera;
            $this->template->info = $this->videoCamerasRepositoryLite->info($videoCamera);
        } else {
            $this->redirect('Homepage:default');
        }
    }

    public function handleReload() {
        if ($this->isAjax() && ($refreshImageId = $this->getHttpRequest()->getPost('refreshImageId'))) {
            $this->payload->refreshImageId = [$refreshImageId => time()];
            $this->redrawControl();
        } else {
            $this->redirect('Homepage:default');
        }
    }

}
