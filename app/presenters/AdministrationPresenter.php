<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Presenters;

use Nette\Utils\Json;
use \NetteAddons\Application\UI\IChangePasswordFormFactory,
    \NetteAddons\Application\UI\IChangeRolesFormFactory,
    \NetteAddons\Application\UI\IChangeUserFormFactory,
    App\Forms\User\ChangeUserFormFactory;
use \NetteAddons\Repository\UsersRepository,
    \NetteAddons\Repository\VisitorsRepository,
    \Netteaddons\Security\Identity;

/**
 * Description of AdministrationPresenter
 *
 * @author karel.novak
 */
class AdministrationPresenter extends BasePresenter {

    /**
     * @var IChangePasswordFormFactory 
     * @inject 
     */
    public $changePasswordFormFactory;

    /**
     * @var IChangeUserFormFactory 
     * @inject 
     */
    public $changeUserFormFactory;

    /**
     * @var IChangeRolesFormFactory 
     * @inject 
     */
    public $changeRolesFormFactory;

    /**
     * @var UsersRepository 
     * @inject 
     */
    public $usersRepository;

    /**
     * @var VisitorsRepository 
     * @inject 
     */
    public $visitorsRepository;

    /**
     * Change User Form factory.
     * @return Nette\Application\UI\Form
     */
    protected function createComponentChangeUserForm() {
        $control = $this->changeUserFormFactory->create();
        $control->onSuccess[] = function () {
            $this->redirect('this');
        };
        return $control;
    }

    /**
     * Change Roles Form factory.
     * @return Nette\Application\UI\Form
     */
    protected function createComponentChangeRolesForm() {
        $control = $this->changeRolesFormFactory->create();
        $control->onSuccess[] = function () {
            $this->redirect('this');
        };
        return $control;
    }

    /**
     * Change Password Form factory.
     * @return Nette\Application\UI\Form
     */
    protected function createComponentChangePasswordForm() {
        $control = $this->changePasswordFormFactory->create();
        $control->onSuccess[] = function () {
            $this->redirect('this');
        };
        return $control;
    }

    public function actionUserProfile($userId = NULL) {
        $identity = NULL;
        if (!$this->user->isLoggedIn()) {
            $this->redirect('Homepage:');
        } elseif ($this->user->isAllowed(UsersRepository::RESOURCE_MANAGEMENT_USER, UsersRepository::RESOURCE_MANAGEMENT_USER_PRIVILEDGE_OTHER)) {
            $identity = new Identity((empty($userId)) ? (int) $this->user->id : (int) $userId);
        } elseif ($this->user->isAllowed(UsersRepository::RESOURCE_MANAGEMENT_USER, UsersRepository::RESOURCE_MANAGEMENT_USER_PRIVILEDGE_SELF)) {
            $identity = new Identity((int) $this->user->id);
        } else {
            $this->redirect('Homepage:');
        }
        $this->usersRepository->fetchIdentity($identity);
        $this->getComponent('changeUserForm')->setDefaults($identity);
        $this->getComponent('changePasswordForm')->setDefaults($identity);
        $this->getComponent('changeRolesForm')->setDefaults($identity);
    }

    public function renderUsersList() {
        if (!$this->user->isLoggedIn()) {
            $this->redirect('Homepage:');
        } elseif ($this->user->isAllowed(UsersRepository::RESOURCE_MANAGEMENT_USER, UsersRepository::RESOURCE_MANAGEMENT_USER_PRIVILEDGE_OTHER)) {
            $this->template->identities = $this->usersRepository->getIdentities();
        } else {
            $this->redirect('Homepage:');
        }
    }

    public function renderTest() {
        
    }

    public function renderInfo() {
        if (!$this->user->isAllowed('administration')) {
            $this->redirect('Homepage:');
        }
        $this->template->uniqueIPAddress = $this->visitorsRepository->getUniqueIPAddress();
        $this->template->visitors = $this->visitorsRepository->getVisitors();
        $this->template->info = @file_get_contents('http://127.0.0.1/' . 'server-status?auto');
    }

}
