<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Presenters;

use Nette\Utils\Image;
use NetteAddons\Http\Url;

/**
 * Description of WatermarkPresenter
 *
 * @author Karel
 */
class WatermarkPresenter extends BasePresenter {

    public function actionRender($target = NULL) {
        $targetUrl = new Url($this->link('//VideoCameras:videoSecure', $target));
        $watermark = Image::fromFile(WWW_DIR . DIRECTORY_SEPARATOR . $this->link('Watermark:image'));
        $image = Image::fromFile($targetUrl);
        $watermark->resize($image->getWidth() / 10, $image->getHeight() / 10, Image::SHRINK_ONLY);
        $background = Image::fromBlank($watermark->getWidth(), $watermark->getHeight(), Image::rgb(255, 255, 255));
        $image->place($background, '95%', '95%', 40);
        $image->place($watermark, '95%', '95%', 100);
        $image->send();
        $this->terminate();
    }

}
