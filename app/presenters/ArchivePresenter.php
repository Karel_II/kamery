<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Presenters;

use App\Model\Repository\ArchiveRepositoryLite;
use App\Model\Objects\VideoCamera,
    App\Model\Objects\MeteoStation;
use App\Model\Rrd\Options\OptionsFetch;

/**
 * Description of ArchivePresenter
 *
 * @author karel.novak
 */
class ArchivePresenter extends BasePresenter {

    const ARCHIVE_ITEMS_PER_PAGE = 10;

    /**
     * @inject
     * @var \App\Model\Repository\VideoCamerasRepositoryLite 
     */
    public $videoCamerasRepositoryLite;

    /**
     * @inject
     * @var \App\Model\Repository\MeteoStationsRepositoryLite 
     */
    public $meteoStationsRepositoryLite;

    /**
     * @inject
     * @var \App\Model\Repository\RrdSetupRepositoryLite 
     */
    public $rrdSetupRepositoryLite;

    /**
     * @inject 
     * @var \App\Model\RepositoryRrd\MeteoStationsRepositoryRrd 
     */
    public $meteoStationsRepositoryRrd;

    /**
     * @inject
     * @var ArchiveRepositoryLite 
     */
    public $archiveRepositoryLite;

    protected function createComponentDataPresent() {
        $control = new \App\Control\DataPresentControl();
        $control->setTranslator($this->translator);
        return $control;
    }

    public function renderVideocamera($id = NULL, $year = NULL) {
        $pageControl = $this->getComponentPageControl();
        $videoCamera = VideoCamera::newObjectId($id);
        if ($this->videoCamerasRepositoryLite->fetchVideoCamera($videoCamera) && $videoCamera->hasArchive && ($videoCamera->private === false || $this->user->isAllowed('videoCameras', 'private'))) {
            $paginator = $pageControl->getPaginator();
            $paginator->setItemsPerPage(self::ARCHIVE_ITEMS_PER_PAGE);

            $years = $this->archiveRepositoryLite->getArchiveYears($videoCamera);

            if (empty($years)) {
                $this->flashMessage('Archive of ' . $videoCamera->name . ' is empty !', 'danger');
                $this->redirect('Homepage:default');
            }

            $yearCurrentArchiveRecordFinder = (isset($year) && isset($years[$year])) ? $years[$year] : reset($years);
            $archiveRecordsCount = $this->archiveRepositoryLite->countArchive($yearCurrentArchiveRecordFinder);
            $paginator->setItemCount($archiveRecordsCount);

            $this->template->yearCurrentArchiveRecordFinder = $yearCurrentArchiveRecordFinder;
            $this->template->years = $years;
            $this->template->archive = $this->archiveRepositoryLite->getArchive($yearCurrentArchiveRecordFinder, $paginator);
        } else {
            $this->redirect('Homepage:default');
        }
    }

    public function renderMeteostation($id = NULL, $archiveIntervalId = NULL) {
        $pageControl = $this->getComponentPageControl();
        $meteoStation = MeteoStation::newObjectId($id);
        $options = new OptionsFetch();
        if ($this->meteoStationsRepositoryLite->fetchMeteoStation($meteoStation) &&
                ($meteoStation->private === false || $this->user->isAllowed('meteoStations', 'private')) &&
                $this->rrdSetupRepositoryLite->fetchRrdArchiveIntervalOption($options, $meteoStation, $archiveIntervalId)
        ) {
            $paginator = $pageControl->getPaginator();
            $paginator->setItemsPerPage(self::ARCHIVE_ITEMS_PER_PAGE);
            $this->meteoStationsRepositoryRrd->setDevice($meteoStation);
            $this->template->archiveIntervalNames = $this->rrdSetupRepositoryLite->getRrdArchiveIntervalNames($meteoStation);
            $this->template->archiveIntervalCurrent = $archiveIntervalId;
            $this->template->meteoStation = $meteoStation;
            $archive = $this->meteoStationsRepositoryRrd->fetch($meteoStation, $options);
            $paginator->setItemCount(count($archive));
            $this->template->archive = new \Nette\Utils\ArrayList();
            for ($i = 0; $i < $paginator->getLength(); $i++) {
                $this->template->archive[] = $archive[$paginator->getOffset() + $i];
            }
        } else {
            $this->redirect('Homepage:default');
        }
    }

}
