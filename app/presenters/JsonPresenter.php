<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Presenters;

use Nette,
    Nette\Application\Responses\JsonResponse,
    Nette\Utils\DateTime;
use App\Model\DBase\KameryRepositoryLite,
    App\Model\Repository\ArchiveRepositoryLite;
use App\Model\Objects\VideoCamera,
    App\Model\Objects\MeteoStation;

/**
 * Description of JsonPresenter
 *
 * @author karel.novak
 */
class JsonPresenter extends BasePresenter {

    /**
     * @inject
     * @var \App\Model\Repository\VideoCamerasRepositoryLite 
     */
    public $videoCamerasRepositoryLite;

    /**
     * @inject
     * @var \App\Model\Repository\MeteoStationsRepositoryLite 
     */
    public $meteoStationsRepositoryLite;

    /** @var ArchiveRepositoryLite @inject */
    public $archiveRepositoryLite;

    protected function beforeRender() {
        $httpRequest = $this->getHttpRequest();
        $httpResponse = $this->getHttpResponse();

        $http_origin = $httpRequest->getHeader('Origin');
        if (preg_match('/^http[s]?:\/\/(krivonet.cz|krivonet.rubiq.cz|www.krivonet.cz)/i', $http_origin)) {
            $httpResponse->addHeader('Access-Control-Allow-Origin', $http_origin);
            $httpResponse->addHeader('Access-Control-Allow-Methods', ' GET, POST');
        }
        parent::beforeRender();
    }

    public function renderDefault() {
        $homePageJson = array(
            "homepage" => $this->link("//Homepage:default"),
            "json" => $this->link("//this"),
            "videocameras" => $this->link("//Json:Videocameras"),
            "meteostations" => $this->link("//Json:Meteostations"));
        $this->sendResponse(new JsonResponse($homePageJson));
    }

    public function renderVideocameras() {
        $videoCameras = $this->videoCamerasRepositoryLite->getVideoCameras(FALSE);
        $videoCamerasJson = array();
        foreach ($videoCameras as $videoCameraId => $videoCamera) {
            $videoCameraJson = array();
            $videoCameraJson['name'] = $videoCamera->name;
            $videoCameraJson['description'] = $videoCamera->description;
            $videoCameraJson['previewUrl'] = $this->link('//VideoCameras:videoPreview', $videoCamera->name);
            $videoCameraJson['liveUrl'] = $this->link('//VideoCameras:video', $videoCamera->name, time());
            $videoCameraJson['archiveUrl'] = $this->link('//Json:videocameraArchive', $videoCamera->id);
            $videoCamerasJson [$videoCameraId] = $videoCameraJson;
        }
        $this->sendResponse(new JsonResponse($videoCamerasJson));
    }

    public function renderMeteostations() {
        $meteoStations = $this->meteoStationsRepositoryLite->getMeteoStations(FALSE);
        $meteoStationsJson = array();
        foreach ($meteoStations as $meteoStationId => $meteoStation) {
            $meteoStationJson = array();
            $meteoStationJson['name'] = $meteoStation->name;
            $meteoStationJson['description'] = $meteoStation->description;
            $meteoStationJson['previewUrl'] = $this->link('//MeteoStations:xmlPreview', $meteoStation->name);
            $meteoStationJson['liveUrl'] = $this->link('//MeteoStations:xml', $meteoStation->name, time());
            $meteoStationJson['archiveUrl'] = $this->link('//Json:meteostationsArchive', $meteoStation->id);
            $meteoStationsJson[$meteoStationId] = $meteoStationJson;
        }
        $this->sendResponse(new JsonResponse($meteoStationsJson));
    }

    public function renderVideocameraArchive($id = NULL, $year = NULL) {
        $videoCamera = VideoCamera::newObjectId($id);
        if ($this->videoCamerasRepositoryLite->fetchVideoCamera($videoCamera) && $videoCamera->private === false) {
            $return = array();
            $years = $this->archiveRepositoryLite->getArchiveYears($videoCamera);
            foreach ($years as $year => $dateTime) {
                $return['years'][$year] = $this->link('//this', $videoCamera->id, $year);
            }
            $yearCurrentArchiveRecordFinder = (isset($year) && isset($years[$year])) ? $years[$year] : reset($years);
            $archiveRecordsCount = $this->archiveRepositoryLite->countArchive($yearCurrentArchiveRecordFinder);
            $return['recordsCount'][$yearCurrentArchiveRecordFinder->year] = $archiveRecordsCount;
            $archive = $this->archiveRepositoryLite->getArchive($yearCurrentArchiveRecordFinder);

            foreach ($archive as $archiveRecord) {
                $return['records'][$year][] = $this->link('//VideoCameras:videoArchive', $archiveRecord->year, $archiveRecord->device->name, $archiveRecord->datehour);
            }

            $this->sendResponse(new JsonResponse($return));
        }
        $this->terminate();
    }

    public function renderMeteostationsArchive($id = NULL, $year = NULL) {
        $meteoStation = MeteoStation::newObjectId($id);
        if ($this->meteoStationsRepositoryLite->fetchMeteoStation($meteoStation) && $meteoStation->private === false) {
            $return = array();
            $years = $this->archiveRepositoryLite->getArchiveYears($meteoStation);
            foreach ($years as $year => $dateTime) {
                $return['years'][$year] = $this->link('//this', $meteoStation->id, $year);
            }
            $yearCurrentArchiveRecordFinder = (isset($year) && isset($years[$year])) ? $years[$year] : reset($years);
            $archiveRecordsCount = $this->archiveRepositoryLite->countArchive($yearCurrentArchiveRecordFinder);
            $return['recordsCount'][$yearCurrentArchiveRecordFinder->year] = $archiveRecordsCount;
            $archive = $this->archiveRepositoryLite->getArchive($yearCurrentArchiveRecordFinder);

            foreach ($archive as $archiveRecord) {
                $return['records'][$year][] = $this->link('//MeteoStations:xmlArchive', $archiveRecord->year, $archiveRecord->device->name, $archiveRecord->datehour);
            }

            $this->sendResponse(new JsonResponse($return));
        }
        $this->terminate();
    }

}
