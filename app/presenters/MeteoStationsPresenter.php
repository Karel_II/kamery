<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Presenters;

use App\Forms\MeteoStationFormFactory;
use App\Model\Objects\MeteoStation;

/**
 * Description of MeteoStationsPresenter
 *
 * @author Karel
 */
class MeteoStationsPresenter extends BasePresenter {

    /**
     * @var MeteoStationFormFactory 
     * @inject
     */
    public $meteoStationFormFactory;

    /**
     * @inject
     * @var \App\Model\Repository\MeteoStationsRepositoryLite 
     */
    public $meteoStationsRepositoryLite;

    /**
     * @inject 
     * @var \App\Model\RepositoryRrd\MeteoStationsRepositoryRrd 
     */
    public $meteoStationsRepositoryRrd;

    /**
     * @return Nette\Application\UI\Form
     */
    protected function createComponentMeteoStationForm() {
        $form = $this->meteoStationFormFactory->create();
        $form->onSuccess[] = function ($form) {
            $form->getPresenter()->redirect('default');
        };
        return $form;
    }

    protected function createComponentDataPresent() {
        $control = new \App\Control\DataPresentControl();
        $control->setTranslator($this->translator);
        return $control;
    }

    /**
     * @todo Remove this stuff
     * 
     */
    public function actionXml($name = null, $time = null) {
        $this->redirect('Homepage:default');
    }

    /**
     * @todo Remove this stuff
     * 
     */
    public function actionXmlSecure($name = null, $time = null) {
        $this->redirect('Homepage:default');
    }

    public function actionUpdateWeatherStation($ID, $PASSWORD) {
        $query = $this->getHttpRequest()->getQuery();
        \Tracy\Debugger::log($query);
        $this->terminate();
    }

    public function renderDataXml($id = null, $time = null) {
        $meteoStation = MeteoStation::newObjectId($id);
        if ($this->meteoStationsRepositoryLite->fetchMeteoStation($meteoStation) && ($meteoStation->private === false || $this->user->isAllowed('meteoStations', 'private'))) {
            $this->meteoStationsRepositoryRrd->setDevice($meteoStation);
            $this->template->data = $this->meteoStationsRepositoryRrd->lastUpdate($meteoStation);
        } else {
            $this->error();
        }
    }

    public function renderDataJson($id = null, $time = null) {
        $meteoStation = MeteoStation::newObjectId($id);
        if ($this->meteoStationsRepositoryLite->fetchMeteoStation($meteoStation) && ($meteoStation->private === false || $this->user->isAllowed('meteoStations', 'private'))) {
            $this->meteoStationsRepositoryRrd->setDevice($meteoStation);
            $this->template->data = $this->meteoStationsRepositoryRrd->lastUpdate($meteoStation);
        } else {
            $this->error();
        }
    }

    public function renderDefault() {
        if ($this->user->isAllowed('meteoStations', 'private')) {
            $meteoStations = $this->meteoStationsRepositoryLite->getMeteoStations();
        } elseif ($this->user->isAllowed('meteoStations', 'public')) {
            $meteoStations = $this->meteoStationsRepositoryLite->getMeteoStations(FALSE);
        } else {
            $meteoStations = new \App\Model\Objects\MeteoStations();
        }
        $this->template->meteoStations = $meteoStations;
    }

    public function renderEdit($id = null) {
        if (!$this->user->isAllowed('meteoStations', 'edit')) {
            $this->redirect('Homepage:default');
        }
        if (isset($id)) {
            $meteoStation = MeteoStation::newObjectId($id);
            $this->meteoStationFormFactory->setDefaults($meteoStation);
            $this->meteoStationFormFactory->setEdit(true);
        }
    }

}
