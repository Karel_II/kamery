<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Presenters;

use App\Model\Objects\VideoCamera,
    App\Model\Objects\MeteoStation;
use App\Model\RepositoryRrd\MeteoStationsRepositoryRrd;

/**
 * Description of MrtgPresenter
 *
 * @author Karel
 */
class MrtgPresenter extends BasePresenter {

    private $refresh = 6000;

    /**
     * @inject
     * @var \App\Model\Repository\VideoCamerasRepositoryLite 
     */
    public $videoCamerasRepositoryLite;

    /**
     * @inject
     * @var \App\Model\Repository\MeteoStationsRepositoryLite 
     */
    public $meteoStationsRepositoryLite;

    public function actionDefault() {
        $this->redirect('Homepage:default');
    }

    public function renderVideocamera($id = null) {
        $videoCamera = VideoCamera::newObjectId($id);
        if ($this->videoCamerasRepositoryLite->fetchVideoCamera($videoCamera) && !empty($videoCamera->snmpCommunity) && ($videoCamera->private === false || $this->user->isAllowed('videoCameras', 'private'))) {
            $this->template->videoCamera = $videoCamera;
            $this->template->time = time();
            $this->template->refresh = $this->refresh;
        } else {
            $this->redirect('Homepage:default');
        }
    }

    public function renderMeteostation($id = null) {
        $meteoStation = MeteoStation::newObjectId($id);
        if ($this->meteoStationsRepositoryLite->fetchMeteoStation($meteoStation) && !empty($meteoStation->snmpCommunity) && ($meteoStation->private === false || $this->user->isAllowed('meteoStations', 'private'))) {
            $this->template->meteoStation = $meteoStation;
            $this->template->time = time();
            $this->template->refresh = $this->refresh;
        } else {
            $this->redirect('Homepage:default');
        }
    }

    public function actionVideocameraHistory($name = null, $nameProbe = null, $probe = null) {
        $this->redirect('Homepage:default');
    }

    public function actionMeteostationHistory($name = null, $nameProbe = null, $probe = null) {
        $this->redirect('Homepage:default');
    }

    public function actionVideocameraGraph($name = null, $nameProbe = null, $probe = null, $period = null) {
        $this->redirect('Homepage:default');
    }

    public function actionMeteostationGraph($name = null, $nameProbe = null, $probe = null, $period = null) {
        $this->redirect('Homepage:default');
    }

    public function handleReload() {
        if ($this->isAjax()) {
            $this->template->time = time();
            $this->redrawControl('reload');
        } else {
            $this->redirect('Homepage:default');
        }
    }

}
