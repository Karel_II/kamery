<?php

namespace App\Presenters;

use Nette;
use NetteAddons\Application\UI\ISignInFormFactory,
    NetteAddons\Application\UI\ISignUpFormFactory,
    NetteAddons\Application\UI\IForgotPasswordFormFactory,
    NetteAddons\Application\UI\IChangePasswordFormFactory;
use \NetteAddons\Repository\UsersRepository;

/**
 * Sign in/out presenters.
 */
class SignPresenter extends BasePresenter {

    /**
     * @var ISignInFormFactory 
     * @inject 
     */
    public $signInFactory;

    /**
     * @var ISignUpFormFactory 
     * @inject */
    public $signUpFactory;

    /**
     * @var IForgotPasswordFormFactory 
     * @inject */
    public $forgotPasswordFormFactory;

    /**
     * @var IChangePasswordFormFactory 
     * @inject */
    public $changePasswordFormFactory;

    /**
     * @var UsersRepository 
     * @inject */
    public $usersRepository = NULL;

    /**
     * Sign-in form factory.
     * @return Nette\Application\UI\Control
     */
    protected function createComponentSignInForm() {
        $control = $this->signInFactory->create();
        $control->onSuccess[] = function () {
            $this->redirect('Homepage:');
        };
        return $control;
    }

    /**
     * Sign-up form factory.
     * @return Nette\Application\UI\Form
     */
    protected function createComponentSignUpForm() {
        $control = $this->signUpFactory->create();
        $control->onSuccess[] = function () {
            $this->redirect('Homepage:');
        };
        return $control;
    }

    /**
     * Sign-up form factory.
     * @return Nette\Application\UI\Form
     */
    protected function createComponentForgotPasswordForm() {
        $control = $this->forgotPasswordFormFactory->create();
        $control->onSuccess[] = function () {
            $this->redirect('Homepage:');
        };
        $control->setOnSendEmail(function ($otp, \Netteaddons\Security\Identity $identity) {
            \Tracy\Debugger::barDump($otp);
            \Tracy\Debugger::barDump($identity);
        });
        return $control;
    }

    /**
     * Sign-up form factory.
     * @return Nette\Application\UI\Form
     */
    protected function createComponentChangePasswordForm() {
        $control = $this->changePasswordFormFactory->create();
        $control->onSuccess[] = function () {
            $this->redirect('Homepage:');
        };
        return $control;
    }

    public function actionOut() {
        $this->getUser()->logout();
    }

    public function actionUp() {
        if (!$this->getUser()->isAllowed('administration')) {
            $this->redirect('Homepage:');
        }
    }

    public function actionOtp($otp, $emailAddress) {
        $identity = new \Netteaddons\Security\Identity(0, array(), ['email' => base64_decode($emailAddress)]);
        if ($this->usersRepository->identityFetchOtpPasswordChange($identity, $otp)) {
            $this->changePasswordFormFactory->setDefaults($identity->id);
        } else {
            throw new Nette\Application\BadRequestException('Page Not Found', '404');
        }
    }

}
