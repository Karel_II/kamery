<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Forms;

use \NetteAddons\Application\UI\ISetupFormFactory;
use Nette;
use Nette\Application\UI\Form;
use NetteAddons\Http\Url;
use App\Model\Repository\VideoCamerasRepositoryLite,
    App\Model\Repository\RrdSetupRepositoryLite;
use App\Model\Objects\VideoCamera;
use App\Model\Data\Loader;
use App\Model\Objects\GeographicCoordinateSystem,
    VojtechDobes\NetteForms\GpsPicker;

/**
 * Description of VideoCameraFormFactory
 *
 * @author Karel
 */
class VideoCameraFormFactory {

    use \Nette\SmartObject;

    /** @var ISetupFormFactory */
    private $factory;

    /** @var VideoCamerasRepositoryLite */
    private $videoCamerasRepositoryLite;

    /** @var RrdSetupRepositoryLite */
    private $rrdSetupRepositoryLite;

    /** @var Form */
    private $form;

    /** @var array */
    private $defaults = null;

    /** @var bool */
    private $edit = false;

    public function __construct(ISetupFormFactory $factory, VideoCamerasRepositoryLite $videoCamerasRepositoryLite, RrdSetupRepositoryLite $rrdSetupRepositoryLite) {
        $this->factory = $factory;
        $this->videoCamerasRepositoryLite = $videoCamerasRepositoryLite;
        $this->rrdSetupRepositoryLite = $rrdSetupRepositoryLite;
        $this->form = $this->factory->create();
    }

    /**
     * @return Form
     */
    public function create() {

        $this->form->addText('name', 'Name :')->setRequired();
        $this->form->addText('description', 'Description :');
        $this->form->addTextArea('instalationDescription', 'Istalation description :');
        $this->form->addText('url', 'Url :')->addRule(Form::URL, 'Must be URL !')->setRequired();
        $this->form->addText('urlSource', 'Url Source:');
        $this->form->addText('urlUsername', 'Username :');
        $this->form->addPassword('urlPassword', 'Password :');
        $this->form->addText('snmpCommunity', 'Smnp Community :');
        $this->form->addCheckbox('watermark', 'Watermark');
        $this->form->addCheckbox('private', 'Private');
        //$this->form->addGpsPicker('coords', 'Coordinates:')->disableSearch()->setDriver(GpsPicker::DRIVER_OPENSTREETMAP);
        $this->form->addGroup('RRD Data Collection');
        $this->form->addSelect('rrdDataDriver', 'Data Collector Driver', Loader::getDrivers())->setRequired(TRUE);
        $this->form->addSelect('rrdStep', 'Data Collect Step', $this->rrdSetupRepositoryLite->getRrdCollectIntervalNames())->setRequired(TRUE);
        $this->form->addCheckboxList('rrdGraphs', 'Graphs', $this->rrdSetupRepositoryLite->getRrdGraphs());
        /**
         * Preview and Archive
         */
        $this->form->addGroup('Preview and Archive');
        $this->form->addText('archiveDays', 'Archive Days :')
                ->setDefaultValue(365);
        $this->form->addText('previewMaxWidth', 'Preview Image Max Width')
                ->setDefaultValue(1024);
        $this->form->addText('previewMaxHeight', 'Preview Image Max Height')
                ->setDefaultValue(1024);
        $this->form->addText('archiveMaxWidth', 'Archive Image Max Width')
                ->setDefaultValue(1024);
        $this->form->addText('archiveMaxHeight', 'Archive Image Max Height')
                ->setDefaultValue(1024);


        $this->form->addHidden('id');


        if ($this->edit) {
            $this->form->addSubmit('send', 'Update');
        } else {
            $this->form->addSubmit('send', 'Add');
        }


        if (isset($this->defaults)) {
            $this->form->setDefaults($this->defaults);
        }

        // call method addSeriesFormSucceeded() on success
        $this->form->onSuccess[] = [$this, 'addVideoCameraFormSucceeded'];
        return $this->form;
    }

    public function addVideoCameraFormSucceeded(Form $form) {
        $values = $form->getValues();
        $url = new Url($values->url);
        if (!(empty($values->urlUsername) && empty($values->urlPassword))) {
            $url->setUser($values->urlUsername)->setPassword($values->urlPassword);
        }
        if (empty($values->id)) {
            //$videoCamera = VideoCamera::newVideoCamera($values->id, $values->name, $values->description, $values->instalationDescription, $url, $values->private, $values->snmpCommunity, new GeographicCoordinateSystem($values->coords->lat, $values->coords->lng), $values->archiveDays);
            $videoCamera = VideoCamera::newVideoCamera(NULL, $values->name, $values->description, $values->instalationDescription, $url, (empty($values->urlSource) ? NULL : new Url($values->urlSource)), $values->watermark, $values->private, $values->snmpCommunity, NULL, $values->archiveDays, $values->previewMaxWidth, $values->previewMaxHeight, $values->archiveMaxWidth, $values->archiveMaxHeight);
            $videoCamera->setRrd($values->rrdDataDriver, $values->rrdStep, $values->rrdGraphs);
            $this->videoCamerasRepositoryLite->insertVideoCamera($videoCamera);
            $form->getPresenter()->flashMessage('Video Camera ' . $videoCamera->name . ' has heen inserted', 'info');
        } else {
            //$videoCamera = VideoCamera::newVideoCamera($values->id, $values->name, $values->description, $values->instalationDescription, $url, $values->private, $values->snmpCommunity, new GeographicCoordinateSystem($values->coords->lat, $values->coords->lng), $values->archiveDays);
            $videoCamera = VideoCamera::newVideoCamera($values->id, $values->name, $values->description, $values->instalationDescription, $url, (empty($values->urlSource) ? NULL : new Url($values->urlSource)), $values->watermark, $values->private, $values->snmpCommunity, NULL, $values->archiveDays, $values->previewMaxWidth, $values->previewMaxHeight, $values->archiveMaxWidth, $values->archiveMaxHeight);
            $videoCamera->setRrd($values->rrdDataDriver, $values->rrdStep, $values->rrdGraphs);
            $this->videoCamerasRepositoryLite->updateVideoCamera($videoCamera);
            $form->getPresenter()->flashMessage('Video Camera ' . $videoCamera->name . ' has heen udated', 'info');
        }
    }

    public function setDefaults(VideoCamera $videoCamera) {
        $this->videoCamerasRepositoryLite->fetchVideoCamera($videoCamera);
        $this->defaults = array();
        $this->defaults['id'] = $videoCamera->id;
        $this->defaults['name'] = $videoCamera->name;
        $this->defaults['description'] = $videoCamera->description;
        $this->defaults['instalationDescription'] = $videoCamera->instalationDescription;

        $this->defaults['url'] = (string) $videoCamera->url;
        $this->defaults['urlUsername'] = (string) $videoCamera->url->getUser();
        $this->defaults['urlPassword'] = (string) $videoCamera->url->getPassword();

        $this->defaults['urlSource'] = (string) $videoCamera->urlSource;
        /**
         * Preview and Archive
         */
        $this->defaults['archiveDays'] = $videoCamera->archiveDays;
        $this->defaults['previewMaxHeight'] = $videoCamera->previewMaxHeight;
        $this->defaults['previewMaxWidth'] = $videoCamera->previewMaxWidth;
        $this->defaults['archiveMaxHeight'] = $videoCamera->archiveMaxHeight;
        $this->defaults['archiveMaxWidth'] = $videoCamera->archiveMaxWidth;

        $this->defaults['snmpCommunity'] = $videoCamera->snmpCommunity;
        $this->defaults['watermark'] = $videoCamera->watermark;
        $this->defaults['private'] = $videoCamera->private;

        $this->defaults['snmpCommunity'] = $videoCamera->snmpCommunity;
        $this->defaults['watermark'] = $videoCamera->watermark;
        $this->defaults['private'] = $videoCamera->private;

        $this->defaults['rrdDataDriver'] = $videoCamera->rrdDataDriver;
        $this->defaults['rrdStep'] = $videoCamera->rrdStep;
        $this->defaults['rrdGraphs'] = $videoCamera->rrdGraphs;


        if ($videoCamera->geographicCoordinates->dec !== NULL) {
            $coords = array(
                'lat' => $videoCamera->geographicCoordinates->latitudeDec,
                'lng' => $videoCamera->geographicCoordinates->longitudeDec
            );
            $this->defaults['coords'] = $coords;
        }

        if ($this->form->name !== null) {
            $this->form->setDefaults($this->defaults);
        }
    }

    public function setEdit($edit = false) {
        $this->edit = $edit;
        if ($this->form->name !== null) {
            $this->form['send']->caption = 'Update';
        }
    }

}
