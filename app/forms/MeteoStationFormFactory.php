<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Forms;

use \NetteAddons\Application\UI\ISetupFormFactory;
use Nette;
use Nette\Application\UI\Form;
use NetteAddons\Http\Url;
use App\Model\Repository\MeteoStationsRepositoryLite,
    App\Model\Repository\RrdSetupRepositoryLite;
use App\Model\Objects\MeteoStation;
use App\Model\Data\Loader;
use App\Model\Objects\GeographicCoordinateSystem,
    VojtechDobes\NetteForms\GpsPicker;

/**
 * Description of VideoCameraFormFactory
 *
 * @author Karel
 */
class MeteoStationFormFactory {

    use \Nette\SmartObject;

    /** @var ISetupFormFactory */
    private $factory;

    /** @var MeteoStationsRepositoryLite */
    private $meteoStationsRepositoryLite;

    /** @var RrdSetupRepositoryLite */
    private $rrdSetupRepositoryLite;

    /** @var Form */
    private $form;

    /** @var array */
    private $defaults = null;

    /** @var bool */
    private $edit = false;

    public function __construct(ISetupFormFactory $factory, MeteoStationsRepositoryLite $meteoStationsRepositoryLite, RrdSetupRepositoryLite $rrdSetupRepositoryLite) {
        $this->factory = $factory;
        $this->meteoStationsRepositoryLite = $meteoStationsRepositoryLite;
        $this->rrdSetupRepositoryLite = $rrdSetupRepositoryLite;
        $this->form = $this->factory->create();
    }

    /**
     * @return Form
     */
    public function create() {

        $this->form->addText('name', 'Name :')->setRequired();
        $this->form->addText('description', 'Description :');
        $this->form->addTextArea('instalationDescription', 'Istalation description :');
        $this->form->addText('url', 'Url :')->addRule(Form::URL, 'Must be URL !')->setRequired();
        $this->form->addText('urlSource', 'Url Source:');
        $this->form->addText('urlUsername', 'Username :');
        $this->form->addPassword('urlPassword', 'Password :');
        $this->form->addText('snmpCommunity', 'Smnp Community :');
        $this->form->addCheckbox('watermark', 'Watermark');
        $this->form->addCheckbox('private', 'Private');
        //$this->form->addGpsPicker('coords', 'Coordinates:')->disableSearch()->setDriver(GpsPicker::DRIVER_OPENSTREETMAP);
        $this->form->addGroup('RRD Data Collection');
        $this->form->addSelect('rrdDataDriver', 'Data Collector Driver', Loader::getDrivers())->setRequired(TRUE);
        $this->form->addSelect('rrdStep', 'Data Collect Step', $this->rrdSetupRepositoryLite->getRrdCollectIntervalNames())->setRequired(TRUE);
        $this->form->addCheckboxList('rrdGraphs', 'Graphs', $this->rrdSetupRepositoryLite->getRrdGraphs());
        /**
         * Preview and Archive
         */
        $this->form->addGroup('Preview and Archive');
        $this->form->addText('archiveDays', 'Archive Days :');

        $this->form->addHidden('id');


        if ($this->edit) {
            $this->form->addSubmit('send', 'Update');
        } else {
            $this->form->addSubmit('send', 'Add');
        }


        if (isset($this->defaults)) {
            $this->form->setDefaults($this->defaults);
        }

        // call method addSeriesFormSucceeded() on success
        $this->form->onSuccess[] = [$this, 'addMeteoStationFormSucceeded'];
        return $this->form;
    }

    public function addMeteoStationFormSucceeded(Form $form) {
        $values = $form->getValues();
        $url = new Url($values->url);
        if (!(empty($values->urlUsername) && empty($values->urlPassword))) {
            $url->setUser($values->urlUsername)->setPassword($values->urlPassword);
        }
        if (empty($values->id)) {
            //$meteoStation = MeteoStation::newMeteoStation($values->id, $values->name, $values->description, $values->instalationDescription, $url, $values->private, $values->snmpCommunity, new GeographicCoordinateSystem($values->coords->lat, $values->coords->lng), $values->archiveDays);
            $meteoStation = MeteoStation::newMeteoStation(NULL, $values->name, $values->description, $values->instalationDescription, $url, (empty($values->urlSource) ? NULL : new Url($values->urlSource)), $values->watermark, $values->private, $values->snmpCommunity, NULL, $values->archiveDays);
            $meteoStation->setRrd($values->rrdDataDriver, $values->rrdStep, $values->rrdGraphs);
            $this->meteoStationsRepositoryLite->insertMeteoStation($meteoStation);
            $form->getPresenter()->flashMessage('Meteo Station ' . $meteoStation->name . ' has heen inserted', 'info');
        } else {
            //$meteoStation = MeteoStation::newMeteoStation($values->id, $values->name, $values->description, $values->instalationDescription, $url, $values->private, $values->snmpCommunity, new GeographicCoordinateSystem($values->coords->lat, $values->coords->lng), $values->archiveDays);
            $meteoStation = MeteoStation::newMeteoStation($values->id, $values->name, $values->description, $values->instalationDescription, $url, (empty($values->urlSource) ? NULL : new Url($values->urlSource)), $values->watermark, $values->private, $values->snmpCommunity, NULL, $values->archiveDays);
            $meteoStation->setRrd($values->rrdDataDriver, $values->rrdStep, $values->rrdGraphs);
            $this->meteoStationsRepositoryLite->updateMeteoStation($meteoStation);
            $form->getPresenter()->flashMessage('Meteo Station ' . $meteoStation->name . ' has heen udated', 'info');
        }
    }

    public function setDefaults(MeteoStation $meteoStation) {
        $this->meteoStationsRepositoryLite->fetchMeteoStation($meteoStation);
        $this->defaults = array();
        $this->defaults['id'] = $meteoStation->id;
        $this->defaults['name'] = $meteoStation->name;
        $this->defaults['description'] = $meteoStation->description;
        $this->defaults['instalationDescription'] = $meteoStation->instalationDescription;

        $this->defaults['url'] = (string) $meteoStation->url;
        $this->defaults['urlUsername'] = (string) $meteoStation->url->getUser();
        $this->defaults['urlPassword'] = (string) $meteoStation->url->getPassword();

        $this->defaults['urlSource'] = (string) $meteoStation->urlSource;

        $this->defaults['archiveDays'] = $meteoStation->archiveDays;

        $this->defaults['snmpCommunity'] = $meteoStation->snmpCommunity;
        $this->defaults['watermark'] = $meteoStation->watermark;
        $this->defaults['private'] = $meteoStation->private;

        $this->defaults['rrdDataDriver'] = $meteoStation->rrdDataDriver;
        $this->defaults['rrdStep'] = $meteoStation->rrdStep;
        $this->defaults['rrdGraphs'] = $meteoStation->rrdGraphs;

        if ($meteoStation->geographicCoordinates->dec !== NULL) {
            $coords = array(
                'lat' => $meteoStation->geographicCoordinates->latitudeDec,
                'lng' => $meteoStation->geographicCoordinates->longitudeDec
            );
            $this->defaults['coords'] = $coords;
        }
        if ($this->form->name !== null) {
            $this->form->setDefaults($this->defaults);
        }
    }

    public function setEdit($edit = false) {
        $this->edit = $edit;
        if ($this->form->name !== null) {
            $this->form['send']->caption = 'Update';
        }
    }

}
