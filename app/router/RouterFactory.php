<?php

namespace App;

use Nette,
    Nette\Application\Routers\RouteList,
    Nette\Application\Routers\Route,
    Nette\Application\Routers\CliRouter;

class RouterFactory {

    /**
     * @return Nette\Application\IRouter
     */
    public static function createRouter() {
        $router = new RouteList();

        if (PHP_SAPI === 'cli') {
            /* LIVE DATA */
            $router[] = new Route('video-camera/<name>/video.jpg?time=<time>', 'VideoCameras:video');
            $router[] = new Route('video-camera-secure/<name=>/video.jpg?time=<time=>', 'VideoCameras:videoSecure');
            $router[] = new Route('meteo-station/<name>/status.xml?time=<time>', 'MeteoStations:xml');
            $router[] = new Route('meteo-station-secure/<name=>/status.xml?time=<time=>', 'MeteoStations:xmlSecure');

            /* PREVIEW DATA */
            $router[] = new Route('images/preview/video-camera/<name>.jpg', 'VideoCameras:videoPreview');
            $router[] = new Route('images/preview/meteo-station/<name>.xml', 'MeteoStations:xmlPreview');

            /* ARCHIVE DATA */
            $router[] = new Route('images/archive/video-camera/<year>/<name>/[<date>.jpg]', 'VideoCameras:videoArchive');
            $router[] = new Route('images/archive/meteo-station/<year>/<name>/[<date>.xml]', 'MeteoStations:xmlArchive');

            /* SITEMAP */

            $router[] = new Route('/sitemap.xml', 'Cli:Setup:siteMap');

            /* PRESENTERS */
            $router[] = new CliRouter(array('action' => 'Cli:Setup:apache2',));
            $router[] = new CliRouter(array('action' => 'Cli:Setup:letsEncrypt',));
            $router[] = new CliRouter(array('action' => 'Cli:Setup:MrtgVideoCameras',));
            $router[] = new CliRouter(array('action' => 'Cli:Setup:MrtgMeteoStations',));
            $router[] = new CliRouter(array('action' => 'Cli:Setup:SiteMap',));
            $router[] = new CliRouter(array('action' => 'Cli:Cron:data',));
            $router[] = new CliRouter(array('action' => 'Cli:Cron::archive',));
            $router[] = new CliRouter(array('action' => 'Cli:Cron::cleanup',));
            $router[] = new CliRouter(array('action' => 'Cli:Cron::commit',));

            /* DEFAULT */
            $router[] = new Route('[<lang ' . \LiveTranslator\Language::REGULAR_LANGUAGE . '?>/]<presenter>/<action>[/<id>]', ':Homepage:default');
        } else {
            /* SOURCE DATA */
            $router[] = new Route('/video-camera/<name>/video.jpg?time=<time>', 'VideoCameras:video');
            $router[] = new Route('/video-camera-secure/<name=>/video.jpg?time=<time=>', 'VideoCameras:videoSecure');
            $router[] = new Route('/meteo-station/<name>/status.xml?time=<time>', 'MeteoStations:xml');
            $router[] = new Route('/meteo-station-secure/<name=>/status.xml?time=<time=>', 'MeteoStations:xmlSecure');

            /* WEATHER UNDEGROUND */
            $router[] = new Route('/weatherstation/updateweatherstation.php', 'MeteoStations:UpdateWeatherStation');


            /* LIVE DATA */




            /* PREVIEW DATA */
            $router[] = new Route('/images/preview/video-camera/<name>.jpg', 'VideoCameras:videoPreview');
            $router[] = new Route('/images/preview/meteo-station/<name>.xml', 'MeteoStations:xmlPreview');

            /* IMAGE HELPER */
            $router[] = new Route('/images/watermark/watermark.png', 'Watermark:image');

            /* ARCHIVE DATA */
            $router[] = new Route('/images/archive/video-camera/<year>/<name>/[<date>.jpg]', 'VideoCameras:videoArchive');
            $router[] = new Route('/images/archive/meteo-station/<year>/<name>/[<date>.xml]', 'MeteoStations:xmlArchive');

            /* RRD */
            $router[] = new Route('/[<lang ' . \LiveTranslator\Language::REGULAR_LANGUAGE . '?>/]rrd/video-camera/<id>/<probe>-<period>.pn', 'Rrd:VideocameraGraphImage');
            $router[] = new Route('/[<lang ' . \LiveTranslator\Language::REGULAR_LANGUAGE . '?>/]rrd/meteo-station/<id>/<probe>-<period>.pn', 'Rrd:MeteostationGraphImage');
            /* MRTG */
            $router[] = new Route('/mrtg/video-camera/<name>/<nameProbe>-<probe>.html', 'Mrtg:VideocameraHistory');
            $router[] = new Route('/mrtg/video-camera/<name>/<nameProbe>-<probe>-<period>.png', 'Mrtg:VideocameraGraph');
            $router[] = new Route('/mrtg/meteo-station/<name>/<nameProbe>-<probe>.html', 'Mrtg:MeteostationHistory');
            $router[] = new Route('/mrtg/meteo-station/<name>/<nameProbe>-<probe>-<period>.png', 'Mrtg:MeteostationGraph');

            /* INFO */


            /* DEFAULT */
            $router[] = new Route('[<lang ' . \LiveTranslator\Language::REGULAR_LANGUAGE . '?>/]<presenter>/<action>[/<id>]', 'Homepage:default');
        }

        return $router;
    }

}
