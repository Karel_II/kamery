<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Control;

use NetteAddons\Application\UI\BaseComponentControl;
use App\Model\Weather\Weather;

/**
 * Description of DataPresentControl
 *
 * @author Karel
 */
class DataPresentControl extends BaseComponentControl {

    public function renderWeatherInfo(Weather $weather) {
        $this->setViewFromFunctionName(__FUNCTION__);
        $this->template->weather = $weather;
        $this->template->giom3000Data = $weather;
        $this->template->render();
    }

    public function renderWeatherInPocasi(Weather $weather) {
        $this->setViewFromFunctionName(__FUNCTION__);
        $this->template->weather = $weather;
        $this->template->render();
    }

    public function renderWeatherJson(Weather $weather) {
        $this->setViewFromFunctionName(__FUNCTION__);
        $this->template->weather = $weather;
        $this->template->render();
    }

    public function renderWeatherXML(Weather $weather) {
        $this->setViewFromFunctionName(__FUNCTION__);
        $this->template->weather = $weather;
        $this->template->render();
    }

    public function renderWeatherXMLItem($name, $description, $value, $unit = NULL) {
        $this->setViewFromFunctionName(__FUNCTION__);
        $this->template->numberDigits = 3;
        $this->template->name = $name;
        $this->template->description = $description;
        $this->template->value = $value;
        $this->template->unit = $unit;
        $this->template->render();
    }

    public function renderWeatherDetail(Weather $weather) {
        $this->setViewFromFunctionName(__FUNCTION__);
        $this->template->weather = $weather;
        $this->template->giom3000Data = $weather;
        $this->template->render();
    }

    public function renderWeatherDetailItem($icon, $name, $value, $unit = NULL) {
        $this->setViewFromFunctionName(__FUNCTION__);
        $this->template->numberDigits = 3;
        $this->template->icon = $icon;
        $this->template->name = $name;
        $this->template->value = $value;
        $this->template->unit = $unit;
        $this->template->render();
    }

    public function renderWeatherInfoItem($icon, $name, $value, $unit = NULL) {
        $this->setViewFromFunctionName(__FUNCTION__);
        $this->template->numberDigits = 3;
        $this->template->icon = $icon;
        $this->template->name = $name;
        $this->template->value = $value;
        $this->template->unit = $unit;
        $this->template->render();
    }

    public function renderWeatherWindDirection($icon, $name, $value, $deg) {
        $this->setViewFromFunctionName(__FUNCTION__);
        $this->template->numberDigits = 3;
        $this->template->icon = $icon;
        $this->template->name = $name;
        $this->template->value = $value;
        $this->template->deg = $deg;
        $this->template->render();
    }

    public function renderCompass($height = 50, $width = 50, $strokeWidth = 2, $angle = 0) {
        $this->setViewFromFunctionName(__FUNCTION__);
        $this->template->height = $height;
        $this->template->width = $width;
        $this->template->strokeWidth = $strokeWidth;
        $this->template->colorBackground = '#337ab7';
        $this->template->colorScale = 'white';
        $this->template->colorNeedleIn = 'orange';
        $this->template->colorNeedleOut = 'yellow';
        $this->template->angle = $angle;
        $this->template->cacheKey = sha1(implode(',', [$height, $width, $strokeWidth, $angle]));
        $this->template->render();
    }

}
