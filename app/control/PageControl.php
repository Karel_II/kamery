<?php

namespace App\Control;

use NetteAddons\Application\UI\BaseComponentControl;
use Nette\Utils\Paginator;

/**
 * Description of PageControl
 *
 * @author knovak
 */
class PageControl extends BaseComponentControl {

    /** @var string Title of Control */
    protected $title = 'Page';

    /**
     *
     * @var \Nette\Utils\Paginator 
     * @persistent
     */
    public $paginator;

    /**
     * Loads state informations.
     * @param  array
     * @return void
     */
    public function loadState(array $params) {
        parent::loadState($params);
        $this->getPaginator();
    }

    public function __construct($title = null) {
        if (isset($title)) {
            $this->title = $title;
        }
        parent::__construct(); // tenhle řádek je velice důležitý
    }

    private function getSessionSection() {
        $presenter = $this->getPresenter();
        $session = $presenter->getSession();
        $key = sha1($presenter->getName() . $presenter->getAction() . $presenter->getParameter('id'));
        return $session->getSection($key);
    }

    private function increasePage($step) {
        $this->paginator->page += $step;
    }

    private function decreasePage($step) {
        $this->paginator->page -= $step;
    }

    private function setPage($page) {
        $this->paginator->page = $page;
    }

    /**
     * @return \Nette\Utils\Paginator
     */
    public function getPaginator() {

        $sessionSection = $this->getSessionSection();
        if (isset($sessionSection->paginator)) {
            $this->paginator = $sessionSection->paginator;
        }
        if (!$this->paginator) {
            $this->paginator = new Paginator;
            $sessionSection->paginator = $this->paginator;
        }
        return $this->paginator;
    }

    /**
     * Renders paginator.
     * @return void
     */
    public function render() {
        $paginator = $this->getPaginator();
        if ($paginator->pageCount < 2) {
            $steps = array($paginator->page);
        } else {
            $arr = range(max($paginator->firstPage, $paginator->page - 3), min($paginator->lastPage, $paginator->page + 3));
            $count = 4;
            $quotient = ($paginator->pageCount - 1) / $count;
            for ($i = $paginator->firstPage; $i <= $paginator->pageCount; $i += $quotient) {
                $arr[] = round($i);
            }
            sort($arr);
            $steps = array_unique($arr);
        }
        $this->template->steps = $steps;
        $this->template->log10 = log10($paginator->pageCount);
        $this->template->paginator = $paginator;
        $this->template->render();
    }

    /**
     * Renders paginator.
     * @return void
     */
    public function renderAtom() {
        $this->setViewFromFunctionName(__FUNCTION__);
        $paginator = $this->getPaginator();
        $this->template->paginator = $paginator;
        $this->template->render();
    }

    public function handleFirst() {
        $this->setPage($this->template->paginator->firstPage);
        $this->redirect('this');
    }

    public function handleLast() {
        $this->setPage($this->template->paginator->lastPage);
        $this->redirect('this');
    }

    public function handleNext($count = 1) {
        $this->increasePage($count);
        $this->redirect('this');
    }

    public function handlePrev($count = 1) {
        $this->decreasePage($count);
        $this->redirect('this');
    }

    public function handleNavigate($page) {
        $this->setPage($page);
        $this->redirect('this');
    }

}
