<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteAddons\Bridges\DatabaseRrdDI;

use Nette;

/**
 * Description of DatabaseRrdExtension
 *
 * @author karel.novak
 */
class DatabaseRrdExtension extends Nette\DI\CompilerExtension {

    public $databaseDefaults = [
        'directoryDatabase' => null,
        'directoryGraph' => null,
        'daemon' => null,
        'debugger' => true,
        'autowired' => null,
    ];

    /** @var bool */
    private $debugMode;

    public function __construct($debugMode = false) {
        $this->debugMode = $debugMode;
    }

    public function loadConfiguration() {
        $configs = $this->getConfig();
        foreach ($configs as $k => $v) {
            if (is_scalar($v)) {
                $configs = ['default' => $configs];
                break;
            }
        }

        $defaults = $this->databaseDefaults;
        $defaults['autowired'] = true;
        foreach ((array) $configs as $name => $config) {
            if (!is_array($config)) {
                continue;
            }
            $config = $this->validateConfig($defaults, $config, $this->prefix($name));
            $defaults['autowired'] = false;
            $this->setupDatabase($config, $name);
        }
    }

    private function setupDatabase($config, $name) {
        $builder = $this->getContainerBuilder();

        $connectionRrd = $builder->addDefinition($this->prefix("$name.connection"))
                ->setFactory(\NetteAddons\DatabaseRrd\Connection::class, [$config['directoryDatabase'], $config['directoryGraph'], $config['daemon']])
                ->setAutowired($config['autowired']);
        /*

          if ($config['debugger']) {
          $connection->addSetup('@Tracy\BlueScreen::addPanel', [
          'Nette\Bridges\DatabaseTracy\ConnectionPanel::renderException',
          ]);
          if ($this->debugMode) {
          $connection->addSetup('Nette\Database\Helpers::createDebugPanel', [$connection, !empty($config['explain']), $name]);
          }
          }

          } */
    }

}
