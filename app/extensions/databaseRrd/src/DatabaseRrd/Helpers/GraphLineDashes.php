<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Rrd\Helpes;

/**
 * Description of GraphLineDashes
 *
 * @author karel.novak
 */
class GraphLineDashes {

    /**
     *
     * @var array|boolean 
     */
    protected $dots = NULL;

    /**
     *
     * @var integer|NULL 
     */
    protected $dashOffset = NULL;

    /**
     * 
     * @param array|boolean $dots
     * @param integer $dashOffset
     */
    public function __construct($dots, $dashOffset = NULL) {
        $this->dots = $dots;
        $this->dashOffset = $dashOffset;
    }

    /**
     * 
     * @return string
     */
    public function __toString() {
        if ($this->dots === TRUE) {
            return ":dashes";
        } elseif (is_array($this->dots)) {
            $return = ":dashes=" . join(',', $this->dots);
        } ELSE {
            return '';
        }
        if (is_integer($this->dashOffset)) {
            $return = ":dash-offset=offset" . join(',', $this->dashOffset);
        }
        return $return;
    }

}
