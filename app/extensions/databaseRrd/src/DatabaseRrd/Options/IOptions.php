<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Rrd\Options;

/**
 *
 * @author Karel
 */
interface IOptions {

    /**
     * 
     * @return string
     */
    public function getTableName();

    /**
     * 
     * @return string
     */
    public function getDatabaseName();

    /**
     * @return array
     */
    public function getOptions(): array;

    /**
     * 
     * @return bool
     */
    public function hasDaemon(): bool;

    /**
     * 
     * @return string
     */
    public function getDaemon();

    /**
     * 
     * @param string $daemon
     * @return $this
     */
    public function setDaemon($daemon);

    /**
     * 
     * @param string $databaseName
     * @return $this
     */
    public function setDatabaseName($databaseName);

    /**
     * 
     * @param string $tableName
     * @return $this
     */
    public function setTableName($tableName);
}
