<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Rrd\Options;

/**
 * Description of OptionsCreate
 *
 * @author Karel
 */
class OptionsCreate extends BaseOptions {

    use Extensions\TCreateOptions,
        Extensions\TCreateOptionsDataSource,
        Extensions\TCreateOptionsRoundRobinArchives;
}
