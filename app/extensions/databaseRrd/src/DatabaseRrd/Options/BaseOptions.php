<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Rrd\Options;

/**
 * Description of BaseOptions
 *
 * @author Karel
 * @property string $daemon Address of the rrdcached daemon. For a list of accepted formats, see the -l option in the rrdcached manual.
 */
abstract class BaseOptions implements IOptions {

    const DATABASE_FILE_EXTENSION = '.rrd';

    protected $directoryDatabase = NULL;
    protected $directoryGraph = NULL;
    protected $tableName = NULL;
    protected $databaseName = NULL;

    /**
     * Address of the rrdcached daemon. For a list of accepted formats, see the -l option in the rrdcached manual.
     * @var string 
     */
    protected $daemon = NULL;

    /**
     *
     * @var callable[]  function (): string; 
     */
    protected $onGetOptions;

    /**
     * 
     * @return string
     */
    public function getTableName() {
        return $this->tableName;
    }

    /**
     * 
     * @return string
     */
    public function getDatabaseName() {
        return $this->databaseName;
    }

    /**
     * 
     * @return string
     */
    public function getDaemon() {
        return $this->daemon;
    }

    /**
     * 
     * @return string
     */
    public function getDirectoryDatabase() {
        return $this->directoryDatabase;
    }

    /**
     * 
     * @return string
     */
    public function getDirectoryGraph() {
        return $this->directoryGraph;
    }

    /**
     * 
     * @param string $databaseName
     * @return \SplFileInfo
     */
    public function getFileDatabase() {
        return new \SplFileInfo($this->directoryDatabase . DIRECTORY_SEPARATOR . $this->databaseName . DIRECTORY_SEPARATOR . $this->tableName . self::DATABASE_FILE_EXTENSION);
    }

    /**
     * 
     * @param string $databaseName
     * @return $this
     */
    public function setDatabaseName($databaseName) {
        $this->databaseName = $databaseName;
        return $this;
    }

    /**
     * 
     * @param string $tableName
     * @return $this
     */
    public function setTableName($tableName) {
        $this->tableName = $tableName;
        return $this;
    }

    /**
     * 
     * @param string $directoryDatabase
     * @return $this
     */
    public function setDirectoryDatabase($directoryDatabase) {
        $this->directoryDatabase = $directoryDatabase;
        return $this;
    }

    /**
     * 
     * @param string $directoryGraph
     * @return $this
     */
    public function setDirectoryGraph($directoryGraph) {
        $this->directoryGraph = $directoryGraph;
        return $this;
    }

    /**
     * 
     * @param string $daemon
     * @return $this
     */
    public function setDaemon($daemon) {
        $this->daemon = $daemon;
        return $this;
    }

    /**
     * 
     * @return bool
     */
    public function hasDaemon(): bool {
        return ($this->daemon !== NULL);
    }

    /**
     * 
     * @return bool
     */
    public function isConnected(): bool {
        return ($this->tableName !== NULL && $this->directoryDatabase !== NULL);
    }

    public function mergeBaseOptions(IOptions $options) {
        $this->setDaemon($options->getDaemon());
        $this->setDirectoryDatabase($options->getDirectoryDatabase());
        $this->setDirectoryGraph($options->getDirectoryGraph());
        $this->setTableName($options->getTableName());
        $this->setDatabaseName($options->getDatabaseName());
        return $this;
    }

    /**
     * 
     * @param float|null|string $value
     * @return float|null
     * @throws \Exception
     */
    protected function checkValue($value) {
        if (is_numeric($value)) {
            return $value;
        } elseif ($value === NULL || empty($value)) {
            return 'U';
        }
        throw new \Exception('Invalid value : ' . $value);
    }

    /**
     * 
     * @return array
     */
    public function getOptions(): array {
        $return = array();
        if ($this->daemon !== NULL) {
            $return[] = '--daemon';
            $return[] = $this->daemon;
        }
        if ($this->onGetOptions && $this->onGetOptions !== NULL) {
            foreach ($this->onGetOptions as $callback) {
                $return = array_merge($return, call_user_func($callback));
            }
        }
        return $return;
    }

}
