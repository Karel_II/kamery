<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Rrd\Options;

/**
 * Description of OptionsGraph
 *
 * @author Karel
 */
class OptionsGraph extends BaseOptions {

    use Extensions\TGraphOptions,
        Extensions\TGraphOptionsDataDefinition,
        Extensions\TGraphOptionsVariableCalculations,
        Extensions\TGraphOptionsPrintElements,
        Extensions\TGraphOptionsGraphElements;

    public function getFileGraph() {
        return new \SplFileInfo($this->directoryGraph . DIRECTORY_SEPARATOR . $this->databaseName . DIRECTORY_SEPARATOR . $this->tableName . '_' . strtolower($this->title) . '_' . substr(sha1($this->step . $this->end . $this->start), -5) . '.' . strtolower($this->imgformat));
    }

}
