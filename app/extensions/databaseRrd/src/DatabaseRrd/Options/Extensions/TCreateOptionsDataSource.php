<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Rrd\Options\Extensions;

use App\Model\RepositoryRrd\RRDParameterException;
/**
 *
 * @author Karel
 */
trait TCreateOptionsDataSource {

    /**
     *
     * @var array 
     */
    protected $dataElements = array();

    /**
     * 
     * @param string $name
     * @param string $dataSourceType
     * @param int $heartBeat
     * @param int $min
     * @param int $max
     * @return $this
     * @throws \Exception
     */
    public function dataSource($name, $dataSourceType, $heartBeat, $min = NULL, $max = NULL) {
        if (!preg_match('/^[a-zA-Z0-9_]{1,19}(=[a-zA-Z0-9_]{1,19}(\[[0-9]+\])?)?$/', $name)) {
            throw new RRDParameterException('Wrong name! :' . $name);
        }
        if (!preg_match('/^[0-9_]+(s|m|h|d|w|M|y)?$/', $heartBeat)) {
            throw new RRDParameterException('Wrong heartBeat! :' . $heartBeat);
        }
        if (!preg_match('/^(GAUGE|COUNTER|DERIVE|DCOUNTER|DDERIVE|ABSOLUTE)$/', $dataSourceType)) {
            throw new RRDParameterException('Wrong dataSourceType! :' . $dataSourceType);
        }
        $this->dataElements[] = 'DS:' . $name . ':' . $dataSourceType . ':' . $heartBeat . ':' . $this->checkValue($min) . ':' . $this->checkValue($max);
        $this->onGetOptions['onGetOptionsTCreateOptionsDataSource'] = [$this, 'onGetOptionsTCreateOptionsDataSource'];
        return $this;
    }

    /**
     * 
     * @param string $name
     * @param string $rpnExpression
     * @return $this
     * @throws \Exception
     */
    public function dataSourceCompute($name, $rpnExpression) {
        if (!preg_match('/^[a-zA-Z0-9_]{1,19}(=[a-zA-Z0-9_]{1,19}(\[[0-9]+\])?)?$/', $name)) {
            throw new RRDParameterException('Wrong name! :' . $name);
        }
        $this->dataElements[] = 'DS:' . $name . ':COMPUTE:' . $rpnExpression;
        $this->onGetOptions['onGetOptionsTCreateOptionsDataSource'] = [$this, 'onGetOptionsTCreateOptionsDataSource'];
        return $this;
    }

    /**
     * 
     * @return array
     */
    protected function onGetOptionsTCreateOptionsDataSource() {
        return $this->dataElements;
    }

}
