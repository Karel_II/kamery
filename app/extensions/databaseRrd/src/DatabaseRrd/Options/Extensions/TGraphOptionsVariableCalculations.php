<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Rrd\Options\Extensions;

/**
 *
 * @author Karel
 */
trait TGraphOptionsVariableCalculations {

    /**
     *
     * @var array 
     */
    protected $variableCalculationsElements = array();

    public function cdef($valueName, $rpnExpression) {
        $this->variableCalculationsElements[] = "CDEF:$valueName=$rpnExpression";
        $this->onGetOptions['onGetOptionsTGraphOptionsVariableCalculations'] = [$this, 'onGetOptionsTGraphOptionsVariableCalculations'];
        return $this;
    }

    public function vdef($valueName, $rpnExpression) {
        $this->variableCalculationsElements[] = "VDEF:$valueName=$rpnExpression";
        $this->onGetOptions['onGetOptionsTGraphOptionsVariableCalculations'] = [$this, 'onGetOptionsTGraphOptionsVariableCalculations'];
        return $this;
    }

    /**
     * 
     * @return array
     */
    protected function onGetOptionsTGraphOptionsVariableCalculations() {
        return $this->variableCalculationsElements;
    }

}
