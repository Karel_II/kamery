<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Rrd\Options\Extensions;

/**
 *
 * @author Karel
 */
trait TFetchOptions {

    /**
     * the consolidation function that is applied to the data you want to fetch (AVERAGE,MIN,MAX,LAST)
     * @var string
     */
    protected $consolidationFunction = NULL;

    /**
     * the interval you want the values to have (seconds per value). 
     * An optional suffix may be used (e.g. 5m instead of 300 seconds). 
     * rrdfetch will try to match your request, but it will return data even if no absolute match is possible. See "RESOLUTION INTERVAL".
     * @var string 
     */
    protected $resolution = NULL;

    /**
     * start of the time series. A time in seconds since epoch (1970-01-01) is required. Negative numbers are relative to the current time. By default, one day worth of data will be fetched. See also "AT-STYLE TIME SPECIFICATION" for a detailed explanation on ways to specify the start time.
     * @var string 
     */
    protected $start = NULL;

    /**
     * the end of the time series in seconds since epoch. See also "AT-STYLE TIME SPECIFICATION" for a detailed explanation of how to specify the end time.
     * @var string 
     */
    protected $end = NULL;

    /**
     * Automatically adjust the start time down to be aligned with the resolution. The end-time is adjusted by the same amount. This avoids the need for external calculations described in "RESOLUTION INTERVAL", though if a specific RRA is desired this will not ensure the start and end fall within its bounds.
     * @var boolean 
     */
    protected $alignStart = NULL;

    public function setConsolidationFunction($consolidationFunction) {
        if (preg_match('/(AVERAGE|MIN|MAX|LAST)/', $consolidationFunction)) {
            $this->consolidationFunction = $consolidationFunction;
            $this->onGetOptions['onGetOptionsTFetchOptions'] = [$this, 'onGetOptionsTFetchOptions'];
        }
        return $this;
    }

    public function setResolution($resolution) {
        $this->resolution = $resolution;
        $this->onGetOptions['onGetOptionsTFetchOptions'] = [$this, 'onGetOptionsTFetchOptions'];
        return $this;
    }

    public function setStart($start) {
        $this->start = $start;
        $this->onGetOptions['onGetOptionsTFetchOptions'] = [$this, 'onGetOptionsTFetchOptions'];
        return $this;
    }

    public function setEnd($end) {
        $this->end = $end;
        $this->onGetOptions['onGetOptionsTFetchOptions'] = [$this, 'onGetOptionsTFetchOptions'];
        return $this;
    }

    public function setAlignStart($alignStart) {
        $this->alignStart = $alignStart;
        $this->onGetOptions['onGetOptionsTFetchOptions'] = [$this, 'onGetOptionsTFetchOptions'];
        return $this;
    }

    /**
     * 
     * @return array
     */
    public function onGetOptionsTFetchOptions() {
        $return = array();
        if ($this->consolidationFunction !== NULL) {
            $return[] = $this->consolidationFunction;
        }
        if ($this->start !== NULL) {
            $return[] = '--start';
            $return[] = $this->start;
        }
        if ($this->end !== NULL) {
            $return[] = '--end';
            $return[] = $this->end;
        }
        if ($this->resolution !== NULL) {
            $return[] = '--resolution';
            $return[] = $this->resolution;
        }
        if ($this->alignStart === TRUE) {
            $return[] = '--align-start';
        }
        return $return;
    }

}
