<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Rrd\Options\Extensions;

/**
 *
 * @author Karel
 */
trait TGraphOptions {

    /**
     * Time range [-s|--start time] [-e|--end time] [-S|--step seconds]
     * The start and end of the time series you would like to display, and which RRA the data should come from. 
     * Defaults are: 1 day ago until now, with the best possible resolution. Start and end can be specified in several formats, see "AT-STYLE TIME SPECIFICATION" in rrdfetch and rrdgraph_examples. 
     * By default, rrdtool graph calculates the width of one pixel in the time domain and tries to get data from an RRA with that resolution. With the step option you can alter this behavior. If you want rrdtool graph to get data at a one-hour resolution from the RRD, set step to 3'600. Note: a step smaller than one pixel will silently be ignored.
     * For non-image --imgformats see "OUTPUT FORMAT" in rrdxport for details on how this affects the output.
     * @var string 
     */
    protected $start = NULL;

    /**
     * Time range [-s|--start time] [-e|--end time] [-S|--step seconds]
     * The start and end of the time series you would like to display, and which RRA the data should come from. 
     * Defaults are: 1 day ago until now, with the best possible resolution. Start and end can be specified in several formats, see "AT-STYLE TIME SPECIFICATION" in rrdfetch and rrdgraph_examples. 
     * By default, rrdtool graph calculates the width of one pixel in the time domain and tries to get data from an RRA with that resolution. With the step option you can alter this behavior. If you want rrdtool graph to get data at a one-hour resolution from the RRD, set step to 3'600. Note: a step smaller than one pixel will silently be ignored.
     * For non-image --imgformats see "OUTPUT FORMAT" in rrdxport for details on how this affects the output.n
     * @var string 
     */
    protected $end = NULL;

    /**
     * Time range [-s|--start time] [-e|--end time] [-S|--step seconds]
     * The start and end of the time series you would like to display, and which RRA the data should come from. 
     * Defaults are: 1 day ago until now, with the best possible resolution. Start and end can be specified in several formats, see "AT-STYLE TIME SPECIFICATION" in rrdfetch and rrdgraph_examples. 
     * By default, rrdtool graph calculates the width of one pixel in the time domain and tries to get data from an RRA with that resolution. With the step option you can alter this behavior. If you want rrdtool graph to get data at a one-hour resolution from the RRD, set step to 3'600. Note: a step smaller than one pixel will silently be ignored.
     * For non-image --imgformats see "OUTPUT FORMAT" in rrdxport for details on how this affects the output.
     * @var string 
     */
    protected $step = NULL;

    /**
     * Labels [-t|--title string]
     * A horizontal string placed at the top of the graph which may be separated into multiple lines using <br/> or \n
     * @var string 
     */
    protected $title = NULL;

    /**
     * Labels [-v|--vertical-label string]
     * A vertical string placed at the left hand of the graph.
     * @var string 
     */
    protected $verticalLabel = NULL;

    /**
     * Size [-w|--width pixels] [-h|--height pixels] [-j|--only-graph] [-D|--full-size-mode]
     * By default, the width and height of the canvas (the part with the actual data and such). This defaults to 400 pixels by 100 pixels.
     * @var integer 
     */
    protected $width = NULL;

    /**
     * Size [-w|--width pixels] [-h|--height pixels] [-j|--only-graph] [-D|--full-size-mode]
     * By default, the width and height of the canvas (the part with the actual data and such). This defaults to 400 pixels by 100 pixels.
     * @var integer 
     */
    protected $height = NULL;

    /**
     * Size [-w|--width pixels] [-h|--height pixels] [-j|--only-graph] [-D|--full-size-mode]
     * If you specify the --only-graph option and set the height < 32 pixels you will get a tiny graph image (thumbnail) to use as an icon for use in an overview, for example. All labeling will be stripped off the graph.
     * @var bool 
     */
    protected $onlyGraph = FALSE;

    /**
     * Size [-w|--width pixels] [-h|--height pixels] [-j|--only-graph] [-D|--full-size-mode]
     * If you specify the --full-size-mode option, the width and height specify the final dimensions of the output image and the canvas is automatically resized to fit.
     * @var bool 
     */
    protected $fullSizeMode = FALSE;

    /**
     * Limits [-u|--upper-limit value] [-l|--lower-limit value] [-r|--rigid] [--allow-shrink]
     * By default the graph will be autoscaling so that it will adjust the y-axis to the range of the data. You can change this behavior by explicitly setting the limits. The displayed y-axis will then range at least from lower-limit to upper-limit. Autoscaling will still permit those boundaries to be stretched unless the rigid option is set. allow-shrink alters behaivor of rigid by allowing auto down scale, graph will not overrun user specified limits.
     * @var int 
     */
    protected $upperLimit = NULL;

    /**
     * Limits [-u|--upper-limit value] [-l|--lower-limit value] [-r|--rigid] [--allow-shrink]
     * By default the graph will be autoscaling so that it will adjust the y-axis to the range of the data. You can change this behavior by explicitly setting the limits. The displayed y-axis will then range at least from lower-limit to upper-limit. Autoscaling will still permit those boundaries to be stretched unless the rigid option is set. allow-shrink alters behaivor of rigid by allowing auto down scale, graph will not overrun user specified limits.
     * @var int 
     */
    protected $lowerLimit = NULL;

    /**
     * Limits [-u|--upper-limit value] [-l|--lower-limit value] [-r|--rigid] [--allow-shrink]
     * By default the graph will be autoscaling so that it will adjust the y-axis to the range of the data. You can change this behavior by explicitly setting the limits. The displayed y-axis will then range at least from lower-limit to upper-limit. Autoscaling will still permit those boundaries to be stretched unless the rigid option is set. allow-shrink alters behaivor of rigid by allowing auto down scale, graph will not overrun user specified limits.
     * @var bool 
     */
    protected $rigid = FALSE;

    /**
     * Limits [-u|--upper-limit value] [-l|--lower-limit value] [-r|--rigid] [--allow-shrink]
     * By default the graph will be autoscaling so that it will adjust the y-axis to the range of the data. You can change this behavior by explicitly setting the limits. The displayed y-axis will then range at least from lower-limit to upper-limit. Autoscaling will still permit those boundaries to be stretched unless the rigid option is set. allow-shrink alters behaivor of rigid by allowing auto down scale, graph will not overrun user specified limits.
     * @var bool 
     */
    protected $allowShrink = FALSE;

    /**
     *
     * [-A|--alt-autoscale] - BOTH
     * 
     * Sometimes the default algorithm for selecting the y-axis scale is not satisfactory. 
     * Normally the scale is selected from a predefined set of ranges and this fails miserably when you need to graph something like 260 + 0.001 * sin(x). 
     * This option calculates the minimum and maximum y-axis from the actual minimum and maximum data values. 
     * Our example would display slightly less than 260-0.001 to slightly more than 260+0.001 (this feature was contributed by Sasha Mikheev).
     * 
     * [-J|--alt-autoscale-min] - MIN
     * This option will only affect the minimum value. The maximum value, if not defined on the command line, will be 0. This option can be useful when graphing router traffic when the WAN line uses compression, and thus the throughput may be higher than the WAN line speed.
     * 
     * [-M|--alt-autoscale-max] - MAX
     * This option will only affect the maximum value. The minimum value, if not defined on the command line, will be 0. This option can be useful when graphing router traffic when the WAN line uses compression, and thus the throughput may be higher than the WAN line speed.
     * 
     * @var string (BOTH|MIN|MAX) 
     */
    protected $altAutoscale = NULL;

    /**
     * [-N|--no-gridfit]     
     * In order to avoid anti-aliasing blurring effects RRDtool snaps points to device resolution pixels, this results in a crisper appearance. If this is not to your liking, you can use this switch to turn this behavior off.
     * Grid-fitting is turned off for PDF, EPS, SVG output by default.
     * @var bool 
     */
    protected $noGridfit = FALSE;

    /*

      X-Axis
      [-x|--x-grid GTM:GST:MTM:MST:LTM:LST:LPR:LFM]

      [-x|--x-grid none]

      The x-axis label is quite complex to configure. If you don't have very special needs it is probably best to rely on the auto configuration to get this right. You can specify the string none to suppress the grid and labels altogether.

      The grid is defined by specifying a certain amount of time in the ?TM positions. You can choose from SECOND, MINUTE, HOUR, DAY, WEEK, MONTH or YEAR. Then you define how many of these should pass between each line or label. This pair (?TM:?ST) needs to be specified for the base grid (G??), the major grid (M??) and the labels (L??). For the labels you also must define a precision in LPR and a strftime format string in LFM. LPR defines where each label will be placed. If it is zero, the label will be placed right under the corresponding line (useful for hours, dates etcetera). If you specify a number of seconds here the label is centered on this interval (useful for Monday, January etcetera).

      --x-grid MINUTE:10:HOUR:1:HOUR:4:0:%X
      This places grid lines every 10 minutes, major grid lines every hour, and labels every 4 hours. The labels are placed under the major grid lines as they specify exactly that time.

      --x-grid HOUR:8:DAY:1:DAY:1:86400:%A
      This places grid lines every 8 hours, major grid lines and labels each day. The labels are placed exactly between two major grid lines as they specify the complete day and not just midnight.

      [--week-fmt strftime format string]

      By default rrdtool uses "Week %V" to render the week number. With this option you can define your own format, without completely overriding the xaxis format.

      Y-Axis
      [-y|--y-grid grid step:label factor]

      [-y|--y-grid none]

      Y-axis grid lines appear at each grid step interval. Labels are placed every label factor lines. You can specify -y none to suppress the grid and labels altogether. The default for this option is to automatically select sensible values.

      If you have set --y-grid to 'none' not only the labels get suppressed, also the space reserved for the labels is removed. You can still add space manually if you use the --units-length command to explicitly reserve space.

      [--left-axis-formatter formatter-name]

      Specify what formatter to use to render axis values.

      numeric
      The default, values are expressed as numeric quantities.

      timestamp
      Values are interpreted as unix timestamps (number of seconds since 1970-01-01 00:00:00 UTC) and expressed using strftime format (default is '%Y-%m-%d %H:%M:%S'). See also --units-length and --left-axis-format.

      duration
      Values are interpreted as duration in milliseconds. Formatting follows the rules of valstrfduration qualified PRINT/GPRINT. See rrdgraph_graph.

      [--left-axis-format format-string]

      By default the format of the axis labels gets determined automatically. If you want to do this yourself, use this option with the same %lf arguments you know from the PRINT and GPRINT commands, or others if using different formatter.

      [-Y|--alt-y-grid]

      Place the Y grid dynamically based on the graph's Y range. The algorithm ensures that you always have a grid, that there are enough but not too many grid lines, and that the grid is metric. That is the grid lines are placed every 1, 2, 5 or 10 units. This parameter will also ensure that you get enough decimals displayed even if your graph goes from 69.998 to 70.001. (contributed by Sasha Mikheev).

      [-o|--logarithmic]

      Logarithmic y-axis scaling.

      [-X|--units-exponent value]

      This sets the 10**exponent scaling of the y-axis values. Normally, values will be scaled to the appropriate units (k, M, etc.). However, you may wish to display units always in k (Kilo, 10e3) even if the data is in the M (Mega, 10e6) range, for instance. Value should be an integer which is a multiple of 3 between -18 and 18 inclusively. It is the exponent on the units you wish to use. For example, use 3 to display the y-axis values in k (Kilo, 10e3, thousands), use -6 to display the y-axis values in u (Micro, 10e-6, millionths). Use a value of 0 to prevent any scaling of the y-axis values.

      This option is very effective at confusing the heck out of the default RRDtool autoscaling function and grid painter. If RRDtool detects that it is not successful in labeling the graph under the given circumstances, it will switch to the more robust --alt-y-grid mode.

      [-L|--units-length value]

      How many digits should RRDtool assume the y-axis labels to be? You may have to use this option to make enough space once you start fiddling with the y-axis labeling.

      [--units=si]

      With this option y-axis values on logarithmic graphs will be scaled to the appropriate units (k, M, etc.) instead of using exponential notation. Note that for linear graphs, SI notation is used by default.

      Right Y Axis
      [--right-axis scale:shift] [--right-axis-label label]

      A second axis will be drawn to the right of the graph. It is tied to the left axis via the scale and shift parameters. You can also define a label for the right axis.

      [--right-axis-formatter formatter-name]

      Specify what formatter to use to render axis values.

      numeric
      The default, values are expressed as numeric quantities.

      timestamp
      Values are interpreted as unix timestamps (number of seconds since 1970-01-01 00:00:00 UTC) and expressed using strftime format (default is '%Y-%m-%d %H:%M:%S'). See also --units-length and --right-axis-format.

      duration
      Values are interpreted as duration in milliseconds. Formatting follows the rules of valstrfduration qualified PRINT/GPRINT. See rrdgraph_graph.

      [--right-axis-format format-string]

      By default the format of the axis labels gets determined automatically. If you want to do this yourself, use this option with the same %lf arguments you know from the PRINT and GPRINT commands, or others if using different formatter.

      Legend
      [-g|--no-legend]

      Suppress generation of the legend; only render the graph.

      [-F|--force-rules-legend]

      Force the generation of HRULE and VRULE legends even if those HRULE or VRULE will not be drawn because out of graph boundaries (mimics behavior of pre 1.0.42 versions).

      [--legend-position=(north|south|west|east)]

      Place the legend at the given side of the graph. The default is south. In west or east position it is necessary to add line breaks manually.

      [--legend-direction=(topdown|bottomup|bottomup2)]

      Place the legend items in the given vertical order. The default is topdown. Using bottomup the legend items appear in the same vertical order as a stack of lines or areas. Using bottomup2 will keep leading and trailing COMMENT lines in order, this might be useful for generators that use them for table headers and the like.

      Miscellaneous
      [-z|--lazy]

      Only generate the graph if the current graph is out of date or not existent. Note, that all the calculations will happen regardless so that the output of PRINT and graphv will be complete regardless. Note that the behavior of lazy in this regard has seen several changes over time. The only thing you can really rely on before RRDtool 1.3.7 is that lazy will not generate the graph when it is already there and up to date, and also that it will output the size of the graph.

      [-d|--daemon address]

      Address of the rrdcached daemon. If specified, a flush command is sent to the server before reading the RRD files. This allows the graph to contain fresh data even if the daemon is configured to cache values for a long time. For a list of accepted formats, see the -l option in the rrdcached manual.

      rrdtool graph [...] --daemon unix:/var/run/rrdcached.sock [...]
      [-f|--imginfo printfstr]

      After the image has been created, the graph function uses printf together with this format string to create output similar to the PRINT function, only that the printf function is supplied with the parameters filename, xsize and ysize. In order to generate an IMG tag suitable for including the graph into a web page, the command line would look like this:

      --imginfo '<IMG SRC="/img/%s" WIDTH="%lu" HEIGHT="%lu" ALT="Demo">'
      [-c|--color COLORTAG#rrggbb[aa]]

      Override the default colors for the standard elements of the graph. The COLORTAG is one of BACK background, CANVAS for the background of the actual graph, SHADEA for the left and top border, SHADEB for the right and bottom border, GRID, MGRID for the major grid, FONT for the color of the font, AXIS for the axis of the graph, FRAME for the line around the color spots, and finally ARROW for the arrow head pointing up and forward. Each color is composed out of three hexadecimal numbers specifying its rgb color component (00 is off, FF is maximum) of red, green and blue. Optionally you may add another hexadecimal number specifying the transparency (FF is solid). You may set this option several times to alter multiple defaults.

      A green arrow is made by: --color ARROW#00FF00

      [--grid-dash on:off]

      by default the grid is drawn in a 1 on, 1 off pattern. With this option you can set this yourself

      --grid-dash 1:3    for a dot grid

      --grid-dash 1:0    for uninterrupted grid lines
      [--border width]

      Width in pixels for the 3d border drawn around the image. Default 2, 0 disables the border. See SHADEA and SHADEB above for setting the border color.

      [--dynamic-labels]

      Pick the shape of the color marker next to the label according to the element drawn on the graph.

      [-m|--zoom factor]

      Zoom the graphics by the given amount. The factor must be > 0

      [-n|--font FONTTAG:size[:font]]

      This lets you customize which font to use for the various text elements on the RRD graphs. DEFAULT sets the default value for all elements, TITLE for the title, AXIS for the axis labels, UNIT for the vertical unit label, LEGEND for the graph legend, WATERMARK for the watermark on the edge of the graph.

      Use Times for the title: --font TITLE:13:Times

      Note that you need to quote the argument to --font if the font-name contains whitespace: --font "TITLE:13:Some Font"

      If you do not give a font string you can modify just the size of the default font: --font TITLE:13:.

      If you specify the size 0 then you can modify just the font without touching the size. This is especially useful for altering the default font without resetting the default fontsizes: --font DEFAULT:0:Courier.

      RRDtool comes with a preset default font. You can set the environment variable RRD_DEFAULT_FONT if you want to change this.

      RRDtool uses Pango for its font handling. This means you can to use the full Pango syntax when selecting your font:

      The font name has the form "[FAMILY-LIST] [STYLE-OPTIONS] [SIZE]", where FAMILY-LIST is a comma separated list of families optionally terminated by a comma, STYLE_OPTIONS is a whitespace separated list of words where each WORD describes one of style, variant, weight, stretch, or gravity, and SIZE is a decimal number (size in points) or optionally followed by the unit modifier "px" for absolute size. Any one of the options may be absent.

      [-R|--font-render-mode {normal,light,mono}]

      There are 3 font render modes:

      normal: Full Hinting and Anti-aliasing (default)

      light: Slight Hinting and Anti-aliasing

      mono: Full Hinting and NO Anti-aliasing

      [-B|--font-smoothing-threshold size]

      (this gets ignored in 1.3 for now!)

      This specifies the largest font size which will be rendered bitmapped, that is, without any font smoothing. By default, no text is rendered bitmapped.

      [-P|--pango-markup]

      All text in RRDtool is rendered using Pango. With the --pango-markup option, all text will be processed by pango markup. This allows one to embed some simple html like markup tags using

      <span key="value">text</span>
      Apart from the verbose syntax, there are also the following short tags available.

      b     Bold
      big   Makes font relatively larger, equivalent to <span size="larger">
      i     Italic
      s     Strikethrough
      sub   Subscript
      sup   Superscript
      small Makes font relatively smaller, equivalent to <span size="smaller">
      tt    Monospace font
      u     Underline
      More details on http://developer.gnome.org/pango/stable/PangoMarkupFormat.html.

      [-G|--graph-render-mode {normal,mono}]

      There are 2 render modes:

      normal: Graphs are fully Anti-aliased (default)

      mono: No Anti-aliasing

      [-E|--slope-mode]

      RRDtool graphs are composed of stair case curves by default. This is in line with the way RRDtool calculates its data. Some people favor a more 'organic' look for their graphs even though it is not all that true.
     */

    /**
     * [-a|--imgformat PNG|SVG|EPS|PDF|XML|XMLENUM|JSON|JSONTIME|CSV|TSV|SSV]
     * 
     * Image format for the generated graph. For the vector formats you can choose among the standard Postscript fonts Courier-Bold, Courier-BoldOblique, Courier-Oblique, Courier, Helvetica-Bold, Helvetica-BoldOblique, Helvetica-Oblique, Helvetica, Symbol, Times-Bold, Times-BoldItalic, Times-Italic, Times-Roman, and ZapfDingbats.
     * For Export type you can define XML, XMLENUM (enumerates the value tags <v0>,<v1>,<v2>,...), JSON, JSONTIME (adds a timestamp to each data row), CSV (=comma separated values), TSV (=tab separated values), SSV (=semicolon separated values), (for comma/tab/semicolon separated values the time format by default is in the form of unix time. to change it to something else use: --x-grid MINUTE:10:HOUR:1:HOUR:4:0:"%Y-%m-%d %H:%M:%S")
     * For non-image --imgformats see "OUTPUT FORMAT" in rrdxport for details on the output.
     * @var string 
     */
    protected $imgformat = 'PNG';

    /*

      [-i|--interlaced]

      (this gets ignored in 1.3 for now!)

      If images are interlaced they become visible on browsers more quickly.

      [-T|--tabwidth value]

      By default the tab-width is 40 pixels, use this option to change it.

      [-b|--base value]

      If you are graphing memory (and NOT network traffic) this switch should be set to 1024 so that one Kb is 1024 byte. For traffic measurement, 1 kb/s is 1000 b/s.

      [-W|--watermark string]

      Adds the given string as a watermark, horizontally centered, at the bottom of the graph.

      [-Z|--use-nan-for-all-missing-data]

      If one DS is missing, either because the RRD is not available or because it does not contain the requested DS name, just assume that we got empty values instead of raising a fatal error.
     */

    public function setTimeRange($start = NULL, $end = NULL, $step = NULL) {
        $this->start = $start;
        $this->end = $end;
        $this->step = $step;
        $this->onGetOptions['onGetOptionsTGraphOptions'] = [$this, 'onGetOptionsTGraphOptions'];
        return $this;
    }

    public function setLabel($title = NULL, $verticalLabel = NULL) {
        $this->title = $title;
        $this->verticalLabel = $verticalLabel;
        $this->onGetOptions['onGetOptionsTGraphOptions'] = [$this, 'onGetOptionsTGraphOptions'];
        return $this;
    }

    public function setSize($width = NULL, $height = NULL, $onlyGraph = NULL, $fullSizeMode = NULL) {
        $this->width = $width;
        $this->height = $height;
        $this->onlyGraph = $onlyGraph;
        $this->fullSizeMode = $fullSizeMode;
        $this->onGetOptions['onGetOptionsTGraphOptions'] = [$this, 'onGetOptionsTGraphOptions'];
        return $this;
    }

    public function setLimit($upperLimit = NULL, $lowerLimit = NULL, $rigid = FALSE, $allowShrink = FALSE) {
        $this->upperLimit = $upperLimit;
        $this->lowerLimit = $lowerLimit;
        $this->rigid = $rigid;
        $this->allowShrink = $allowShrink;
        $this->onGetOptions['onGetOptionsTGraphOptions'] = [$this, 'onGetOptionsTGraphOptions'];
        return $this;
    }

    public function setAltAutoscale($altAutoscale) {
        if (preg_match("/^(BOTH|MIN|MAX)$/", $altAutoscale)) {
            $this->altAutoscale = $altAutoscale;
        }
        $this->onGetOptions['onGetOptionsTGraphOptions'] = [$this, 'onGetOptionsTGraphOptions'];
        return $this;
    }

    public function setNoGridfit($noGridfit = FALSE) {
        $this->noGridfit = ($noGridfit === TRUE);
        $this->onGetOptions['onGetOptionsTGraphOptions'] = [$this, 'onGetOptionsTGraphOptions'];
        return $this;
    }

    /**
     * 
     * @param string $imgformat
     * @return $this
     */
    public function setImgformat($imgformat) {
        if (preg_match("/^(PNG|SVG|EPS|PDF|XML|XMLENUM|JSON|JSONTIME|CSV|TSV|SSV)$/", $imgformat)) {
            $this->imgformat = $imgformat;
            $this->onGetOptions['onGetOptionsTGraphOptions'] = [$this, 'onGetOptionsTGraphOptions'];
        }
        return $this;
    }

    /**
     * 
     * @return array
     */
    protected function onGetOptionsTGraphOptions() {
        $return = array();
        if ($this->start !== NULL) {
            $return[] = '--start';
            $return[] = $this->start;
        }
        if ($this->end !== NULL) {
            $return[] = '--end';
            $return[] = $this->end;
        }
        if ($this->step !== NULL) {
            $return[] = '--step';
            $return[] = $this->step;
        }
        if ($this->title !== NULL) {
            $return[] = '--title';
            $return[] = $this->title;
        }
        if ($this->verticalLabel !== NULL) {
            $return[] = '--vertical-label';
            $return[] = $this->verticalLabel;
        }
        if ($this->width !== NULL) {
            $return[] = '--width';
            $return[] = $this->width;
        }
        if ($this->height !== NULL) {
            $return[] = '--height';
            $return[] = $this->height;
        }
        if ($this->onlyGraph === TRUE) {
            $return[] = '--only-graph';
        }
        if ($this->fullSizeMode === TRUE) {
            $return[] = '--full-size-mode';
        }
        if ($this->upperLimit !== NULL) {
            $return[] = '--upper-limit';
            $return[] = $this->upperLimit;
        }
        if ($this->lowerLimit !== NULL) {
            $return[] = '--lower-limit';
            $return[] = $this->lowerLimit;
        }
        if ($this->rigid === TRUE) {
            $return[] = '--rigid';
        }
        if ($this->allowShrink === TRUE) {
            $return[] = '--allow-shrink';
        }
        switch ($this->altAutoscale) {
            case "BOTH":
                $return[] = '--alt-autoscale';
                break;
            case "MIN":
                $return[] = '--alt-autoscale-min';
                break;
            case "MAX":
                $return[] = '--alt-autoscale-max';
                break;
        }
        if ($this->noGridfit === TRUE) {
            $return[] = '--no-gridfit]';
        }
        if ($this->imgformat !== NULL) {
            $return[] = '--imgformat';
            $return[] = $this->imgformat;
        }
        return $return;
    }

}
