<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Rrd\Options\Extensions;

/**
 *
 * @author Karel
 */
trait TCreateOptionsRoundRobinArchives {

    /**
     *
     * @var array 
     */
    protected $roundRobinFunctionElements = array();

    public function rraCf($consolidationFunction, $xff, $steps, $rows) {
        if ($xff < 0 || $xff > 1) {
            throw new RRDParameterException('Wrong xff ! :' . $xff);
        }
        if (!preg_match('/^[0-9_]+(s|m|h|d|w|M|y)?$/', $steps)) {
            throw new RRDParameterException('Wrong steps! :' . $steps);
        }
        if (!preg_match('/^[0-9_]+(s|m|h|d|w|M|y)?$/', $rows)) {
            throw new RRDParameterException('Wrong rows ! :' . $rows);
        }
        if (!preg_match('/^(AVERAGE|MIN|MAX|LAST)$/', $consolidationFunction)) {
            throw new RRDParameterException('Wrong consolidationFunction ! :' . $consolidationFunction);
        }
        $this->roundRobinFunctionElements[] = 'RRA:' . $consolidationFunction . ':' . $xff . ':' . $steps . ':' . $rows;
        $this->onGetOptions['onGetOptionsTCreateOptionsRoundRobinArchives'] = [$this, 'onGetOptionsTCreateOptionsRoundRobinArchives'];
        return $this;
    }

    /*
      Aberrant Behavior Detection with Holt-Winters Forecasting
      In addition to the aggregate functions, there are a set of specialized functions that enable RRDtool to provide data smoothing (via the Holt-Winters forecasting algorithm), confidence bands, and the flagging aberrant behavior in the data source time series:

      RRA:HWPREDICT:rows:alpha:beta:seasonal period[:rra-num]
      RRA:MHWPREDICT:rows:alpha:beta:seasonal period[:rra-num]
      RRA:SEASONAL:seasonal period:gamma:rra-num[:smoothing-window=fraction]
      RRA:DEVSEASONAL:seasonal period:gamma:rra-num[:smoothing-window=fraction]
      RRA:DEVPREDICT:rows:rra-num
      RRA:FAILURES:rows:threshold:window length:rra-num
     *      */

    /**
     * 
     * @return array
     */
    protected function onGetOptionsTCreateOptionsRoundRobinArchives() {
        return $this->roundRobinFunctionElements;
    }

}
