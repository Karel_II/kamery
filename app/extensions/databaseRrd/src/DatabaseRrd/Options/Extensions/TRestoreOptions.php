<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Rrd\Options\Extensions;

/**
 *
 * @author Karel
 */
trait TRestoreOptions {

    /**
     * Make sure the values in the RRAs do not exceed the limits defined for the various data sources.
     * @var boolean 
     */
    protected $rangeCheck = NULL;

    /**
     * Allows RRDtool to overwrite the destination RRD.
     * @var boolean 
     */
    protected $forceOverwrite = NULL;

    public function setRangeCheck($rangeCheck) {
        $this->rangeCheck = $rangeCheck;
        $this->onGetOptions['onGetOptionsTRestoreOptions'] = [$this, 'onGetOptionsTRestoreOptions'];
        return $this;
    }

    public function setForceOverwrite($forceOverwrite) {
        $this->forceOverwrite = $forceOverwrite;
        $this->onGetOptions['onGetOptionsTRestoreOptions'] = [$this, 'onGetOptionsTRestoreOptions'];
        return $this;
    }

    /**
     * 
     * @return array
     */
    public function onGetOptionsTRestoreOptions() {
        $return = array();
        if ($this->rangeCheck === TRUE) {
            $return[] = '--range-check';
        }
        if ($this->forceOverwrite === TRUE) {
            $return[] = '--force-overwrite';
        }
        return $return;
    }

}
