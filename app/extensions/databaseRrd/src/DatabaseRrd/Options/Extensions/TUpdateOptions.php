<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Rrd\Options\Extensions;

use Nette\Utils\ArrayHash,
    Nette\Utils\DateTime;

/**
 *
 * @author Karel
 */
trait TUpdateOptions {

    /**
     * The data used for updating the RRD was acquired at a certain time. This time can either be defined in seconds since 1970-01-01 or by using the letter 'N', in which case the update time is set to be the current time. Negative time values are subtracted from the current time. An AT_STYLE TIME SPECIFICATION (see the rrdfetch documentation) may also be used by delimiting the end of the time specification with the '@' character instead of a ':'. Getting the timing right to the second is especially important when you are working with data-sources of type COUNTER, DERIVE, DCOUNTER, DDERIVE or ABSOLUTE.
      When using negative time values, options and data have to be separated by two dashes (--), else the time value would be parsed as an option. See below for an example.
      The remaining elements of the argument are DS updates. The order of this list is the same as the order the data sources were defined in the RRA. If there is no data for a certain data-source, the letter U (e.g., N:0.1:U:1) can be specified.
      The format of the value acquired from the data source is dependent on the data source type chosen. Normally it will be numeric, but the data acquisition modules may impose their very own parsing of this parameter as long as the colon (:) remains the data source value separator.
     * @var array 
     */
    protected $dataUpdate = array();

    /**
     * By default, the update function expects its data input in the order the data sources are defined in the RRD, excluding any COMPUTE data sources (i.e. if the third data source DST is COMPUTE, the third input value will be mapped to the fourth data source in the RRD and so on). This is not very error resistant, as you might be sending the wrong data into an RRD.
      The template switch allows you to specify which data sources you are going to update and in which order. If the data sources specified in the template are not available in the RRD file, the update process will abort with an error message.
      While it appears possible with the template switch to update data sources asynchronously, RRDtool implicitly assigns non-COMPUTE data sources missing from the template the *UNKNOWN* value.
      Do not specify a value for a COMPUTE DST in the update function. If this is done accidentally (and this can only be done using the template switch), RRDtool will ignore the value specified for the COMPUTE DST.
      The caching daemon rrdcached cannot be used together with templates yet.
     * @var array 
     */
    protected $template = NULL;

    /**
     * When updating an rrd file with data earlier than the latest update already applied, rrdtool will issue an error message and abort. This option instructs rrdtool to silently skip such data. It can be useful when re-playing old data into an rrd file and you are not sure how many updates have already been applied.
     * @var boolean 
     */
    protected $skipPastUpdates = NULL;

    /**
     * 
     * @param float|null|string $value
     * @return float|null
     * @throws \Exception
     */
    protected function checkValue($value) {
        if (is_numeric($value)) {
            return $value;
        } elseif ($value === NULL || empty($value)) {
            return 'U';
        }
        throw new \Exception('Invalid value : ' . $value);
    }

    /**
     * 
     * @param bool $skipPastUpdates
     * @return $this
     */
    public function setSkipPastUpdates($skipPastUpdates) {
        $this->skipPastUpdates = ($skipPastUpdates === TRUE);
        $this->onGetOptions['onGetOptionsTUpdateOptions'] = [$this, 'onGetOptionsTUpdateOptions'];
        return $this;
    }

    /**
     * 
     * @param DateTime $time
     * @param array $values
     * @return $this
     * @throws \Exception
     */
    public function addDataUpdate(DateTime $time, array $values) {
        $stripValues = array_values($values);
        $this->dataUpdate[$time->getTimestamp()] = array_map([$this, 'checkValue'], $values);
        if (empty($this->template)) {
            $this->template = array_keys($values);
        } else if ($test = array_diff($this->template, array_keys($values))) {
            throw new \Exception("Inconsistency of values!");
        }
        $this->onGetOptions['onGetOptionsTUpdateOptions'] = [$this, 'onGetOptionsTUpdateOptions'];
        return $this;
    }

    /**
     * 
     * @return array
     */
    protected function onGetOptionsTUpdateOptions() {
        $return = array();
        if ($this->skipPastUpdates === TRUE) {
            $return[] = '--skip-past-updates';
        }
        if ($this->template !== NULL) {
            $return[] = ('-t' . join(':', $this->template));
        }
        foreach ($this->dataUpdate as $time => $values) {
            $return[] = ($time . ':' . join(':', $values));
        }
        return $return;
    }

}
