<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Rrd\Options\Extensions;

/**
 *
 * @author Karel
 */
trait TCreateOptions {

    /**
     * Specifies the time in seconds since 1970-01-01 UTC when the first value should be added to the RRD. 
     * RRDtool will not accept any data timed before or at the time specified.
     * 
     * See also "AT-STYLE TIME SPECIFICATION" in rrdfetch for other ways to specify time.
     * 
     * If one or more source files is used to pre-fill the new RRD, the --start option may be omitted. In that case, the latest update time among all source files will be used as the last update time of the new RRD file, effectively setting the start time.
     * @var string 
     */
    protected $start = NULL;

    /**
     * Specifies the base interval in seconds with which data will be fed into the RRD. 
     * A scaling factor may be present as a suffix to the integer; see "STEP, HEARTBEAT, and Rows As Durations".
     * @var string 
     */
    protected $step = NULL;

    /**
     * Do not clobber an existing file of the same name.
     * @var boolean 
     */
    protected $noOverwrite = NULL;

    /**
     * Specifies a template RRD file to take step, DS and RRA definitions from. This allows one to base the structure of a new file on some existing file. 
     * The data of the template file is NOT used for pre-filling, but it is possible to specify the same file as a source file (see below).
     * 
     * Additional DS and RRA definitions are permitted, and will be added to those taken from the template.
     * @var \SplFileInfo 
     */
    protected $templateFile = NULL;

    /**
     * One or more source RRD files may be named on the command line. 
     * Data from these source files will be used to prefill the created RRD file. 
     * The output file and one source file may refer to the same file name. 
     * This will effectively replace the source file with the new RRD file. 
     * While there is the danger to loose the source file because it gets replaced, there is no danger that the source and the new file may be "garbled" together at any point in time, because the new file will always be created as a temporary file first and will only be moved to its final destination once it has been written in its entirety.
     * 
     * 
     * Prefilling is done by matching up DS names, RRAs and consolidation functions and choosing the best available data resolution when doing so. 
     * Prefilling may not be mathematically correct in all cases (e.g. if resolutions have to change due to changed stepping of the target RRD and old and new resolutions do not match up with old/new bin boundaries in RRAs).
     * 
     * In other words: A best effort is made to preserve data during prefilling. 
     * Also, pre-filling of RRAs may only be possible for certain kinds of DS types. 
     * Prefilling may also have strange effects on Holt-Winters forecasting RRAs. 
     * In other words: there is no guarantee for data-correctness.
     * 
     * When "pre-filling" a RRD file, the structure of the new file must be specified as usual using DS and RRA specifications as outlined below. 
     * Data will be taken from source files based on DS names and types and in the order the source files are specified in. 
     * Data sources with the same name from different source files will be combined to form a new data source. 
     * Generally, for any point in time the new RRD file will cover after its creation, data from only one source file will have been used for pre-filling. 
     * However, data from multiple sources may be combined if it refers to different times or an earlier named source file holds unknown data for a time where a later one holds known data.
     * 
     * If this automatic data selection is not desired, the DS syntax allows one to specify a mapping of target and source data sources for prefilling. 
     * This syntax allows one to rename data sources and to restrict prefilling for a DS to only use data from a single source file.
     * 
     * Prefilling currently only works reliably for RRAs using one of the classic consolidation functions, that is one of: AVERAGE, MIN, MAX, LAST. 
     * It might also currently have problems with COMPUTE data sources.
     * @var \SplFileInfo  
     */
    protected $source = NULL;

    public function setStart($start) {
        $this->start = $start;
        $this->onGetOptions['onGetOptionsTCreateOptions'] = [$this, 'onGetOptionsTCreateOptions'];
        return $this;
    }

    public function setStep($step) {
        $this->step = $step;
        $this->onGetOptions['onGetOptionsTCreateOptions'] = [$this, 'onGetOptionsTCreateOptions'];
        return $this;
    }

    public function setNoOverwrite($noOverwrite) {
        $this->noOverwrite = $noOverwrite;
        $this->onGetOptions['onGetOptionsTCreateOptions'] = [$this, 'onGetOptionsTCreateOptions'];
        return $this;
    }

    public function setTemplateFile(\SplFileInfo $templateFile) {
        $this->templateFile = $templateFile;
        $this->onGetOptions['onGetOptionsTCreateOptions'] = [$this, 'onGetOptionsTCreateOptions'];
        return $this;
    }

    public function setSource(\SplFileInfo $source) {
        $this->source = $source;
        $this->onGetOptions['onGetOptionsTCreateOptions'] = [$this, 'onGetOptionsTCreateOptions'];
        return $this;
    }

    /**
     * @todo [DS:ds-name[=mapped-ds-name[[source-index]]]:DST:dst arguments] [RRA:CF:cf arguments]
     * 
      DS:ds-name[=mapped-ds-name[[source-index]]]:DST:dst arguments
      A single RRD can accept input from several data sources (DS), for example incoming and outgoing traffic on a specific communication line. With the DS configuration option you must define some basic properties of each data source you want to store in the RRD.

      ds-name is the name you will use to reference this particular data source from an RRD. A ds-name must be 1 to 19 characters long in the characters [a-zA-Z0-9_].

      DST defines the Data Source Type. The remaining arguments of a data source entry depend on the data source type. For GAUGE, COUNTER, DERIVE, DCOUNTER, DDERIVE and ABSOLUTE the format for a data source entry is:

      DS:ds-name:{GAUGE | COUNTER | DERIVE | DCOUNTER | DDERIVE | ABSOLUTE}:heartbeat:min:max

      For COMPUTE data sources, the format is:

      DS:ds-name:COMPUTE:rpn-expression

      In order to decide which data source type to use, review the definitions that follow. Also consult the section on "HOW TO MEASURE" for further insight.

      GAUGE
      is for things like temperatures or number of people in a room or the value of a RedHat share.

      COUNTER
      is for continuous incrementing counters like the ifInOctets counter in a router. The COUNTER data source assumes that the counter never decreases, except when a counter overflows. The update function takes the overflow into account. The counter is stored as a per-second rate. When the counter overflows, RRDtool checks if the overflow happened at the 32bit or 64bit border and acts accordingly by adding an appropriate value to the result.

      DCOUNTER
      the same as COUNTER, but for quantities expressed as double-precision floating point number. Could be used to track quantities that increment by non-integer numbers, i.e. number of seconds that some routine has taken to run, total weight processed by some technology equipment etc. The only substantial difference is that DCOUNTER can either be upward counting or downward counting, but not both at the same time. The current direction is detected automatically on the second non-undefined counter update and any further change in the direction is considered a reset. The new direction is determined and locked in by the second update after reset and its difference to the value at reset.

      DERIVE
      will store the derivative of the line going from the last to the current value of the data source. This can be useful for gauges, for example, to measure the rate of people entering or leaving a room. Internally, derive works exactly like COUNTER but without overflow checks. So if your counter does not reset at 32 or 64 bit you might want to use DERIVE and combine it with a MIN value of 0.

      DDERIVE
      the same as DERIVE, but for quantities expressed as double-precision floating point number.

      NOTE on COUNTER vs DERIVE

      by Don Baarda <don.baarda@baesystems.com>

      If you cannot tolerate ever mistaking the occasional counter reset for a legitimate counter wrap, and would prefer "Unknowns" for all legitimate counter wraps and resets, always use DERIVE with min=0. Otherwise, using COUNTER with a suitable max will return correct values for all legitimate counter wraps, mark some counter resets as "Unknown", but can mistake some counter resets for a legitimate counter wrap.

      For a 5 minute step and 32-bit counter, the probability of mistaking a counter reset for a legitimate wrap is arguably about 0.8% per 1Mbps of maximum bandwidth. Note that this equates to 80% for 100Mbps interfaces, so for high bandwidth interfaces and a 32bit counter, DERIVE with min=0 is probably preferable. If you are using a 64bit counter, just about any max setting will eliminate the possibility of mistaking a reset for a counter wrap.

      ABSOLUTE
      is for counters which get reset upon reading. This is used for fast counters which tend to overflow. So instead of reading them normally you reset them after every read to make sure you have a maximum time available before the next overflow. Another usage is for things you count like number of messages since the last update.

      COMPUTE
      is for storing the result of a formula applied to other data sources in the RRD. This data source is not supplied a value on update, but rather its Primary Data Points (PDPs) are computed from the PDPs of the data sources according to the rpn-expression that defines the formula. Consolidation functions are then applied normally to the PDPs of the COMPUTE data source (that is the rpn-expression is only applied to generate PDPs). In database software, such data sets are referred to as "virtual" or "computed" columns.

      heartbeat defines the maximum number of seconds that may pass between two updates of this data source before the value of the data source is assumed to be *UNKNOWN*.

      min and max define the expected range values for data supplied by a data source. If min and/or max are specified any value outside the defined range will be regarded as *UNKNOWN*. If you do not know or care about min and max, set them to U for unknown. Note that min and max always refer to the processed values of the DS. For a traffic-COUNTER type DS this would be the maximum and minimum data-rate expected from the device.

      If information on minimal/maximal expected values is available, always set the min and/or max properties. This will help RRDtool in doing a simple sanity check on the data supplied when running update.

      rpn-expression defines the formula used to compute the PDPs of a COMPUTE data source from other data sources in the same <RRD>. It is similar to defining a CDEF argument for the graph command. Please refer to that manual page for a list and description of RPN operations supported. For COMPUTE data sources, the following RPN operations are not supported: COUNT, PREV, TIME, and LTIME. In addition, in defining the RPN expression, the COMPUTE data source may only refer to the names of data source listed previously in the create command. This is similar to the restriction that CDEFs must refer only to DEFs and CDEFs previously defined in the same graph command.

      When pre-filling the new RRD file using one or more source RRDs, the DS specification may hold an optional mapping after the DS name. This takes the form of an equal sign followed by a mapped-to DS name and an optional source index enclosed in square brackets.

      For example, the DS

      DS:a=b[2]:GAUGE:120:0:U
      specifies that the DS named a should be pre-filled from the DS named b in the second listed source file (source indices are 1-based).

      RRA:CF:cf arguments
      The purpose of an RRD is to store data in the round robin archives (RRA). An archive consists of a number of data values or statistics for each of the defined data-sources (DS) and is defined with an RRA line.

      When data is entered into an RRD, it is first fit into time slots of the length defined with the -s option, thus becoming a primary data point.

      The data is also processed with the consolidation function (CF) of the archive. There are several consolidation functions that consolidate primary data points via an aggregate function: AVERAGE, MIN, MAX, LAST.

      AVERAGE
      the average of the data points is stored.

      MIN
      the smallest of the data points is stored.

      MAX
      the largest of the data points is stored.

      LAST
      the last data points is used.

      Note that data aggregation inevitably leads to loss of precision and information. The trick is to pick the aggregate function such that the interesting properties of your data is kept across the aggregation process.

      The format of RRA line for these consolidation functions is:

      RRA:{AVERAGE | MIN | MAX | LAST}:xff:steps:rows

      xff The xfiles factor defines what part of a consolidation interval may be made up from *UNKNOWN* data while the consolidated value is still regarded as known. It is given as the ratio of allowed *UNKNOWN* PDPs to the number of PDPs in the interval. Thus, it ranges from 0 to 1 (exclusive).

      steps defines how many of these primary data points are used to build a consolidated data point which then goes into the archive. See also "STEP, HEARTBEAT, and Rows As Durations".

      rows defines how many generations of data values are kept in an RRA. Obviously, this has to be greater than zero. See also "STEP, HEARTBEAT, and Rows As Durations".

      Aberrant Behavior Detection with Holt-Winters Forecasting
      In addition to the aggregate functions, there are a set of specialized functions that enable RRDtool to provide data smoothing (via the Holt-Winters forecasting algorithm), confidence bands, and the flagging aberrant behavior in the data source time series:

      RRA:HWPREDICT:rows:alpha:beta:seasonal period[:rra-num]
      RRA:MHWPREDICT:rows:alpha:beta:seasonal period[:rra-num]
      RRA:SEASONAL:seasonal period:gamma:rra-num[:smoothing-window=fraction]
      RRA:DEVSEASONAL:seasonal period:gamma:rra-num[:smoothing-window=fraction]
      RRA:DEVPREDICT:rows:rra-num
      RRA:FAILURES:rows:threshold:window length:rra-num
      These RRAs differ from the true consolidation functions in several ways. First, each of the RRAs is updated once for every primary data point. Second, these RRAs are interdependent. To generate real-time confidence bounds, a matched set of SEASONAL, DEVSEASONAL, DEVPREDICT, and either HWPREDICT or MHWPREDICT must exist. Generating smoothed values of the primary data points requires a SEASONAL RRA and either an HWPREDICT or MHWPREDICT RRA. Aberrant behavior detection requires FAILURES, DEVSEASONAL, SEASONAL, and either HWPREDICT or MHWPREDICT.

      The predicted, or smoothed, values are stored in the HWPREDICT or MHWPREDICT RRA. HWPREDICT and MHWPREDICT are actually two variations on the Holt-Winters method. They are interchangeable. Both attempt to decompose data into three components: a baseline, a trend, and a seasonal coefficient. HWPREDICT adds its seasonal coefficient to the baseline to form a prediction, whereas MHWPREDICT multiplies its seasonal coefficient by the baseline to form a prediction. The difference is noticeable when the baseline changes significantly in the course of a season; HWPREDICT will predict the seasonality to stay constant as the baseline changes, but MHWPREDICT will predict the seasonality to grow or shrink in proportion to the baseline. The proper choice of method depends on the thing being modeled. For simplicity, the rest of this discussion will refer to HWPREDICT, but MHWPREDICT may be substituted in its place.

      The predicted deviations are stored in DEVPREDICT (think a standard deviation which can be scaled to yield a confidence band). The FAILURES RRA stores binary indicators. A 1 marks the indexed observation as failure; that is, the number of confidence bounds violations in the preceding window of observations met or exceeded a specified threshold. An example of using these RRAs to graph confidence bounds and failures appears in rrdgraph.

      The SEASONAL and DEVSEASONAL RRAs store the seasonal coefficients for the Holt-Winters forecasting algorithm and the seasonal deviations, respectively. There is one entry per observation time point in the seasonal cycle. For example, if primary data points are generated every five minutes and the seasonal cycle is 1 day, both SEASONAL and DEVSEASONAL will have 288 rows.

      In order to simplify the creation for the novice user, in addition to supporting explicit creation of the HWPREDICT, SEASONAL, DEVPREDICT, DEVSEASONAL, and FAILURES RRAs, the RRDtool create command supports implicit creation of the other four when HWPREDICT is specified alone and the final argument rra-num is omitted.

      rows specifies the length of the RRA prior to wrap around. Remember that there is a one-to-one correspondence between primary data points and entries in these RRAs. For the HWPREDICT CF, rows should be larger than the seasonal period. If the DEVPREDICT RRA is implicitly created, the default number of rows is the same as the HWPREDICT rows argument. If the FAILURES RRA is implicitly created, rows will be set to the seasonal period argument of the HWPREDICT RRA. Of course, the RRDtool resize command is available if these defaults are not sufficient and the creator wishes to avoid explicit creations of the other specialized function RRAs.

      seasonal period specifies the number of primary data points in a seasonal cycle. If SEASONAL and DEVSEASONAL are implicitly created, this argument for those RRAs is set automatically to the value specified by HWPREDICT. If they are explicitly created, the creator should verify that all three seasonal period arguments agree.

      alpha is the adaption parameter of the intercept (or baseline) coefficient in the Holt-Winters forecasting algorithm. See rrdtool for a description of this algorithm. alpha must lie between 0 and 1. A value closer to 1 means that more recent observations carry greater weight in predicting the baseline component of the forecast. A value closer to 0 means that past history carries greater weight in predicting the baseline component.

      beta is the adaption parameter of the slope (or linear trend) coefficient in the Holt-Winters forecasting algorithm. beta must lie between 0 and 1 and plays the same role as alpha with respect to the predicted linear trend.

      gamma is the adaption parameter of the seasonal coefficients in the Holt-Winters forecasting algorithm (HWPREDICT) or the adaption parameter in the exponential smoothing update of the seasonal deviations. It must lie between 0 and 1. If the SEASONAL and DEVSEASONAL RRAs are created implicitly, they will both have the same value for gamma: the value specified for the HWPREDICT alpha argument. Note that because there is one seasonal coefficient (or deviation) for each time point during the seasonal cycle, the adaptation rate is much slower than the baseline. Each seasonal coefficient is only updated (or adapts) when the observed value occurs at the offset in the seasonal cycle corresponding to that coefficient.

      If SEASONAL and DEVSEASONAL RRAs are created explicitly, gamma need not be the same for both. Note that gamma can also be changed via the RRDtool tune command.

      smoothing-window specifies the fraction of a season that should be averaged around each point. By default, the value of smoothing-window is 0.05, which means each value in SEASONAL and DEVSEASONAL will be occasionally replaced by averaging it with its (seasonal period*0.05) nearest neighbors. Setting smoothing-window to zero will disable the running-average smoother altogether.

      rra-num provides the links between related RRAs. If HWPREDICT is specified alone and the other RRAs are created implicitly, then there is no need to worry about this argument. If RRAs are created explicitly, then carefully pay attention to this argument. For each RRA which includes this argument, there is a dependency between that RRA and another RRA. The rra-num argument is the 1-based index in the order of RRA creation (that is, the order they appear in the create command). The dependent RRA for each RRA requiring the rra-num argument is listed here:

      HWPREDICT rra-num is the index of the SEASONAL RRA.
      SEASONAL rra-num is the index of the HWPREDICT RRA.
      DEVPREDICT rra-num is the index of the DEVSEASONAL RRA.
      DEVSEASONAL rra-num is the index of the HWPREDICT RRA.
      FAILURES rra-num is the index of the DEVSEASONAL RRA.
      threshold is the minimum number of violations (observed values outside the confidence bounds) within a window that constitutes a failure. If the FAILURES RRA is implicitly created, the default value is 7.

      window length is the number of time points in the window. Specify an integer greater than or equal to the threshold and less than or equal to 28. The time interval this window represents depends on the interval between primary data points. If the FAILURES RRA is implicitly created, the default value is 9.
     * 

     */

    /**
     * 
     * @return array
     */
    protected function onGetOptionsTCreateOptions() {
        $return = array();
        /* if ($this->start !== NULL) {
          $return[] = '--start';
          $return[] = $this->start;
          } */
        if ($this->step !== NULL) {
            $return[] = '--step';
            $return[] = $this->step;
        }
        if ($this->noOverwrite === TRUE) {
            $return[] = '--no-overwrite';
        }
        if ($this->templateFile !== NULL) {
            $return[] = '--template';
            $return[] = $this->templateFile;
        }
        if ($this->source !== NULL) {
            $return[] = '--source';
            $return[] = $this->source;
        }

        return $return;
    }

}
