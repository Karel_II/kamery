<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Rrd\Options\Extensions;

/**
 *
 * @author Karel
 */
trait TTuneOptions {
    /*
      --heartbeat|-h ds-name:heartbeat
      modify the heartbeat of a data source. By setting this to a high value the RRD will accept things like one value per day.

      --minimum|-i ds-name:min
      alter the minimum value acceptable as input from the data source. Setting min to 'U' will disable this limit.

      --maximum|-a ds-name:max
      alter the maximum value acceptable as input from the data source. Setting max to 'U' will disable this limit.

      --data-source-type|-d ds-name:DST
      alter the type DST of a data source.

      --data-source-rename|-r old-name:new-name
      rename a data source.

      --deltapos|-p scale-value
      Alter the deviation scaling factor for the upper bound of the confidence band used internally to calculate violations for the FAILURES RRA. The default value is 2. Note that this parameter is not related to graphing confidence bounds which must be specified as a CDEF argument to generate a graph with confidence bounds. The graph scale factor need not to agree with the value used internally by the FAILURES RRA.

      --deltaneg|-n scale-value
      Alter the deviation scaling factor for the lower bound of the confidence band used internally to calculate violations for the FAILURES RRA. The default value is 2. As with --deltapos, this argument is unrelated to the scale factor chosen when graphing confidence bounds.

      --failure-threshold|-f failure-threshold
      Alter the number of confidence bound violations that constitute a failure for purposes of the FAILURES RRA. This must be an integer less than or equal to the window length of the FAILURES RRA. This restriction is not verified by the tune option, so one can reset failure-threshold and window-length simultaneously. Setting this option will reset the count of violations to 0.

      --window-length|-w window-length
      Alter the number of time points in the temporal window for determining failures. This must be an integer greater than or equal to the window length of the FAILURES RRA and less than or equal to 28. Setting this option will reset the count of violations to 0.

      --alpha|-x adaption-parameter
      Alter the intercept adaptation parameter for the Holt-Winters forecasting algorithm. This parameter must be between 0 and 1.

      --beta|-y adaption-parameter
      Alter the slope adaptation parameter for the Holt-Winters forecasting algorithm. This parameter must be between 0 and 1.

      --gamma|-z adaption-parameter
      Alter the seasonal coefficient adaptation parameter for the SEASONAL RRA. This parameter must be between 0 and 1.

      --gamma-deviation|-v adaption-parameter
      Alter the seasonal deviation adaptation parameter for the DEVSEASONAL RRA. This parameter must be between 0 and 1.

      --smoothing-window|-s fraction-of-season
      Alter the size of the smoothing window for the SEASONAL RRA. This must be between 0 and 1.

      --smoothing-window-deviation|-S fraction-of-season
      Alter the size of the smoothing window for the DEVSEASONAL RRA. This must be between 0 and 1.

      --aberrant-reset|-b ds-name
      This option causes the aberrant behavior detection algorithm to reset for the specified data source; that is, forget all it is has learnt so far. Specifically, for the HWPREDICT or MHWPREDICT RRA, it sets the intercept and slope coefficients to unknown. For the SEASONAL RRA, it sets all seasonal coefficients to unknown. For the DEVSEASONAL RRA, it sets all seasonal deviation coefficients to unknown. For the FAILURES RRA, it erases the violation history. Note that reset does not erase past predictions (the values of the HWPREDICT or MHWPREDICT RRA), predicted deviations (the values of the DEVPREDICT RRA), or failure history (the values of the FAILURES RRA). This option will function even if not all the listed RRAs are present.

      Due to the implementation of this option, there is an indirect impact on other data sources in the RRD. A smoothing algorithm is applied to SEASONAL and DEVSEASONAL values on a periodic basis. During bootstrap initialization this smoothing is deferred. For efficiency, the implementation of smoothing is not data source specific. This means that utilizing reset for one data source will delay running the smoothing algorithm for all data sources in the file. This is unlikely to have serious consequences, unless the data being collected for the non-reset data sources is unusually volatile during the reinitialization period of the reset data source.

      Use of this tuning option is advised when the behavior of the data source time series changes in a drastic and permanent manner.

      --step|-t newstep
      Changes the step size of the RRD to newstep.

      TODO: add proper documentation
     */
    /*

      DEL:ds-name
      Every data source named with a DEL specification will be removed. The resulting RRD will miss both the definition and the data for that data source. Multiple DEL specifications are permitted.

      DS:ds-spec
      For every such data source definition (for the exact syntax see the create command), a new data source will be added to the RRD. Multiple DS specifications are permitted.

      DELRRA:index
      Removes the RRA with index index. The index is zero-based, that is the very first RRA has index 0.

      RRA:rra-spec
      For every such archive definition (for the exact syntax see the create command), a new RRA will be added to the output RRD. Multiple RRA specifications are permitted.

      RRA#index:[+-=]<number>
      Adds/removes or sets the given number of rows for the RRA with index <index>. The index is zero-based, that is the very first RRA has index 0.
     */
}
