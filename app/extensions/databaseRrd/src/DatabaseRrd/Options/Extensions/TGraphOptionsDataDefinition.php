<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Rrd\Options\Extensions;

use App\Model\Rrd\Options\Options;

/**
 *
 * @author Karel
 */
trait TGraphOptionsDataDefinition {

    /**
     *
     * @var array 
     */
    protected $dataElements = array();

    public function def($valueName, Options $optionsSource, $dataSourceName, $consolidationFunction, $step = NULL, $start = NULL, $end = NULL, $reduce = NULL) {
        $element = "DEF:$valueName=" . $optionsSource->getFileDatabase() . ":$dataSourceName:$consolidationFunction";

        if ($step !== NULL) {
            $element .= ':step=' . $step;
        }
        if ($start !== NULL) {
            $element .= ':start=' . $start;
        }
        if ($end !== NULL) {
            $element .= ':end=' . $end;
        }
        if ($reduce !== NULL) {
            $element .= ':reduce=' . $reduce;
        }
        if ($optionsSource->hasDaemon()) {
            $element .= ':daemon=' . $optionsSource->getDaemon();
        }
        $this->dataElements[] = $element;
        $this->onGetOptions['onGetOptionsTGraphOptoonsDataDefinition'] = [$this, 'onGetOptionsTGraphOptoonsDataDefinition'];
        return $this;
    }

    /**
     * 
     * @return array
     */
    protected function onGetOptionsTGraphOptoonsDataDefinition() {
        return $this->dataElements;
    }

}
