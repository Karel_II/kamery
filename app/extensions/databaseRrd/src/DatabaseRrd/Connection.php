<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteAddons\DatabaseRrd;

use App\Model\Rrd\Options\Options;
use App\Model\Rrd\Options\IOptions;
use Nette\Utils\FileSystem;

/**
 * Description of RrdConnection
 *
 * @author Karel
 */
class Connection {

    protected $options = NULL;

    public function __construct($directoryDatabase, $directoryGraph, $daemon) {
        $this->options = new Options();
        $this->options->setDaemon($daemon);
        $this->options->setDirectoryDatabase($directoryDatabase);
        $this->options->setDirectoryGraph($directoryGraph);
    }

    public function __clone() {
        $this->options = clone $this->options;
    }

    public function __destruct() {
        if ($this->options->hasDaemon()) {
            rrdc_disconnect();
        }
        $this->options->setTableName(NULL);
    }

    public function getOptions() {
        if (!$this->options->isConnected()) {
            throw new RRDConnectionException('Database is not connected use `connectTable($tableName) function`');
        }
        return $this->options;
    }

    /**
     * 
     * @param string $tableName
     * @return \App\Model\Rrd\Connection
     * @throws \Nette\InvalidStateException
     */
    public function connectTable($databaseName, $tableName) {
        $_this = clone $this;
        $_this->options->setDatabaseName($databaseName);
        $_this->options->setTableName($tableName);
        $file = $_this->options->getFileDatabase();
        if (!$file->isFile()) {
            throw new \Nette\InvalidStateException("File '$file' is not exists.");
        }
        return $_this;
    }

    /**
     * 
     * @param string $tableName
     * @param IOptions $options
     * @param boolan $overwrite
     * @return boolean
     * @throws \Nette\InvalidStateException
     * @throws RRDConnectionException
     */
    public function rrdCreate($databaseName, $tableName, IOptions $options, $overwrite = false) {
        $this->options->setDatabaseName($databaseName);
        $this->options->setTableName($tableName);
        $file = $this->options->getFileDatabase();
        if (!$overwrite && $file->isFile()) {
            throw new \Nette\InvalidStateException("File or directory '$file' already exists.");
        }
        FileSystem::createDir($file->getPath());
        $options->setNoOverwrite(!$overwrite);
        if (($result = rrd_create($file, $options->mergeBaseOptions($this->options)->getOptions()))) {
            return $result;
        } else {
            throw new RRDConnectionException(rrd_error());
        }
    }

    /**
     * 
     * @param string $tableName
     * @param \SplFileInfo $xmlFile 
     * @param IOptions $options
     * @param boolan $overwrite
     * @return boolean
     * @throws \Nette\InvalidStateException
     * @throws RRDConnectionException
     */
    public function rrdRestore($tableName, \SplFileInfo $xmlFile, IOptions $options, $overwrite = false) {
        $this->options->setTableName($tableName);
        $fileDatabase = $this->options->getFileDatabase();
        if (!$overwrite && $fileDatabase->isFile()) {
            throw new \Nette\InvalidStateException("File or directory '$fileDatabase' already exists.");
        }
        if ($xmlFile->isFile()) {
            throw new \Nette\InvalidStateException("File or directory '$xmlFile' is not exists.");
        }
        $options->setForceOverwrite($overwrite);
        if (($result = rrd_restore($xmlFile, $this->options->getFileDatabase(), $options->mergeBaseOptions($this->options)->getOptions()))) {
            return $result;
        } else {
            throw new RRDConnectionException(rrd_error());
        }
    }

    /**
     * 
     * @param IOptions $options
     * @return array
     * @throws RRDConnectionException
     */
    public function rrdFetch(IOptions $options) {
        if (!$this->options->isConnected()) {
            throw new RRDConnectionException('Database is not connected use `connectTable($tableName) function`');
        }
        if (($result = rrd_fetch($this->options->getFileDatabase(), $options->mergeBaseOptions($this->options)->getOptions()))) {
            return $result;
        } else {
            throw new RRDConnectionException(rrd_error());
        }
    }

    /**
     * 
     * @param int $raaindex
     * @return int
     * @throws RRDConnectionException
     */
    public function rrdFirst($raaindex = 0) {
        if (!$this->options->isConnected()) {
            throw new RRDConnectionException('Database is not connected use `connectTable($tableName) function`');
        }
        if (($result = rrd_first($this->options->getFileDatabase(), $raaindex))) {
            return $result;
        } else {
            throw new RRDConnectionException(rrd_error());
        }
    }

    /**
     * 
     * @return array
     * @throws RRDConnectionException
     */
    public function rrdInfo() {
        if (!$this->options->isConnected()) {
            throw new RRDConnectionException('Database is not connected use `connectTable($tableName) function`');
        }
        if (($result = rrd_info($this->options->getFileDatabase()))) {
            return $result;
        } else {
            throw new RRDConnectionException(rrd_error());
        }
    }

    /**
     * 
     * @return int
     * @throws RRDConnectionException
     */
    public function rrdLast() {
        if (!$this->options->isConnected()) {
            throw new RRDConnectionException('Database is not connected use `connectTable($tableName) function`');
        }
        if (($result = rrd_last($this->options->getFileDatabase()))) {
            return $result;
        } else {
            throw new RRDConnectionException(rrd_error());
        }
    }

    /**
     * 
     * @return array
     * @throws RRDConnectionException
     */
    public function rrdLastUpdate() {
        if (!$this->options->isConnected()) {
            throw new RRDConnectionException('Database is not connected use `connectTable($tableName) function`');
        }
        if (($result = rrd_lastupdate($this->options->getFileDatabase()))) {
            return $result;
        } else {
            throw new RRDConnectionException(rrd_error());
        }
    }

    /**
     * 
     * @param IOptions $options
     * @return bool
     * @throws RRDConnectionException
     */
    public function rrdTune(IOptions $options) {
        if (!$this->options->isConnected()) {
            throw new RRDConnectionException('Database is not connected use `connectTable($tableName) function`');
        }
        if (($result = rrd_tune($this->options->getFileDatabase(), $options->mergeBaseOptions($this->options)->getOptions()))) {
            return $result;
        } else {
            throw new RRDConnectionException(rrd_error());
        }
    }

    /**
     * 
     * @param IOptions $options
     * @return bool
     * @throws RRDConnectionException
     */
    public function rrdUpdate(IOptions $options) {
        if (!$this->options->isConnected()) {
            throw new RRDConnectionException('Database is not connected use `connectTable($tableName) function`');
        }
        if (($result = rrd_update($this->options->getFileDatabase(), $options->mergeBaseOptions($this->options)->getOptions()))) {
            return $result;
        } else {
            throw new RRDConnectionException(rrd_error());
        }
    }

    /**
     * 
     * @return array
     * @throws RRDConnectionException
     */
    public function rrdVersion() {
        if (($result = rrd_version())) {
            return $result;
        } else {
            throw new RRDConnectionException(rrd_error());
        }
    }

    /**
     * 
     * @param IOptions $options
     * @return bool
     * @throws RRDConnectionException
     */
    public function rrdXport(IOptions $options) {
        if (($result = rrd_xport($options->mergeBaseOptions($this->options)->getOptions()))) {
            return $result;
        } else {
            throw new RRDConnectionException(rrd_error());
        }
    }

    /**
     * 
     * @param IOptions $options
     * @return array
     * @throws RRDConnectionException
     */
    public function rrdGraph(IOptions $options) {
        if (!$this->options->isConnected()) {
            throw new RRDConnectionException('Database is not connected use `connectTable($tableName) function`');
        }
        $fileGraph = $options->mergeBaseOptions($this->options)->getFileGraph();
        FileSystem::createDir($fileGraph->getPath());
        if (($result = rrd_graph($fileGraph, $options->getOptions()))) {
            return $result;
        } else {
            throw new RRDConnectionException(rrd_error());
        }
    }

}

class RRDConnectionException extends \Exception {
    
}
