<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\RepositoryRrd;

use Nette\Utils\ArrayHash,
    Nette\Utils\DateTime;
use Nette\Localization\ITranslator;
use \NetteAddons\DatabaseRrd\Connection;

/**
 * Description of RepositoryRrd
 *
 * @author karel.novak
 */
abstract class RepositoryRrd {

    /**
     *
     * @var string 
     */
    protected $tableName = NULL;

    /**
     *
     * @var \NetteAddons\DatabaseRrd\Connection
     */
    protected $connection = NULL;

    /**
     *
     * @var array 
     */
    protected $activeConections = array();

    /**
     *
     * @var Nette\Localization\ITranslator 
     */
    protected $translator = NULL;

    /**
     * When updating an rrd file with data earlier than the latest update already applied, rrdtool will issue an error message and abort. 
     * This option instructs rrdtool to silently skip such data. 
     * It can be useful when re-playing old data into an rrd file and you are not sure how many updates have already been applied.
     * @var booleand 
     */
    protected $skipPastUpdates = FALSE;

    /**
     *
     * @var string 
     */
    protected $createStep = NULL;

    /**
     *
     * @var string 
     */
    protected $createStart = NULL;

    /**
     * Heartbeat defines the maximum number of seconds that may pass between two updates of this data source before the value of the data source is assumed to be *UNKNOWN*.
     * @var string 
     */
    protected $dataSourceHearthBeat = NULL;

    /**
     * 
     * @param \NetteAddons\DatabaseRrd\Connection $conection
     */
    public function __construct(Connection $conection, ITranslator $translator) {
        $this->connection = $conection;
        $this->translator = $translator;
    }

    /**
     * 
     * @param string $tableName
     * @return $this
     */
    public function setTableName($tableName) {
        $this->tableName = $tableName;
        return $this;
    }

    /**
     * Vrací název databáze odvozené z jména třídy
     * @return string
     */
    protected function getDatabaseName() {
        $m = array();
        preg_match('#(\w+)RepositoryRrd$#', get_class($this), $m);
        return lcfirst($m[1]);
    }

    /**
     * 
     * @param string $tableName
     * @return \NetteAddons\DatabaseRrd\Connection
     */
    protected function getConnection($tableName = NULL) {
        if ($tableName === NULL) {
            $tableName = $this->tableName;
        }
        if (isset($this->activeConections) && isset($this->activeConections[$tableName])) {
            return $this->activeConections[$tableName];
        }
        return $this->activeConections[$tableName] = $this->connection->connectTable($this->getDatabaseName(), $tableName);
    }

    final protected static function onError() {
        throw new RRDExecutionException(rrd_error());
    }

    private static function detectNull($value) {
        return (preg_match('/^(U|NAN)$/i', $value)) ? NULL : $value;
    }

    /**
     * 
     * @return ArrayHash
     */
    protected function rrdFetch($options) {
        $return = new ArrayHash();
        $result = $this->getConnection()->rrdFetch($options);
        for ($i = $result['start']; $i <= $result['end']; $i += $result['step']) {
            $row = (isset($return[$i])) ? $return[$i] : new ArrayHash();
            $insert = FALSE;
            foreach ($result['data'] as $key => $data) {
                if (isset($data[$i]) && $key !== '') {
                    $value = static::detectNull($data[$i]);
                    $insert = ($insert || ($value !== NULL));
                    $row[$key] = $value;
                } else {
                    $row = NULL;
                    continue;
                }
            }
            if (isset($row) && $insert) {
                $return[$i] = $row;
            }
        }
        return $return;
    }

    /**
     * 
     * @return ArrayHash
     */
    protected function rrdLastUpdate() {
        $return = new ArrayHash();
        $result = $this->getConnection()->rrdLastUpdate();
        $return['#lastUpdate'] = DateTime::from($result['last_update']);
        for ($i = 0; $i < $result['ds_cnt']; $i++) {

            $key = (strlen($result['ds_navm'][$i]) == 0) ? '#' : $result['ds_navm'][$i];
            $value = (($result['data'][$i] === 'U') ? NULL : $result['data'][$i]);
            $return[$key] = $value;
        }
        return $return;
    }

    /**
     * 
     * @return boolean
     */
    public function isUpdated() {
        $info = $this->getConnection()->rrdInfo();
        return(($info['last_update'] + $info['step']) > time());
    }

}

class RRDExecutionException extends \Exception {
    
}

class RRDParameterException extends \Exception {
    
}
