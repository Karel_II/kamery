<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\RepositoryRrd;

use Nette\Utils\ArrayList;
use App\Model\Weather\Weather,
    App\Model\Objects\Device;
use App\Model\Rrd\Helpes\GraphLineDashes,
    App\Model\Rrd\Options\OptionsGraph,
    App\Model\Rrd\Options\OptionsCreate,
    App\Model\Rrd\Options\OptionsFetch,
    App\Model\Rrd\Options\OptionsUpdate;

/**
 * Description of RrdStorage
 *
 * @author karel.novak
 */
class MeteoStationsRepositoryRrd extends RepositoryRrd {

    protected $skipPastUpdates = TRUE;

    private function padTranslate($text, $pads = 0) {
        return str_pad($this->translator->translate($text), $pads, " ", STR_PAD_RIGHT);
    }

    /**
     * 
     * @param Device $device
     */
    public function setDevice(Device $device) {
        $this->setTableName($device->name);
    }

    public function updateList(Device $device, ArrayList $list) {
        $update = array();
        $options = new OptionsUpdate();
        foreach ($list as $key => $weather) {
            $values = array(
                "period" => $weather->period,
                "periodUnit" => $weather->periodUnit,
                "windDirDeg" => $weather->windDirDegrees,
                "windSpeed" => $weather->windSpeed,
                "windSpeedMin" => $weather->windSpeedMin,
                "windSpeedMax" => $weather->windSpeedMax,
                "windGust" => $weather->windGust,
                "temperature" => $weather->temperature,
                "temperatureMin" => $weather->temperatureMin,
                "temperatureMax" => $weather->temperatureMax,
                "temperatureWindchil" => $weather->temperatureWindchill,
                "temperatureDewPoint" => $weather->temperatureDewPoint,
                "temperatureSystem" => $weather->temperatureSystem,
                "pressure" => $weather->pressure,
                "pressureMin" => $weather->pressureMin,
                "pressureMax" => $weather->pressureMax,
                "altitudeBarometric" => $weather->altitudeBarometric,
                "humidityRelative" => $weather->humidityRelative,
                "humidityAbsolute" => $weather->humidityAbsolute,
                "season" => $weather->season,
            );
            $options->addDataUpdate($weather->datetime, $values);
        }
        if ($this->skipPastUpdates === TRUE) {
            $options->setSkipPastUpdates($this->skipPastUpdates);
        }
        $this->getConnection($device->name)->rrdUpdate($options);
        return true;
    }

    public function info(Device $device) {
        return $this->getConnection($device->name)->rrdInfo();
    }

    public function lastUpdate(Device $device) {
        $this->setDevice($device);
        $return = new Weather();
        if (($result = $this->rrdLastUpdate())) {
            $return->setDatetime($result['#lastUpdate']);
            $return->setPeriod($result->period);
            $return->setPeriodUnit($result->periodUnit);
            $return->setWindDirDegrees($result->windDirDeg);
            $return->setWindSpeed($result->windSpeed);
            $return->setWindSpeedMin($result->windSpeedMin);
            $return->setWindSpeedMax($result->windSpeedMax);
            $return->setWindGust($result->windGust);
            $return->setTemperature($result->temperature);
            $return->setTemperatureMin($result->temperatureMin);
            $return->setTemperatureMax($result->temperatureMax);
            $return->setTemperatureWindchill($result->temperatureWindchil);
            $return->setTemperatureDewPoint($result->temperatureDewPoint);
            $return->setTemperatureSystem($result->temperatureSystem);
            $return->setPressure($result->pressure);
            $return->setPressureMin($result->pressureMin);
            $return->setPressureMax($result->pressureMax);
            $return->setAltitudeBarometric($result->altitudeBarometric);
            $return->setHumidityRelative($result->humidityRelative);
            $return->setHumidityAbsolute($result->humidityAbsolute);
            $return->setSeason($result->season);
        }
        return $return;
    }

    public function fetch(Device $device, OptionsFetch $options) {
        $this->setDevice($device);
        $return = new ArrayList();
        if (($result = $this->rrdFetch($options))) {
            foreach ($result as $key => $value) {
                $record = new Weather();
                $record->setDatetime(\Nette\Utils\DateTime::from($key));
                $record->setPeriod($value->period);
                $record->setPeriodUnit($value->periodUnit);
                $record->setWindDirDegrees($value->windDirDeg);
                $record->setWindSpeed($value->windSpeed);
                $record->setWindSpeedMin($value->windSpeedMin);
                $record->setWindSpeedMax($value->windSpeedMax);
                $record->setWindGust($value->windGust);
                $record->setTemperature($value->temperature);
                $record->setTemperatureMin($value->temperatureMin);
                $record->setTemperatureMax($value->temperatureMax);
                $record->setTemperatureWindchill($value->temperatureWindchil);
                $record->setTemperatureDewPoint($value->temperatureDewPoint);
                $record->setTemperatureSystem($value->temperatureSystem);
                $record->setPressure($value->pressure);
                $record->setPressureMin($value->pressureMin);
                $record->setPressureMax($value->pressureMax);
                $record->setAltitudeBarometric($value->altitudeBarometric);
                $record->setHumidityRelative($value->humidityRelative);
                $record->setHumidityAbsolute($value->humidityAbsolute);
                $record->setSeason($value->season);
                $return[] = $record;
            }
            return $return;
        }
    }

    public function temperatureGraph(Device $device, OptionsGraph $options) {
        $optionsSource = $this->getConnection($device->name)->getOptions();
        $options->setLabel($this->padTranslate('Temperature'), $this->padTranslate('Temperature') . ' °C');
        $options->setAltAutoscale("BOTH");
        $options->
                //ReadData
                def('dataTemperature', $optionsSource, 'temperature', 'AVERAGE')->
                def('dataTemperatureMin', $optionsSource, 'temperatureMin', 'AVERAGE')->
                def('dataTemperatureMax', $optionsSource, 'temperatureMax', 'AVERAGE')->
                def('dataTemperatureDewPoint', $optionsSource, 'temperatureDewPoint', 'AVERAGE')->
                def('dataTemperatureWindchill', $optionsSource, 'temperatureWindchil', 'AVERAGE')->
                //Compute limit MIN MAX
                vdef('limitMinDataTemperature', 'dataTemperature,MINIMUM')->
                vdef('limitMaxDataTemperature', 'dataTemperature,MAXIMUM')->
                vdef('limitMinDataTemperatureMin', 'dataTemperatureMin,MINIMUM')->
                vdef('limitMaxDataTemperatureMax', 'dataTemperatureMax,MAXIMUM')->
                //Compute Data For Drawing
                cdef('drawDataTemperature', 'dataTemperature,1,*')->
                cdef('drawDataTemperatureMin', 'limitMinDataTemperatureMin,UN,limitMinDataTemperature,dataTemperatureMin,IF')->
                cdef('drawDataTemperatureMax', 'limitMaxDataTemperatureMax,UN,limitMaxDataTemperature,dataTemperatureMax,IF')->
                cdef('drawDataTemperatureDewPoint', 'dataTemperatureDewPoint,1,*')->
                cdef('drawDataTemperatureWindchill', 'dataTemperatureWindchill,1,*')->
                //Compute Data For Labels
                vdef('labelMaxTemperature', 'drawDataTemperatureMax,MAXIMUM')->
                vdef('labelMinTemperature', 'drawDataTemperatureMin,MINIMUM')->
                vdef('labelAvgTemperature', 'drawDataTemperature,AVERAGE')->
                vdef('labelLasTemperature', 'drawDataTemperature,LAST')->
                vdef('labelMaxTemperatureDewPoint', 'dataTemperatureDewPoint,MAXIMUM')->
                vdef('labelMinTemperatureDewPoint', 'dataTemperatureDewPoint,MINIMUM')->
                vdef('labelAvgTemperatureDewPoint', 'dataTemperatureDewPoint,AVERAGE')->
                vdef('labelLasTemperatureDewPoint', 'dataTemperatureDewPoint,LAST')->
                vdef('labelMaxTemperatureWindchill', 'dataTemperatureWindchill,MAXIMUM')->
                vdef('labelMinTemperatureWindchill', 'dataTemperatureWindchill,MINIMUM')->
                vdef('labelAvgTemperatureWindchill', 'dataTemperatureWindchill,AVERAGE')->
                vdef('labelLasTemperatureWindchill', 'dataTemperatureWindchill,LAST')->
                //Draw
                comment($device->description . '\l')->
                area('drawDataTemperature', "#00FF00", "#008800", $this->padTranslate('Temperature', 24) . ' °C')->
                gprint('labelMaxTemperature', $this->padTranslate("Max") . '\: %5.1lf')->
                gprint('labelAvgTemperature', $this->padTranslate("Avg") . '\: %5.1lf')->
                gprint('labelMinTemperature', $this->padTranslate("Min") . '\: %5.1lf')->
                gprint('labelLasTemperature', $this->padTranslate("Current") . '\: %5.1lf\l')->
                line('drawDataTemperatureDewPoint', '#FF0000', 1, $this->padTranslate('Dew Point Temperature', 25) . ' °C')->
                gprint('labelMaxTemperatureDewPoint', $this->padTranslate("Max") . '\: %5.1lf')->
                gprint('labelAvgTemperatureDewPoint', $this->padTranslate("Avg") . '\: %5.1lf')->
                gprint('labelMinTemperatureDewPoint', $this->padTranslate("Min") . '\: %5.1lf')->
                gprint('labelLasTemperatureDewPoint', $this->padTranslate("Current") . '\: %5.1lf\l')->
                line('drawDataTemperatureWindchill', '#0000FF', 1, $this->padTranslate('Windchill Temperature', 25) . ' °C')->
                gprint('labelMaxTemperatureWindchill', $this->padTranslate("Max") . '\: %5.1lf')->
                gprint('labelAvgTemperatureWindchill', $this->padTranslate("Avg") . '\: %5.1lf')->
                gprint('labelMinTemperatureWindchill', $this->padTranslate("Min") . '\: %5.1lf')->
                gprint('labelLasTemperatureWindchill', $this->padTranslate("Current") . '\: %5.1lf\l')->
                line('drawDataTemperatureMin', '#660033', 1, NULL, NULL, NULL, new GraphLineDashes(true))->
                line('drawDataTemperatureMax', '#554400', 1, NULL, NULL, NULL, new GraphLineDashes(true));

        return $this->getConnection($device->name)->rrdGraph($options);
    }

    public function pressureGraph(Device $device, OptionsGraph $options) {
        $optionsSource = $this->getConnection($device->name)->getOptions();
        $options->setLabel($this->padTranslate('Pressure'), $this->padTranslate('Pressure') . ' hPa');
        $options->setAltAutoscale("BOTH");
        $options->
                //ReadData
                def('dataPressure', $optionsSource, 'pressure', 'AVERAGE')->
                def('dataPressureMin', $optionsSource, 'pressureMin', 'AVERAGE')->
                def('dataPressureMax', $optionsSource, 'pressureMax', 'AVERAGE')->
                //Compute limit MIN MAX
                vdef('limitMinDataPressure', 'dataPressure,MINIMUM')->
                vdef('limitMaxDataPressure', 'dataPressure,MAXIMUM')->
                vdef('limitMinDataPressureMin', 'dataPressureMin,MINIMUM')->
                vdef('limitMaxDataPressureMax', 'dataPressureMax,MAXIMUM')->
                //Compute Data For Drawing
                cdef('drawDataPressure', 'dataPressure,1,*')->
                cdef('drawDataPressureMin', 'limitMinDataPressureMin,UN,limitMinDataPressure,dataPressureMin,IF')->
                cdef('drawDataPressureMax', 'limitMaxDataPressureMax,UN,limitMaxDataPressure,dataPressureMax,IF')->
                //Compute Data For Labels
                vdef('labelMaxPressure', 'drawDataPressureMax,MAXIMUM')->
                vdef('labelMinPressure', 'drawDataPressureMin,MINIMUM')->
                vdef('labelAvgPressure', 'drawDataPressure,AVERAGE')->
                vdef('labelLasPressure', 'drawDataPressure,LAST')->
                //Draw
                comment($device->description . '\l')->
                area('drawDataPressure', "#00FF00", "#008800", $this->padTranslate('Pressure', 25) . ' hPa')->
                gprint('labelMaxPressure', $this->padTranslate("Max") . '\: %5.1lf')->
                gprint('labelAvgPressure', $this->padTranslate("Avg") . '\: %5.1lf')->
                gprint('labelMinPressure', $this->padTranslate("Min") . '\: %5.1lf')->
                gprint('labelLasPressure', $this->padTranslate("Current") . '\: %5.1lf\l')->
                line('drawDataPressureMin', '#660033', 1, NULL, NULL, NULL, new GraphLineDashes(true))->
                line('drawDataPressureMax', '#554400', 1, NULL, NULL, NULL, new GraphLineDashes(true));
        return $this->getConnection($device->name)->rrdGraph($options);
    }

    public function windSpeedGraph(Device $device, OptionsGraph $options) {
        $optionsSource = $this->getConnection($device->name)->getOptions();
        $options->setLabel($this->padTranslate('Wind Speed'), $this->padTranslate('Wind Speed') . ' m/s');
        $options->setAltAutoscale("BOTH");
        $options->
                //ReadData
                def('dataWindSpeed', $optionsSource, 'windSpeed', 'AVERAGE')->
                def('dataWindSpeedMin', $optionsSource, 'windSpeedMin', 'AVERAGE')->
                def('dataWindSpeedMax', $optionsSource, 'windSpeedMax', 'AVERAGE')->
                def('dataWindGust', $optionsSource, 'windGust', 'AVERAGE')->
                //Compute limit MIN MAX
                vdef('limitMinDataWindSpeed', 'dataWindSpeed,MINIMUM')->
                vdef('limitMaxDataWindSpeed', 'dataWindSpeed,MAXIMUM')->
                vdef('limitMinDataWindSpeedMin', 'dataWindSpeedMin,MINIMUM')->
                vdef('limitMaxDataWindSpeedMax', 'dataWindSpeedMax,MAXIMUM')->
                //Compute Data For Drawing
                cdef('drawDataWindSpeed', 'dataWindSpeed,1,*')->
                cdef('drawDataWindSpeedMin', 'limitMinDataWindSpeedMin,UN,limitMinDataWindSpeed,dataWindSpeedMin,IF')->
                cdef('drawDataWindSpeedMax', 'limitMaxDataWindSpeedMax,UN,limitMaxDataWindSpeed,dataWindSpeedMax,IF')->
                cdef('drawDataWindGust', 'dataWindGust,1,*')->
                //Compute Data For Labels
                vdef('labelMaxWindSpeed', 'drawDataWindSpeedMax,MAXIMUM')->
                vdef('labelMinWindSpeed', 'drawDataWindSpeedMin,MINIMUM')->
                vdef('labelAvgWindSpeed', 'drawDataWindSpeed,AVERAGE')->
                vdef('labelLasWindSpeed', 'drawDataWindSpeed,LAST')->
                vdef('labelMaxWindGust', 'drawDataWindGust,MAXIMUM')->
                vdef('labelMinWindGust', 'drawDataWindGust,MINIMUM')->
                vdef('labelAvgWindGust', 'drawDataWindGust,AVERAGE')->
                vdef('labelLasWindGust', 'drawDataWindGust,LAST')->
                //Draw
                comment($device->description . '\l')->
                area('drawDataWindSpeed', "#00FF00", "#008800", $this->padTranslate("Wind Speed", 24) . ' m/s')->
                gprint('labelMaxWindSpeed', $this->padTranslate("Max") . '\: %5.1lf')->
                gprint('labelAvgWindSpeed', $this->padTranslate("Avg") . '\: %5.1lf')->
                gprint('labelMinWindSpeed', $this->padTranslate("Min") . '\: %5.1lf')->
                gprint('labelLasWindSpeed', $this->padTranslate("Current") . '\: %5.1lf\l')->
                line('drawDataWindGust', '#0000FF', 1, $this->padTranslate('Wind Gust', 25) . ' m/s')->
                gprint('labelMaxWindGust', $this->padTranslate("Max") . '\: %5.1lf')->
                gprint('labelAvgWindGust', $this->padTranslate("Avg") . '\: %5.1lf')->
                gprint('labelMinWindGust', $this->padTranslate("Min") . '\: %5.1lf')->
                gprint('labelLasWindGust', $this->padTranslate("Current") . '\: %5.1lf\l')->
                line('drawDataWindSpeedMin', '#660033', 1, NULL, NULL, NULL, new GraphLineDashes(true))->
                line('drawDataWindSpeedMax', '#554400', 1, NULL, NULL, NULL, new GraphLineDashes(true));
        return $this->getConnection($device->name)->rrdGraph($options);
    }

    public function humidityGraph(Device $device, OptionsGraph $options) {
        $optionsSource = $this->getConnection($device->name)->getOptions();
        $options->setLabel($this->padTranslate('Humidity'), $this->padTranslate('Humidity') . ' g/m3, %');
        $options->setAltAutoscale("BOTH");
        $options->
                def('dataHumidityRelative', $optionsSource, 'humidityRelative', 'AVERAGE')->
                def('datahHumidityAbsolute', $optionsSource, 'humidityAbsolute', 'AVERAGE')->
                //Compute limit MIN MAX
                vdef('limitMinDataHumidityRelative', 'dataHumidityRelative,MINIMUM')->
                vdef('limitMaxDataHumidityRelative', 'dataHumidityRelative,MAXIMUM')->
                vdef('limitMinDataHumidityAbsolute', 'datahHumidityAbsolute,MINIMUM')->
                vdef('limitMaxDataHumidityAbsolute', 'datahHumidityAbsolute,MAXIMUM')->
                //Compute Data For Drawing
                cdef('drawDataHumidityRelative', 'dataHumidityRelative,1,*')->
                cdef('drawDataHumidityAbsolute', 'datahHumidityAbsolute,1,*')->
                //Compute Data For Labels
                vdef('labelMaxHumidityRelative', 'drawDataHumidityRelative,MAXIMUM')->
                vdef('labelMinHumidityRelative', 'drawDataHumidityRelative,MINIMUM')->
                vdef('labelAvgHumidityRelative', 'drawDataHumidityRelative,AVERAGE')->
                vdef('labelLasHumidityRelative', 'drawDataHumidityRelative,LAST')->
                vdef('labelMaxHumidityAbsolute', 'drawDataHumidityAbsolute,MAXIMUM')->
                vdef('labelMinHumidityAbsolute', 'drawDataHumidityAbsolute,MINIMUM')->
                vdef('labelAvgHumidityAbsolute', 'drawDataHumidityAbsolute,AVERAGE')->
                vdef('labelLasHumidityAbsolute', 'drawDataHumidityAbsolute,LAST')->
                //Draw
                comment($device->description . '\l')->
                area('drawDataHumidityRelative', "#00FF00", "#008800", $this->padTranslate("Relative Humidity", 25) . '   %')->
                gprint('labelMaxHumidityRelative', $this->padTranslate("Max") . '\: %5.1lf')->
                gprint('labelAvgHumidityRelative', $this->padTranslate("Avg") . '\: %5.1lf')->
                gprint('labelMinHumidityRelative', $this->padTranslate("Min") . '\: %5.1lf')->
                gprint('labelLasHumidityRelative', $this->padTranslate("Current") . '\: %5.1lf\l')->
                line('drawDataHumidityAbsolute', '#0000FF', 1, $this->padTranslate("Absolute Humidity", 25) . 'g/m3')->
                gprint('labelMaxHumidityAbsolute', $this->padTranslate("Max") . '\: %5.1lf')->
                gprint('labelAvgHumidityAbsolute', $this->padTranslate("Avg") . '\: %5.1lf')->
                gprint('labelMinHumidityAbsolute', $this->padTranslate("Min") . '\: %5.1lf')->
                gprint('labelLasHumidityAbsolute', $this->padTranslate("Current") . '\: %5.1lf\l');
        return $this->getConnection($device->name)->rrdGraph($options);
    }

}
