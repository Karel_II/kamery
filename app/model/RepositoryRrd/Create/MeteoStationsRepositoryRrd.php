<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\RepositoryRrd\Create;

use App\Model\Rrd\Options\OptionsCreate;
use App\Model\Objects\Device;

/**
 * Description of MeteoStationsRepositoryRrdCreate
 *
 * @author Karel
 */
class MeteoStationsRepositoryRrd {

    /**
     *
     * @var \App\Model\Rrd\Connection 
     */
    protected $connection = NULL;
    protected $rrdSetupRepositoryLite = NULL;

    /**
     * 
     * @param \App\Model\Rrd\Connection $conection
     */
    public function __construct(\NetteAddons\DatabaseRrd\Connection $conection, \App\Model\Repository\RrdSetupRepositoryLite $rrdSetupRepositoryLite) {
        $this->connection = $conection;
        $this->rrdSetupRepositoryLite = $rrdSetupRepositoryLite;
    }

    /**
     * Vrací název databáze odvozené z jména třídy
     * @return string
     */
    protected function getDatabaseName() {
        $m = array();
        preg_match('#(\w+)RepositoryRrd$#', get_class($this), $m);
        return lcfirst($m[1]);
    }

    public function create(Device $device, $overwrite = false) {
        $options = new OptionsCreate();
        $this->rrdSetupRepositoryLite->fetchRrdCreateIntervalOption($options, $device);
        $dataSourceHearthBeat = $this->rrdSetupRepositoryLite->getRrdCreateHearthBeat($device);
        $options->
                //Set DataSource
                dataSource('period', 'GAUGE', $dataSourceHearthBeat)->
                dataSource('periodUnit', 'GAUGE', $dataSourceHearthBeat)->
                dataSource('windDirDeg', 'GAUGE', $dataSourceHearthBeat)->
                dataSource('windSpeed', 'GAUGE', $dataSourceHearthBeat)->
                dataSource('windSpeedMin', 'GAUGE', $dataSourceHearthBeat)->
                dataSource('windSpeedMax', 'GAUGE', $dataSourceHearthBeat)->
                dataSource('windGust', 'GAUGE', $dataSourceHearthBeat)->
                dataSource('temperature', 'GAUGE', $dataSourceHearthBeat)->
                dataSource('temperatureMin', 'GAUGE', $dataSourceHearthBeat)->
                dataSource('temperatureMax', 'GAUGE', $dataSourceHearthBeat)->
                dataSource('temperatureWindchil', 'GAUGE', $dataSourceHearthBeat)->
                dataSource('temperatureDewPoint', 'GAUGE', $dataSourceHearthBeat)->
                dataSource('temperatureSystem', 'GAUGE', $dataSourceHearthBeat)->
                dataSource('pressure', 'GAUGE', $dataSourceHearthBeat)->
                dataSource('pressureMin', 'GAUGE', $dataSourceHearthBeat)->
                dataSource('pressureMax', 'GAUGE', $dataSourceHearthBeat)->
                dataSource('altitudeBarometric', 'GAUGE', $dataSourceHearthBeat)->
                dataSource('humidityRelative', 'GAUGE', $dataSourceHearthBeat)->
                dataSource('humidityAbsolute', 'GAUGE', $dataSourceHearthBeat)->
                dataSource('season', 'GAUGE', $dataSourceHearthBeat);
        $this->rrdSetupRepositoryLite->fetchRrdCreateRra($options, $device);
        $this->connection->rrdCreate($this->getDatabaseName(), $device->name, $options, $overwrite);
    }

}
