<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Objects;

/**
 *
 * @author Karel
 * 
 * @property-read string $rrdDataDriver Description
 * @property-read string $rrdStep Description
 * @property-read array $rrdGraphs Description
 * @property-read string $rrdGraphsText Description
 */
trait TDeviceRrd {

    /**
     *
     * @var array 
     */
    protected $rrdDataDriver = NULL;

    /**
     *
     * @var array 
     */
    protected $rrdStep = NULL;

    /**
     *
     * @var array 
     */
    protected $rrdGraphs = NULL;

    public function getRrdDataDriver() {
        return $this->rrdDataDriver;
    }

    public function getRrdStep() {
        return $this->rrdStep;
    }

    public function getRrdGraphs() {
        if (empty($this->rrdGraphs)) {
            return array();
        }
        return $this->rrdGraphs;
    }

    public function getRrdGraphsText() {
        if (empty($this->rrdGraphs)) {
            return NULL;
        }
        return implode(';', $this->rrdGraphs);
    }

    /**
     * 
     * @param string $rrdDataDriver
     * @param string $rrdStep
     * @param array $rrdGraphs
     * @return $this
     */
    public function setRrd($rrdDataDriver, $rrdStep, $rrdGraphs) {
        if (is_array($rrdGraphs)) {
            $this->rrdGraphs = $rrdGraphs;
        } elseif (!empty($rrdGraphs) && is_string($rrdGraphs)) {
            $this->rrdGraphs = explode(';', $rrdGraphs);
        }
        $this->rrdDataDriver = $rrdDataDriver;
        $this->rrdStep = $rrdStep;

        return $this;
    }

}
