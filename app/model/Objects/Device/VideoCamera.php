<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Objects;

use Nette;
use NetteAddons\Utils;
use NetteAddons\Http\Url;

/**
 * Description of VideoCamera
 *
 * @author Karel

 * @property int $previewMaxHeight
 * @property int $previewMaxWidth
 * @property int $archiveMaxHeight
 * @property int $archiveMaxWidth
 * 
 */
class VideoCamera extends Device implements Utils\IObjectId {

    use \Nette\SmartObject;

    use Utils\TObjectId;

    /** @var int  */
    private $previewMaxHeight = 1024;

    /** @var int  */
    private $previewMaxWidth = 1024;

    /** @var int  */
    private $archiveMaxHeight = 1024;

    /** @var int  */
    private $archiveMaxWidth = 1024;

    /**
     * 
     * @return int
     */
    public function getPreviewMaxHeight() {
        return $this->previewMaxHeight;
    }

    /**
     * 
     * @return int
     */
    public function getPreviewMaxWidth() {
        return $this->previewMaxWidth;
    }

    /**
     * 
     * @return int
     */
    public function getArchiveMaxHeight() {
        return $this->archiveMaxHeight;
    }

    /**
     * 
     * @return int
     */
    public function getArchiveMaxWidth() {
        return $this->archiveMaxWidth;
    }

    /*     * *******************************************************************
     *                                   SET
     * ********************************************************************** */

    /**
     * 
     * @param int $previewMaxHeight
     * @return $this
     */
    public function setPreviewMaxHeight($previewMaxHeight) {
        $this->previewMaxHeight = $previewMaxHeight;
        return $this;
    }

    /**
     * 
     * @param int $previewMaxWidth
     * @return $this
     */
    public function setPreviewMaxWidth($previewMaxWidth) {
        $this->previewMaxWidth = $previewMaxWidth;
        return $this;
    }

    /**
     * 
     * @param int $archiveMaxHeight
     * @return $this
     */
    public function setArchiveMaxHeight($archiveMaxHeight) {
        $this->archiveMaxHeight = $archiveMaxHeight;
        return $this;
    }

    /**
     * 
     * @param int $archiveMaxWidth
     * @return $this
     */
    public function setArchiveMaxWidth($archiveMaxWidth) {
        $this->archiveMaxWidth = $archiveMaxWidth;
        return $this;
    }

    /**
     * NEW
     */
    public static function newVideoCamera($id, $name, $description = NULL, $instalationDescription = NULL, Url $url = NULL, Url $urlSource = NULL, $watermark = FALSE, $private = TRUE, $snmpCommunity = NULL, GeographicCoordinateSystem $geographicCoordinates = NULL, $archiveDays = 365, $previewMaxWidth = NULL, $previewMaxHeight = NULL, $archiveMaxWidth = NULL, $archiveMaxHeight = NULL) {
        $static = new static;
        if ($id !== NULL) {
            $static->setId($id);
        }
        $static->setName($name);
        $static->setDescription($description);
        $static->setInstalationDescription($instalationDescription);
        $static->setUrl($url);
        $static->setUrlSource($urlSource);
        $static->setPrivate($private);
        $static->setWatermark($watermark);
        $static->setSnmpCommunity($snmpCommunity);
        $static->setGeographicCoordinates($geographicCoordinates);
        /**
         * Preview and Archive
         */
        $static->setArchiveDays($archiveDays);
        $static->setPreviewMaxWidth($previewMaxWidth);
        $static->setPreviewMaxHeight($previewMaxHeight);
        $static->setArchiveMaxWidth($archiveMaxWidth);
        $static->setArchiveMaxHeight($archiveMaxHeight);
        return $static;
    }

}
