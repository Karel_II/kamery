<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Objects;

use Nette;
use NetteAddons\Utils;
use NetteAddons\Http\Url;

/**
 * Description of MeteoStation
 *
 * @author Karel
 * 
 * @property Object $data
 * @property string $weatherClass
 * 
 */
class MeteoStation extends Device implements Utils\IObjectId {

    use \Nette\SmartObject;

    use Utils\TObjectId;

    /**
     *
     * @var Object 
     */
    private $data = NULL;

    /**
     * GET/SET
     */
    function getData() {
        return $this->data;
    }

    function setData($data) {
        $this->data = $data;
        return $this;
    }

    /**
     * NEW
     */
    public static function newMeteoStation($id, $name, $description = NULL, $instalationDescription = NULL, Url $url = NULL, Url $urlSource = NULL, $watermark = FALSE, $private = TRUE, $snmpCommunity = NULL, GeographicCoordinateSystem $geographicCoordinates = NULL, $archiveDays = 365) {
        $static = new static;
        if ($id !== NULL) {
            $static->setId($id);
        }
        $static->setName($name);
        $static->setDescription($description);
        $static->setInstalationDescription($instalationDescription);
        $static->setUrl($url);
        $static->setUrlSource($urlSource);
        $static->setPrivate($private);
        $static->setWatermark($watermark);
        $static->setSnmpCommunity($snmpCommunity);
        $static->setGeographicCoordinates($geographicCoordinates);
        $static->setArchiveDays($archiveDays);
        return $static;
    }

}
