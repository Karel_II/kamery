<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Objects;

use \Nette\Utils\DateTime;
use \NetteAddons\Utils;

/**
 * Description of Archive
 *
 * @author karel.novak
 * 
 * @property-read string $datehour Description
 * @property-read string $year Description
 * @property \Nette\Utils\DateTime $datetime Description
 * @property-read \Nette\Utils\DateTime $datetimeNewYear Description
 * @property \NetteAddons\Utils\IObjectId $device Description
 * @property bool $processed Description
 */
class ArchiveRecord implements Utils\IObjectId {

    use \Nette\SmartObject;

    use Utils\TObjectId;

    /**
     *
     * @var IObjectId 
     */
    private $device = NULL;

    /**
     *
     * @var bool 
     */
    private $processed = FALSE;

    /**
     * @var DateTime
     */
    private $datetime;

    function getDatehour() {
        return $this->datetime->format('YmdH');
    }

    function getYear() {
        return $this->datetime->format('Y');
    }

    function getDatetime() {
        return $this->datetime;
    }

    function getDatetimeNewYear() {
        return $this->datetime->modifyClone('first day of January', 'midnight')->modify('midnight');
    }

    function getDevice() {
        return $this->device;
    }

    function setDevice($device) {
        $this->device = $device;
    }

    function setDatetime($datetime) {
        $this->datetime = $datetime;
    }

    public function getProcessed() {
        return $this->processed;
    }

    public function setProcessed($processed) {
        $this->processed = ($processed === TRUE);
        return $this;
    }

    public static function newArchiveRecord(Utils\IObjectId $device, $datetime = NULL) {
        $static = new static;
        $static->setDevice($device);
        $static->setDatetime($datetime);
        return $static;
    }

}
