<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Objects;

use NetteAddons\Utils;
use NetteAddons\Http\Url;

/**
 * Description of Device
 *
 * @author Karel
 * 
 * @property string $name
 * @property string $description
 * @property string $instalationDescription
 * @property Url $url
 * @property Url $urlSource
 * @property string $snmpCommunity
 * @property string $private
 * @property GeographicCoordinateSystem $geographicCoordinates
 * @property int $archiveDays
 * @property-read boolean $hasArchive
 * 
 * @property-read string $username
 * @property-read string $password
 * @property-read string $authenticated
 * @property-read string $baseAuthorization
 * 
 * @property bool $watermark
 */
abstract class Device implements Utils\IObjectId {

    use \Nette\SmartObject;

    use Utils\TObjectId;
    use TDeviceRrd;

    /** @var string    */
    private $name = NULL;

    /** @var string    */
    private $description = NULL;

    /** @var string    */
    private $instalationDescription = NULL;

    /** @var Url Description */
    private $url = NULL;

    /** @var Url Description */
    private $urlSource = NULL;

    /** @var string    */
    private $snmpCommunity = NULL;

    /** @var bool */
    private $private = NULL;

    /** @var GeographicCoordinateSystem  */
    private $geographicCoordinates = NULL;

    /** @var int  */
    private $archiveDays = 365;

    /** @var bool  */
    private $watermark = FALSE;

    public function getName() {
        return $this->name;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getInstalationDescription() {
        return $this->instalationDescription;
    }

    public function getUrl() {
        return $this->url;
    }

    public function getUrlSource() {
        return $this->urlSource;
    }

    public function getSnmpCommunity() {
        return $this->snmpCommunity;
    }

    public function isPrivate() {
        return $this->private;
    }

    public function getGeographicCoordinates() {
        return $this->geographicCoordinates;
    }

    public function getArchiveDays() {
        return $this->archiveDays;
    }

    public function getHasArchive() {
        return !$this->private && ($this->archiveDays > 0);
    }

    public function getWatermark() {
        return $this->watermark;
    }

    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    public function setDescription($description) {
        $this->description = $description;
        return $this;
    }

    public function setInstalationDescription($instalationDescription) {
        $this->instalationDescription = $instalationDescription;
        return $this;
    }

    public function setUrl(Url $url) {
        $this->url = $url;
        return $this;
    }

    public function setUrlSource(Url $urlSource = NULL) {
        $this->urlSource = $urlSource;
        return $this;
    }

    public function setSnmpCommunity($snmpCommunity) {
        $this->snmpCommunity = $snmpCommunity;
        return $this;
    }

    public function setPrivate($private) {
        $this->private = (preg_match('/^(true|1)$/i', $private) == TRUE);
        return $this;
    }

    public function setGeographicCoordinates(GeographicCoordinateSystem $geographicCoordinates = NULL) {
        $this->geographicCoordinates = $geographicCoordinates;
        return $this;
    }

    public function setArchiveDays($archiveDays) {
        $this->archiveDays = $archiveDays;
        return $this;
    }

    public function setWatermark($watermark) {
        $this->watermark = $watermark;
        return $this;
    }

    /**
     * GET/SET User
     */
    function getUser() {
        return $this->url->user;
    }

    function getPassword() {
        return $this->url->password;
    }

    function setUser($user) {
        $this->url->user = $user;
    }

    function setPassword($password) {
        $this->url->password = $password;
    }

    public function isAuthenticated() {
        return !$this->private && !(empty($this->url->user) && (empty($this->url->user) || empty($this->url->password)));
    }

    public function getBaseAuthorization() {
        if ($this->isAuthenticated()) {
            return base64_encode($this->url->user . ((empty($this->url->password)) ? '' : ':' . $this->url->password ));
        } else {
            return false;
        }
    }

}
