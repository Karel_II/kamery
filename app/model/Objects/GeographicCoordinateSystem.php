<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Objects;

/**
 * Description of GPS
 *
 * @author Karel
 * 
 * 
 * @property float $latitudeDec Description
 * @property float $longitudeDec Description
 * @property-read string $dec Description
 * @property-read string $latitudeDMS Description
 * @property-read string $longitudeDMS Description
 * @property-read string $DMS Description
 */
class GeographicCoordinateSystem {

    use \Nette\SmartObject;

    private $directions = 'NSEW';
    private $directionsDec = array(
        'N' => 1,
        'S' => -1,
        'E' => 1,
        'W' => -1,
    );
    private $directionsDMS = array(
        'latitude' => array(
            1 => 'N',
            -1 => 'S'
        ),
        'longitude' => array(
            1 => 'E',
            -1 => 'W'
        )
    );

    /**
     *
     * @var float|null
     */
    private $latitude = NULL;

    /**
     *
     * @var float|null
     */
    private $longitude = NULL;

    /**
     * @param  float|string|GeographicCoordinateSystem
     * @param  float|string|NULL
     */
    public function __construct($latitude, $longitude = NULL) {
        if ($latitude instanceof self) {
            $this->setLatitudeDec($latitude->getLatitudeDec());
            $this->setLongitudeDec($latitude->getLongitudeDec());
        } else if (isset($latitude) && isset($longitude) && is_numeric($latitude) && is_numeric($longitude)) {
            $this->setLatitudeDec($latitude);
            $this->setLongitudeDec($longitude);
        } else if (isset($latitude) && isset($longitude)) {
            $this->setLatitudeDec($this->parseValueString($latitude));
            $this->setLongitudeDec($this->parseValueString($longitude));
        } else if (isset($latitude)) {
            list($latitude, $longitude) = explode(',', $latitude);
            $this->setLatitudeDec($this->parseValueString($latitude));
            $this->setLongitudeDec($this->parseValueString($longitude));
        }
    }

    /**
     * @param string $string
     * @return float
     * @throws UnknownFormatException
     */
    private function parseValueString($string) {
        $matches = array();
        if (preg_match('/^\s?(?P<value>[0-9.]*)(?P<direction>[' . $this->directions . ']{1})(°)?\s?$/', $string, $matches)) {
            $direction = $matches['direction'];
            return $this->directionsDec[$direction] * $matches['value'];
        } else if (preg_match('/^\s?(?P<direction>[-]?)(?P<value>[0-9.]*)(°)?\s?$/', $string, $matches)) {
            return (($matches['direction'] == '-') ? -1 : 1) * $matches['value'];
        } else if (preg_match('/^\s?(?P<degree>[0-9]{1,2})°\s?(?P<minute>[0-9]{1,2})\'\s?(?P<second>[0-9.]*)"\s?(?P<direction>[NEWS]{1})\s?$/', $string, $matches)) {
            $direction = $matches['direction'];
            return $this->directionsDec[$direction] * ($matches['degree'] + $matches['minute'] / 60 + $matches['second'] / 3600);
        }
        throw new UnknownFormatException;
    }

    /**
     * 
     * @return float|null
     */
    public function getLatitudeDec() {
        if (isset($this->latitude)) {
            return $this->latitude;
        }
        return NULL;
    }

    /**
     * 
     * @return float|null
     */
    public function getLongitudeDec() {
        if (isset($this->longitude)) {
            return $this->longitude;
        }
        return NULL;
    }

    /**
     * 
     * @return string|null
     */
    public function getDec() {
        if (isset($this->latitude) && isset($this->longitude)) {
            return $this->latitude . ", " . $this->longitude;
        }
        return NULL;
    }

    /**
     * 
     * @return string|null
     */
    public function getLatitudeDecDMS() {
        if (isset($this->latitude)) {
            $sign = ($this->latitude < 0) ? -1 : 1;
            $direction = $this->directionsDMS['latitude'][$sign];
            $value = abs($this->latitude);
            $degree = floor($value);
            $minute = floor(($value * 60) % 60);
            $second = ($value * 3600) % 60;
            return $degree . "° " . $minute . "' " . $second . "\" " . $direction;
        }
        return NULL;
    }

    /**
     * 
     * @return string|null
     */
    public function getLongitudeDMS() {
        if (isset($this->longitude)) {
            $sign = ($this->longitude < 0) ? -1 : 1;
            $direction = $this->directionsDMS['longitude'][$sign];
            $value = abs($this->longitude);
            $degree = floor($value);
            $minute = floor(($value * 60) % 60);
            $second = ($value * 3600) % 60;
            return $degree . "° " . $minute . "' " . $second . "\" " . $direction;
        }
        return NULL;
    }

    /**
     * 
     * @return string|null
     */
    public function getDMS() {
        if (isset($this->latitude) && isset($this->longitude)) {
            return $this->getLatitudeDecDMS() . ", " . $this->getLongitudeDMS();
        }
        return NULL;
    }

    /**
     * 
     * @param float $latitude
     * @return $this
     * @throws LatitudeException
     */
    public function setLatitudeDec($latitude) {
        if (is_numeric($latitude) && abs($latitude) <= 90) {
            $this->latitude = $latitude;
            return $this;
        }
        throw new LatitudeException;
    }

    /**
     * 
     * @param float $longitude
     * @return $this
     * @throws LatitudeException
     */
    public function setLongitudeDec($longitude) {
        if (is_numeric($longitude) && abs($longitude) <= 180) {
            $this->longitude = $longitude;
            return $this;
        }
        throw new LongitudeException;
    }

}

class LatitudeException extends \Exception {
    
}

class LongitudeException extends \Exception {
    
}

class UnknownFormatException extends \Exception {
    
}
