<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Security\Repository\Create;

use \App\Model\Security\Repository;

/**
 * Description of UsersRepository
 *
 * @author karel.novak
 * @deprecated since version number
 */
class UsersRepository extends Repository\RepositoryCreate {

    public function createTable() {
        $this->context->beginTransaction();
        $this->context->query("CREATE TABLE IF NOT EXISTS `" . Repository\UsersRepository::TABLE_USER_NAME . "` ( " .
                "`" . Repository\UsersRepository::COLUMN_USER_ID . "`                 INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " .
                "`" . Repository\UsersRepository::COLUMN_USER_NAME . "`               varchar ( 50 ) NOT NULL UNIQUE, " .
                "`" . Repository\UsersRepository::COLUMN_USER_EMAIL . "`              varchar ( 50 ) DEFAULT NULL UNIQUE, " .
                "`" . Repository\UsersRepository::COLUMN_USER_PASSWORD_HASH . "`	varchar ( 60 ) NOT NULL, " .
                "`" . Repository\UsersRepository::COLUMN_USER_KEY_OTP . "`            varchar ( 160 ) DEFAULT NULL UNIQUE, " .
                "`" . Repository\UsersRepository::COLUMN_USER_KEY_API . "`            varchar ( 160 ) DEFAULT NULL UNIQUE " .
                ");"
        );
        $this->context->query("CREATE TABLE IF NOT EXISTS `" . Repository\UsersRepository::TABLE_USERROLES_NAME . "` ( " .
                "`" . Repository\UsersRepository::COLUMN_USERROLES_ID . "`            INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " .
                "`" . Repository\UsersRepository::COLUMN_USERROLES_USERID . "`        int ( 10 ) NOT NULL, " .
                "`" . Repository\UsersRepository::COLUMN_USERROLES_ROLE . "`          char ( 20 ) NOT NULL, " .
                "`" . Repository\UsersRepository::COLUMN_USERROLES_ORDER . "`         tinyint ( 4 ) NOT NULL DEFAULT '1', " .
                "FOREIGN KEY(`" . Repository\UsersRepository::COLUMN_USERROLES_USERID . "`) REFERENCES `" . Repository\UsersRepository::TABLE_USER_NAME . "`(`" . Repository\UsersRepository::COLUMN_USER_ID . "`), " .
                "FOREIGN KEY(`" . Repository\UsersRepository::COLUMN_USERROLES_ROLE . "`)   REFERENCES `" . Repository\RolesRepository::TABLE_ROLES_NAME . "`(`" . Repository\RolesRepository::COLUMN_ROLES_NAME . "`) " .
                ");"
        );

        $this->alterAddColumn(Repository\UsersRepository::TABLE_USER_NAME, "imagePerPage", "tinyint ( 3 ) NOT NULL DEFAULT '20'");
        $this->context->commit();
    }

}
