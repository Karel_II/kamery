<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Security;

use Nette;
use NetteAddons\Repository;

/**
 * Description of AuthorizatorFactory
 *
 * @author karel.novak
 */
class AuthorizatorFactory extends \Netteaddons\Security\AuthorizatorFactory {

    protected function localPermission(\Nette\Security\Permission $permission) {

        //$permission->allow('guest', Repository\UsersRepository::RESOURCE_MANAGEMENT_USER, Repository\UsersRepository::RESOURCE_MANAGEMENT_USER_PRIVILEDGE_ADD); //guest can add another user

        /*
         * LEGAL NOTICE RESOURCES
         */

        $permission->addResource(\LegalNotice\LegalNotice::PERMISSION_RESOURCE);
        $permission->addResource(\NetteAddons\GoogleServices\GoogleService::ANALYTICS_PERMISSION_RESOURCE, \LegalNotice\LegalNotice::PERMISSION_RESOURCE);

        /*
         * RESOURCES
         */

        $permission->addResource('settings');
        $permission->addResource('videoCameras');
        $permission->addResource('meteoStations');
        $permission->addResource('backend');

        /*
         * RIGHTS
         */
        $permission->allow('admin', 'administration');
        $permission->allow('user', 'settings');
        $permission->allow('guest', 'videoCameras', array('public'));
        $permission->allow('user', 'videoCameras', array('private'));
        $permission->allow('admin', 'videoCameras', array('edit'));
        $permission->allow('guest', 'meteoStations', array('public'));
        $permission->allow('user', 'meteoStations', array('private'));
        $permission->allow('admin', 'meteoStations', array('edit'));

        return $permission;
    }

}
