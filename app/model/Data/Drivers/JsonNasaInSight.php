<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Data\Drivers;

use App\Model\Data\IDataDriver;
use Nette\Utils\Json,
    Nette\Utils\DateTime,
    Nette\Utils\ArrayList;
use App\Model\Weather\Weather;

/**
 * Description of JsonNasaInSight
 *
 * @author Karel
 */
class JsonNasaInSight implements IDataDriver {

    private static function formatWeather($data, $period) {
        $weather = new Weather();
        if (isset($data->Last_UTC)) {
            $weather->setDatetime(DateTime::from($data->Last_UTC));
        } else {
            $weather->setDatetime(DateTime::from($data->First_UTC));
        }
        if ($period !== NULL) {
            $weather->setPeriod($period);
        }
        if (isset($data->WD) && isset($data->WD->most_common)) {
            $weather->setWindDirDegrees($data->WD->most_common->compass_degrees);
        }
        if (isset($data->HWS)) {
            $weather->setWindSpeed($data->HWS->av);
            $weather->setWindSpeedMin($data->HWS->mn);
            $weather->setWindSpeedMax($data->HWS->mx);
        }
        if (isset($data->AT)) {
            $weather->setTemperature($data->AT->av);
            $weather->setTemperatureMin($data->AT->mn);
            $weather->setTemperatureMax($data->AT->mx);
        }
        if (isset($data->PRE)) {
            $weather->setPressure($data->PRE->av / 100);
            $weather->setPressureMin($data->PRE->mn / 100);
            $weather->setPressureMax($data->PRE->mx / 100);
        }
        if (isset($data->Season)) {
            $weather->setSeasonText($data->Season);
        }
        $weather->setPeriodUnit(1);
        return $weather;
    }

    public static function getWeather(\SplFileInfo $file) {
        $return = new ArrayList;
        if (($content = @file_get_contents($file)) === FALSE) {
            return FALSE;
        }
        try {
            $insight = Json::decode($content);
            foreach ($insight->validity_checks->sols_checked as $solChecked) {
                $valid = true;
                foreach ($insight->validity_checks->$solChecked as $probeValidateData) {
                    $valid &= $probeValidateData->valid;
                }
                if ($valid && in_array($solChecked, $insight->sol_keys)) {
                    $return[] = static::formatWeather($insight->$solChecked, $solChecked);
                }
            }
            return $return;
        } catch (\Exception $e) {
            return FALSE;
        }
    }

}
