<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Data\Drivers;

use App\Model\Data\IDataDriver;
use Nette\Utils\DateTime,
    Nette\Utils\ArrayList;
use App\Model\Weather\Weather;

/**
 * Description of XmlGiom3000
 *
 * @author Karel
 */
class XmlGiom3000 implements IDataDriver {

    /**
     * 
     * @param Object $data
     * @param int $period
     * @return \App\Model\Objects\Weather\Drivers\Weather
     */
    private static function formatWeather($data, $period) {
        $weather = new Weather();
        if ($period !== NULL) {
            $weather->setPeriod($period);
        }
        $weather->setWindSpeed(floatval($data->windspeed));
        $weather->setWindDirStep(floatval($data->winddir));
        $weather->setWindGust(floatval($data->windgust));
        $weather->setPressure(floatval($data->pressure));
        $weather->setTemperatureSystem(floatval($data->systemp));
        $weather->setTemperature(floatval($data->temperature));
        $weather->setAltitudeBarometric(floatval($data->baraltitude));
        $weather->setTemperatureWindchill(floatval($data->windchill));
        $weather->setHumidityRelative(floatval($data->relhumidity));
        $weather->setHumidityAbsolute(floatval($data->abshumidity));
        $weather->setTemperatureDewPoint(floatval($data->dewpoint));
        $weather->setDeviceName(trim($data->devname));
        $weather->setPeriodUnit(0);
        return $weather;
    }

    /**
     * 
     * @param \SplFileInfo $file
     * @return \App\Model\Objects\Weather
     */
    public static function getWeather(\SplFileInfo $file) {
        $return = new ArrayList;
        $content = NULL;
        if (($content = @file_get_contents($file)) === FALSE) {
            return null;
        }
        try {
            $giom3000Data = @new \SimpleXMLElement($content);
        } catch (\Exception $e) {
            return null;
        }
        if ($file->isFile() && ($filemtime = $file->getMTime())) {
            $dateTime = DateTime::from($filemtime);
            $period = $dateTime->format('z');
            $return[] = (static::formatWeather($giom3000Data, $period)->setDatetime($dateTime));
            return $return;
        } else {
            $dateTime = new DateTime();
            $period = $dateTime->format('z');
            $return[] = (static::formatWeather($giom3000Data, $period)->setDatetime($dateTime));
            return $return;
        }
    }

}
