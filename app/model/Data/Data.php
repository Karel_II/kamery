<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Data;

use App\Model\Objects\Device;

/**
 * Description of Data
 *
 * @author Karel
 */
class Data {

    /**
     *
     * @var App\Model\Objects\Device 
     */
    private $device = NULL;

    public function getDevice(): App\Model\Objects\Device {
        return $this->device;
    }

    public function setDevice(App\Model\Objects\Device $device) {
        $this->device = $device;
        if (isset($this->device->data) && $this->device->data === NULL) {
            $this->device->data = $this;
        }
        return $this;
    }

}
