<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Data;

/**
 * Description of Loader
 *
 * @author Karel
 */
class Loader {

    static private $drivers = array("None", "JsonNasaInSight", "XmlGiom3000");

    public static function getDrivers() {
        return array_combine(self::$drivers, self::$drivers);
    }

    /**
     * 
     * @param string $driver
     * @param \SplFileInfo $file
     * @return Weather
     * @throws \Exception
     */
    public static function getWeather($driver, \SplFileInfo $file) {
        if (in_array($driver, self::$drivers) && class_exists(($driverClass = __NAMESPACE__ . '\\Drivers\\' . $driver)) && ((new $driverClass) instanceof \App\Model\Data\IDataDriver)) {
            return $driverClass::getWeather($file);
        }
        throw new \Exception;
    }

}
