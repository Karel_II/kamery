<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model;

use Nette;

/**
 * Description of Mrtg
 *
 * @author Karel
 * @property bool $enableIPv6 Description
 * @property string $workDir Description
 * @property string $language Description
 * @property string $configDir Description
 * @property string $logDir Description
 * @property string $binFile Description
 * 
 * @property string $xSize Description
 * @property string $ySize Description
 */
class Mrtg {

    use \Nette\SmartObject;

    private $enableIPv6;
    private $workDir;
    private $language;
    private $configDir;
    private $logDir;
    private $binFile;

    /**
     * Size 30-600 (Default 400)
     * @var int 
     */
    private $xSize;

    /**
     * Size 20-... (Default 100)
     * @var int 
     */
    private $ySize;

    function isIPv6() {
        return $this->enableIPv6;
    }

    function getIPv6() {
        return ($this->enableIPv6) ? 'yes' : 'no';
    }

    function getWorkDir() {
        return $this->workDir;
    }

    function setIPv6($enableIPv6) {
        $this->enableIPv6 = $enableIPv6;
    }

    function setWorkDir($workDir) {
        $this->workDir = $workDir;
    }

    function getEnableIPv6() {
        return $this->enableIPv6;
    }

    function getLanguage() {
        return $this->language;
    }

    function getConfigDir() {
        return $this->configDir;
    }

    function setEnableIPv6($enableIPv6) {
        $this->enableIPv6 = $enableIPv6;
    }

    function setLanguage($language) {
        $this->language = $language;
    }

    function getXSize() {
        return $this->xSize;
    }

    function getYSize() {
        return $this->ySize;
    }

    function setXSize($xSize) {
        $this->xSize = $xSize;
    }

    function setYSize($ySize) {
        $this->ySize = $ySize;
    }

    function setConfigDir($configDir) {
        $this->configDir = $configDir;
    }

    function getLogDir() {
        return $this->logDir;
    }

    function setLogDir($logDir) {
        $this->logDir = $logDir;
    }

    function getBinFile() {
        return $this->binFile;
    }

    function setBinFile($binFile) {
        $this->binFile = $binFile;
    }

}
