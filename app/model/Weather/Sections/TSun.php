<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Weather\Sections;

/**
 *
 * @author Karel
 * 
 * @property float|null $solarRadiation [W/m^2]
 * @property float|null $ultraViolet [index]
 * 
 */
trait TSun {

    /**
     * [W/m^2]
     * @var float|null 
     */
    protected $solarRadiation = NULL;

    /**
     * [index]
     * @var float|null 
     */
    protected $ultraViolet = NULL;

    public function getSolarRadiation(): ?float {
        return $this->solarRadiation;
    }

    public function getUltraViolet(): ?float {
        return $this->ultraViolet;
    }

    public function setSolarRadiation(?float $solarRadiation) {
        $this->solarRadiation = $solarRadiation;
        return $this;
    }

    public function setUltraViolet(?float $ultraViolet) {
        $this->ultraViolet = $ultraViolet;
        return $this;
    }

}
