<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Weather\Sections;

use App\Model\Weather\Helpers;

/**
 *
 * @author Karel
 * 
 * 
 * @property float|null $windDir - [0-360 instantaneous wind direction]
 * @property float|null $windGustDir - [0-360 using software specific time period]
 * @property float|null $windDirAvg2m - [0-360 2 minute average wind direction]
 * @property float|null $windGustDir10m - [0-360 past 10 minutes wind gust direction]
 * @property float|null $windDirDegrees  - [0-360 instantaneous wind direction]
 * @property float|null $windGustDirDegrees  - [0-360 using software specific time period]
 * @property float|null $windDirAvg2mDegrees  - [0-360 2 minute average wind direction]
 * @property float|null $windGustDir10mDegrees  - [0-360 past 10 minutes wind gust direction]
 * @property float|null $windDirStep  - [0-16 instantaneous wind direction]
 * @property float|null $windGustDirStep  - [0-16 using software specific time period]
 * @property float|null $windDirAvg2mStep  - [0-16 2 minute average wind direction]
 * @property float|null $windGustDir10mStep  - [0-16 past 10 minutes wind gust direction]
 * 
 * @property-read string|null $windDirText Description
 * 
 * @property float|null $windSpeed - [m/s instantaneous wind speed]
 * @property float|null $windSpeedMin - [m/s minimal wind speed]
 * @property float|null $windSpeedMax - [m/s maximal wind speed]
 * @property float|null $windGust - [m/s current wind gust, using software specific time period]
 * @property float|null $windSpedAvg2m  - [m/s 2 minute average wind speed m/s]
 * @property float|null $windGust10m - [m/s past 10 minutes wind gust m/s ] 
 * @property float|null $windSpeedMph - [mph instantaneous wind speed]
 * @property float|null $windSpeedMphMin - [mph minimal wind speed]
 * @property float|null $windSpeedMphMax - [mph maximal wind speed]
 * @property float|null $windGustMph - [mph current wind gust, using software specific time period]
 * @property float|null $windSpedMphAvg2m  - [mph 2 minute average wind speed mph]
 * @property float|null $windGustMph10m - [mph past 10 minutes wind gust mph ]
 * @property float|null $windSpeedMs - [m/s instantaneous wind speed]
 * @property float|null $windSpeedMsMin - [m/s minimal wind speed]
 * @property float|null $windSpeedMsMax - [m/s maximal wind speed]
 * @property float|null $windGustMs - [m/s current wind gust, using software specific time period]
 * @property float|null $windSpeedMsAvg2m  - [m/s 2 minute average wind speed m/s]
 * @property float|null $windGustMs10m - [m/s past 10 minutes wind gust m/s ]
 */
trait TWind {

    /**
     * [0-360 instantaneous wind direction]
     * @var float|null 
     */
    protected $windDirDegrees = NULL;

    /**
     * [0-360 using software specific time period]
     * @var float|null 
     */
    protected $windGustDirDegrees = NULL;

    /**
     * [0-360 2 minute average wind direction]
     * @var float|null 
     */
    protected $windDirAvg2mDegrees = NULL;

    /**
     * [0-360 past 10 minutes wind gust direction]
     * @var float|null
     */
    protected $windGustDir10mDegrees = NULL;

    /**
     * [m/s instantaneous wind speed]
     * @var float|null 
     */
    protected $windSpeedMs = NULL;

    /**
     * [m/s minimal wind speed]
     * @var float|null 
     */
    protected $windSpeedMsMin = NULL;

    /**
     * [m/s maximal wind speed]
     * @var float|null 
     */
    protected $windSpeedMsMax = NULL;

    /**
     * [m/s current wind gust, using software specific time period]
     * @var float|null 
     */
    protected $windGustMs = NULL;

    /**
     * [m/s 2 minute average wind speed m/s]
     * @var float|null 
     */
    protected $windSpeedMsAvg2m = NULL;

    /**
     * [m/s past 10 minutes wind gust m/s]
     * @var float|null 
     */
    protected $windGustMs10m = NULL;

    public function getWindDirDegrees(): ?float {
        return $this->windDirDegrees;
    }

    public function getWindGustDirDegrees(): ?float {
        return $this->windGustDirDegrees;
    }

    public function getWindDirAvg2mDegrees(): ?float {
        return $this->windDirAvg2mDegrees;
    }

    public function getWindGustDir10mDegrees(): ?float {
        return $this->windGustDir10mDegrees;
    }

    public function getWindDir(): ?float {
        return $this->windDirDegrees;
    }

    public function getWindGustDir(): ?float {
        return $this->windGustDirDegrees;
    }

    public function getWindDirAvg2m(): ?float {
        return $this->windDirAvg2mDegrees;
    }

    public function getWindGustDir10m(): ?float {
        return $this->windGustDir10mDegrees;
    }

    public function getWindDirStep(): ?int {
        return Helpers::directionDegreeToStep($this->windDirDegrees);
    }

    public function getWindGustDirStep(): ?int {
        return Helpers::directionDegreeToStep($this->windGustDirDegrees);
    }

    public function getWindDirAvg2mStep(): ?int {
        return Helpers::directionDegreeToStep($this->windDirAvg2mDegrees);
    }

    public function getWindGustDir10mStep(): ?int {
        return Helpers::directionDegreeToStep($this->windGustDir10mDegrees);
    }

    public function getWindDirText(): ?string {
        return Helpers::directionDegreeToStepText($this->windDirDegrees);
    }

    public function getWindGustDirText(): ?string {
        return Helpers::directionDegreeToStepText($this->windGustDirDegrees);
    }

    public function getWindDirAvg2mText(): ?string {
        return Helpers::directionDegreeToStepText($this->windDirAvg2mDegrees);
    }

    public function getWindGustDir10mText(): ?string {
        return Helpers::directionDegreeToStepText($this->windGustDir10mDegrees);
    }

    public function getWindSpeed(): ?float {
        return $this->windSpeedMs;
    }

    public function getWindSpeedMin(): ?float {
        return $this->windSpeedMsMin;
    }

    public function getWindSpeedMax(): ?float {
        return $this->windSpeedMsMax;
    }

    public function getWindGust(): ?float {
        return $this->windGustMs;
    }

    public function getWindSpedAvg2m(): ?float {
        return $this->windSpedMsAvg2m;
    }

    public function getWindGust10m(): ?float {
        return $this->windGustMs10m;
    }

    public function getWindSpeedMs(): ?float {
        return $this->windSpeedMs;
    }

    public function getWindSpeedMsMin(): ?float {
        return $this->windSpeedMsMin;
    }

    public function getWindSpeedMsMax(): ?float {
        return $this->windSpeedMsMax;
    }

    public function getWindGustMS(): ?float {
        return $this->windGustMs;
    }

    public function getWindSpedMSAvg2m(): ?float {
        return $this->windSpedMsAvg2m;
    }

    public function getWindGustMS10m(): ?float {
        return $this->windGustMs10m;
    }

    public function getWindSpeedMph(): ?float {
        return Helpers::speedMsToMph($this->windSpeedMs);
    }

    public function getWindSpeedMphMin(): ?float {
        return Helpers::speedMsToMph($this->windSpeedMsMin);
    }

    public function getWindSpeedMphMax(): ?float {
        return Helpers::speedMsToMph($this->windSpeedMsMax);
    }

    public function getWindGustMph(): ?float {
        return Helpers::speedMsToMph($this->windGustMs);
    }

    public function getWindSpedMphAvg2m(): ?float {
        return Helpers::speedMsToMph($this->windSpeedMsAvg2m);
    }

    public function getWindGustMph10m(): ?float {
        return Helpers::speedMsToMph($this->windGustMs10m);
    }

    public function setWindSpeed(?float $windSpeed) {
        $this->windSpeedMs = $windSpeed;
        return $this;
    }

    public function setWindSpeedMin(?float $windSpeedMin) {
        $this->windSpeedMsMin = $windSpeedMin;
        return $this;
    }

    public function setWindSpeedMax(?float $windSpeedMax) {
        $this->windSpeedMsMax = $windSpeedMax;
        return $this;
    }

    public function setWindGust(?float $windGust) {
        $this->windGustMs = $windGust;
        return $this;
    }

    public function setWindSpeedAvg2m(?float $windSpeedAvg2m) {
        $this->windSpeedMsAvg2m = $windSpeedAvg2m;
        return $this;
    }

    public function setWindGust10m(?float $windGust10m) {
        $this->windGustMs10m = $windGust10m;
        return $this;
    }

    public function setWindSpeedMs(?float $windSpeedMs) {
        $this->windSpeedMs = $windSpeedMs;
        return $this;
    }

    public function setWindSpeedMsMin(?float $windSpeedMsMin) {
        $this->windSpeedMsMin = $windSpeedMsMin;
        return $this;
    }

    public function setWindSpeedMsMax(?float $windSpeedMsMax) {
        $this->windSpeedMsMax = $windSpeedMsMax;
        return $this;
    }

    public function setWindGustMs(?float $windGustMs) {
        $this->windGustMs = $windGustMs;
        return $this;
    }

    public function setWindSpeedMsAvg2m(?float $windSpeedMsAvg2m) {
        $this->windSpeedMsAvg2m = $windSpeedMsAvg2m;
        return $this;
    }

    public function setWindGustMs10m(?float $windGustMs10m) {
        $this->windGustMs10m = $windGustMs10m;
        return $this;
    }

    public function setWindSpeedMph(?float $windSpeedMph) {
        $this->windSpeedMs = Helpers::speedMphToMs($windSpeedMph);
        return $this;
    }

    public function setWindSpeedMphMin(?float $windSpeedMphMin) {
        $this->windSpeedMsMin = Helpers::speedMphToMs($windSpeedMphMin);
        return $this;
    }

    public function setWindSpeedMphMax(?float $windSpeedMphMax) {
        $this->windSpeedMsMax = Helpers::speedMphToMs($windSpeedMphMax);
        return $this;
    }

    public function setWindGustMph(?float $windGustMph) {
        $this->windGustMs = Helpers::speedMphToMs($windGustMph);
        return $this;
    }

    public function setWindSpeedMphAvg2m(?float $windSpeedMphAvg2m) {
        $this->windSpeedMsAvg2m = Helpers::speedMphToMs($windSpeedMphAvg2m);
        return $this;
    }

    public function setWindGustMph10m(?float $windGustMph10m) {
        $this->windGustMs10m = Helpers::speedMphToMs($windGustMph10m);
        return $this;
    }

    public function setWindDirDegrees(?float $windDirDegrees) {
        $this->windDirDegrees = $windDirDegrees;
        return $this;
    }

    public function setWindGustDirDegrees(?float $windGustDirDegrees) {
        $this->windGustDirDegrees = $windGustDirDegrees;
        return $this;
    }

    public function setWindDirAvg2mDegrees(?float $windDirAvg2mDegrees) {
        $this->windDirAvg2mDegrees = $windDirAvg2mDegrees;
        return $this;
    }

    public function setWindGustDir10mDegrees(?float $windGustDir10mDegrees) {
        $this->windGustDir10mDegrees = $windGustDir10mDegrees;
        return $this;
    }

    public function setWindDir(?float $windDir) {
        $this->windDirDegrees = $windDir;
        return $this;
    }

    public function setWindGustDir(?float $windGustDir) {
        $this->windGustDirDegrees = $windGustDir;
        return $this;
    }

    public function setWindDirAvg2m(?float $windDirAvg2m) {
        $this->windDirAvg2mDegrees = $windDirAvg2m;
        return $this;
    }

    public function setWindGustDir10m(?float $windGustDir10m) {
        $this->windGustDir10mDegrees = $windGustDir10m;
        return $this;
    }

    public function setWindDirStep(?int $windDirStep) {
        $this->windDirDegrees = Helpers::directionStepToDegree($windDirStep);
        return $this;
    }

    public function setWindGustDirStep(?int $windGustDirStep) {
        $this->windGustDirDegrees = Helpers::directionStepToDegree($windGustDirStep);
        return $this;
    }

    public function setWindDirAvg2mStep(?int $windDirAvg2mStep) {
        $this->windDirAvg2mDegrees = Helpers::directionStepToDegree($windDirAvg2mStep);
        return $this;
    }

    public function setWindGustDir10mStep(?int $windGustDir10mStep) {
        $this->windGustDir10mDegrees = Helpers::directionStepToDegree($windGustDir10mStep);
        return $this;
    }

}
