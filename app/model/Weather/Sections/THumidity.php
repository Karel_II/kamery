<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Weather\Sections;

/**
 *
 * @author Karel
 * 
 * @property float|null $humidityRelative [% outdoor humidity 0-100%]
 * @property float|null $humidityAbsolute Grams of moisture per cubic meter of air (g/m3)
 * 
 */
trait THumidity {

    /**
     * [% outdoor humidity 0-100%]
     * @var float|null 
     */
    protected $humidityRelative = NULL;

    /**
     * Grams of moisture per cubic meter of air (g/m3)
     * @var float|null  
     */
    protected $humidityAbsolute = NULL;

    public function getHumidityRelative(): ?float {
        return $this->humidityRelative;
    }

    public function getHumidityAbsolute(): ?float {
        return $this->humidityAbsolute;
    }

    public function setHumidityRelative(?float $humidityRelative) {
        $this->humidityRelative = $humidityRelative;
        return $this;
    }

    public function setHumidityAbsolute(?float $humidityAbsolute) {
        $this->humidityAbsolute = $humidityAbsolute;
        return $this;
    }

}
