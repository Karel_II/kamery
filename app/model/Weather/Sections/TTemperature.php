<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Weather\Sections;

use App\Model\Weather\Helpers;

/**
 *
 * @author Karel
 * 
 * @property float|null $temperature [°C outdoor temperature]
 * @property float|null $temperature2 [°C outdoor temperature 2]
 * @property float|null $temperature3 [°C outdoor temperature 3]
 * @property float|null $temperature4 [°C outdoor temperature 4]
 * @property float|null $temperature5 [°C outdoor temperature 5]
 * @property float|null $temperatureMin [°C outdoor temperature minimal]
 * @property float|null $temperatureMax [°C outdoor temperature maximal]
 * @property float|null $temperatureWindchill [°C outdoor winchill temperature]
 * @property float|null $temperatureDewPoint [°C outdoor dewpoint °C]
 * 
 * @property float|null $temperatureC [°C outdoor temperature]
 * @property float|null $temperatureC2 [°C outdoor temperature 2]
 * @property float|null $temperatureC3 [°C outdoor temperature 3]
 * @property float|null $temperatureC4 [°C outdoor temperature 4]
 * @property float|null $temperatureC5 [°C outdoor temperature 5]
 * @property float|null $temperatureCMin [°C outdoor temperature minimal]
 * @property float|null $temperatureCMax [°C outdoor temperature maximal]
 * @property float|null $temperatureCWindchill [°C outdoor winchill temperature]
 * @property float|null $temperatureCDewPoint [°C outdoor dewpoint °C]
 * 
 * @property float|null $temperatureF [F outdoor temperature]
 * @property float|null $temperatureF2 [F outdoor temperature 2]
 * @property float|null $temperatureF3 [F outdoor temperature 2]
 * @property float|null $temperatureF4 [Foutdoor temperature 2]
 * @property float|null $temperatureF5 [Foutdoor temperature 2]
 * @property float|null $temperatureFMin [F outdoor temperature minimal]
 * @property float|null $temperatureFMax [F outdoor temperature maximal]
 * @property float|null $temperatureFWindchill [F outdoor winchill temperature]
 * @property float|null $temperatureFDewPoint [F outdoor dewpoint F]
 * 
 *  */
trait TTemperature {

    /**
     * [°C outdoor temperature]
     * @var float|null  
     */
    protected $temperatureC = NULL;

    /**
     * [°C outdoor temperature 2]
     * @var float|null  
     */
    protected $temperatureC2 = NULL;

    /**
     * [°C outdoor temperature 3]
     * @var float|null  
     */
    protected $temperatureC3 = NULL;

    /**
     * [°C outdoor temperature 4]
     * @var float|null  
     */
    protected $temperatureC4 = NULL;

    /**
     * [°C outdoor temperature 5]
     * @var float|null  
     */
    protected $temperatureC5 = NULL;

    /**
     * [°C outdoor temperature minimal]
     * @var float|null  
     */
    protected $temperatureCMin = NULL;

    /**
     * [°C outdoor temperature maximal] 
     * @var float|null  
     */
    protected $temperatureCMax = NULL;

    /**
     * [°C outdoor winchill temperature]
     * @var float|null  
     */
    protected $temperatureCWindchill = NULL;

    /**
     * [°C outdoor dewpoint °C]
     * @var float|null 
     */
    protected $temperatureCDewPoint = NULL;

    public function getTemperature(): ?float {
        return $this->temperatureC;
    }

    public function getTemperature2(): ?float {
        return $this->temperatureC2;
    }

    public function getTemperature3(): ?float {
        return $this->temperatureC3;
    }

    public function getTemperature4(): ?float {
        return $this->temperatureC4;
    }

    public function getTemperature5(): ?float {
        return $this->temperatureC5;
    }

    public function getTemperatureMin(): ?float {
        return $this->temperatureCMin;
    }

    public function getTemperatureMax(): ?float {
        return $this->temperatureCMax;
    }

    public function getTemperatureWindchill(): ?float {
        return $this->temperatureCWindchill;
    }

    public function getTemperatureDewPoint(): ?float {
        return $this->temperatureCDewPoint;
    }

    public function getTemperatureC(): ?float {
        return $this->temperatureC;
    }

    public function getTemperatureC2(): ?float {
        return $this->temperatureC2;
    }

    public function getTemperatureC3(): ?float {
        return $this->temperatureC3;
    }

    public function getTemperatureC4(): ?float {
        return $this->temperatureC4;
    }

    public function getTemperatureC5(): ?float {
        return $this->temperatureC5;
    }

    public function getTemperatureCMin(): ?float {
        return $this->temperatureCMin;
    }

    public function getTemperatureCMax(): ?float {
        return $this->temperatureCMax;
    }

    public function getTemperatureCWindchill(): ?float {
        return $this->temperatureCWindchill;
    }

    public function getTemperatureCDewPoint(): ?float {
        return $this->temperatureCDewPoint;
    }

    public function getTemperatureF(): ?float {
        return Helpers::celsiusToFahrenheit($this->temperatureC);
    }

    public function getTemperatureF2(): ?float {
        return Helpers::celsiusToFahrenheit($this->temperatureC2);
    }

    public function getTemperatureF3(): ?float {
        return Helpers::celsiusToFahrenheit($this->temperatureC3);
    }

    public function getTemperatureF4(): ?float {
        return Helpers::celsiusToFahrenheit($this->temperatureC4);
    }

    public function getTemperatureF5(): ?float {
        return Helpers::celsiusToFahrenheit($this->temperatureC5);
    }

    public function getTemperatureFMin(): ?float {
        return Helpers::celsiusToFahrenheit($this->temperatureCMin);
    }

    public function getTemperatureFMax(): ?float {
        return Helpers::celsiusToFahrenheit($this->temperatureCMax);
    }

    public function getTemperatureFWindchill(): ?float {
        return Helpers::celsiusToFahrenheit($this->temperatureCWindchill);
    }

    public function getTemperatureFDewPoint(): ?float {
        return Helpers::celsiusToFahrenheit($this->temperatureCDewPoint);
    }

    public function setTemperature(?float $temperature) {
        $this->temperatureC = $temperature;
        return $this;
    }

    public function setTemperature2(?float $temperature2) {
        $this->temperatureC2 = $temperature2;
        return $this;
    }

    public function setTemperature3(?float $temperature3) {
        $this->temperatureC3 = $temperature3;
        return $this;
    }

    public function setTemperature4(?float $temperature4) {
        $this->temperatureC4 = $temperature4;
        return $this;
    }

    public function setTemperature5(?float $temperature5) {
        $this->temperatureC5 = $temperature5;
        return $this;
    }

    public function setTemperatureMin(?float $temperatureMin) {
        $this->temperatureCMin = $temperatureMin;
        return $this;
    }

    public function setTemperatureMax(?float $temperatureMax) {
        $this->temperatureCMax = $temperatureMax;
        return $this;
    }

    public function setTemperatureWindchill(?float $temperatureWindchill) {
        $this->temperatureCWindchill = $temperatureWindchill;
        return $this;
    }

    public function setTemperatureDewPoint(?float $temperatureDewPoint) {
        $this->temperatureCDewPoint = $temperatureDewPoint;
        return $this;
    }

    public function setTemperatureC(?float $temperatureC) {
        $this->temperatureC = $temperatureC;
        return $this;
    }

    public function setTemperatureC2(?float $temperatureC2) {
        $this->temperatureC2 = $temperatureC2;
        return $this;
    }

    public function setTemperatureC3(?float $temperatureC3) {
        $this->temperatureC3 = $temperatureC3;
        return $this;
    }

    public function setTemperatureC4(?float $temperatureC4) {
        $this->temperatureC4 = $temperatureC4;
        return $this;
    }

    public function setTemperatureC5(?float $temperatureC5) {
        $this->temperatureC5 = $temperatureC5;
        return $this;
    }

    public function setTemperatureCMin(?float $temperatureCMin) {
        $this->temperatureCMin = $temperatureCMin;
        return $this;
    }

    public function setTemperatureCMax(?float $temperatureCMax) {
        $this->temperatureCMax = $temperatureCMax;
        return $this;
    }

    public function setTemperatureCWindchill(?float $temperatureCWindchill) {
        $this->temperatureCWindchill = $temperatureCWindchill;
        return $this;
    }

    public function setTemperatureCDewPoint(?float $temperatureCDewPoint) {
        $this->temperatureCDewPoint = $temperatureCDewPoint;
        return $this;
    }

    public function setTemperatureF(?float $temperatureF) {
        $this->temperatureC = Helpers::fahrenheitToCelsius($temperatureF);
        return $this;
    }

    public function setTemperatureF2(?float $temperatureF2) {
        $this->temperatureC2 = Helpers::fahrenheitToCelsius($temperatureF2);
        return $this;
    }

    public function setTemperatureF3(?float $temperatureF3) {
        $this->temperatureC3 = Helpers::fahrenheitToCelsius($temperatureF3);
        return $this;
    }

    public function setTemperatureF4(?float $temperatureF4) {
        $this->temperatureC4 = Helpers::fahrenheitToCelsius($temperatureF4);
        return $this;
    }

    public function setTemperatureF5(?float $temperatureF5) {
        $this->temperatureC5 = Helpers::fahrenheitToCelsius($temperatureF5);
        return $this;
    }

    public function setTemperatureFMin(?float $temperatureFMin) {
        $this->temperatureCMin = Helpers::fahrenheitToCelsius($temperatureFMin);
        return $this;
    }

    public function setTemperatureFMax(?float $temperatureFMax) {
        $this->temperatureCMax = Helpers::fahrenheitToCelsius($temperatureFMax);
        return $this;
    }

    public function setTemperatureFWindchill(?float $temperatureFWindchill) {
        $this->temperatureCWindchill = Helpers::fahrenheitToCelsius($temperatureFWindchill);
        return $this;
    }

    public function setTemperatureFDewPoint(?float $temperatureFDewPoint) {
        $this->temperatureCDewPoint = Helpers::fahrenheitToCelsius($temperatureFDewPoint);
        return $this;
    }

}
