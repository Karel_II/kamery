<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Weather\Sections;

use App\Model\Weather\Helpers;

/**
 * Description of TSystem
 *
 * @author Karel
 * 
 * 
 * @property string|null $deviceName
 * @property bool $error
 * @property string|null $softwareType
 * 
 * @property float|null $temperatureSystem
 * @property float|null $temperatureCSystem
 * @property float|null $temperatureFSystem
 */
trait TSystem {

    /**
     *
     * @var string|null 
     */
    protected $deviceName = NULL;

    /**
     *
     * @var string|null 
     */
    protected $softwareType = NULL;

    /**
     *
     * @var bool 
     */
    protected $error = false;

    /**
     *
     * @var float|null 
     */
    protected $temperatureCSystem = NULL;

    public function getDeviceName(): ?string {
        return $this->deviceName;
    }

    public function getSoftwareType(): ?string {
        return $this->softwareType;
    }

    public function getError(): bool {
        return $this->error;
    }

    public function getTemperatureSystem(): ?float {
        return $this->temperatureCSystem;
    }

    public function getTemperatureCSystem(): ?float {
        return $this->temperatureCSystem;
    }

    public function getTemperatureFSystem(): ?float {
        return Helpers::celsiusToFahrenheit($this->temperatureCSystem);
    }

    public function setDeviceName(?string $deviceName) {
        $this->deviceName = $deviceName;
        return $this;
    }

    public function setSoftwareType(?string $softwareType) {
        $this->softwareType = $softwareType;
        return $this;
    }

    public function setError(bool $error) {
        $this->error = $error;
        return $this;
    }

    public function setTemperatureSystem(?float $temperatureSystem) {
        $this->temperatureCSystem = $temperatureSystem;
        return $this;
    }

    public function setTemperatureCSystem(?float $temperatureCSystem) {
        $this->temperatureCSystem = $temperatureCSystem;
        return $this;
    }

    public function setTemperatureFSystem(?float $temperatureFSystem) {
        $this->temperatureCSystem = Helpers::fahrenheitToCelsius($temperatureFSystem);
        return $this;
    }

}
