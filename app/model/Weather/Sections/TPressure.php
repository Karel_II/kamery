<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Weather\Sections;

use App\Model\Weather\Helpers;

/**
 *
 * @author Karel
 * 
 * @property float|null $pressure [barometric pressure hPa]
 * @property float|null $pressureMin [barometric minimal pressure hPa]
 * @property float|null $pressureMax [barometric maximal pressure hPa]
 * 
 * @property float|null $pressureHPa [barometric pressure hPa]
 * @property float|null $pressureHPaMin [barometric minimal pressure hPa]
 * @property float|null $pressureHPaMax [barometric maximal pressure hPa]
 * 
 * @property float|null $pressureIn [barometric pressure inches]
 * @property float|null $pressureInMin [barometric minimal pressure inches]
 * @property float|null $pressureInMax [barometric maximal pressure inches] 
 * 
 * @property float|null $altitudeBarometric 
 */
trait TPressure {


    /**
     *
     * @var float|null 
     */
    protected $pressureHPa = NULL;

    /**
     *
     * @var float|null 
     */
    protected $pressureHPaMin = NULL;

    /**
     *
     * @var float|null 
     */
    protected $pressureHPaMax = NULL;

    /**
     *
     * @var float|null 
     */
    protected $altitudeBarometric = NULL;

    public function getPressure(): ?float {
        return $this->pressureHPa;
    }

    public function getPressureMin(): ?float {
        return $this->pressureHPaMin;
    }

    public function getPressureMax(): ?float {
        return $this->pressureHPaMax;
    }

    public function getPressureHPa(): ?float {
        return $this->pressureHPa;
    }

    public function getPressureHPaMin(): ?float {
        return $this->pressureHPaMin;
    }

    public function getPressureHPaMax(): ?float {
        return $this->pressureHPaMax;
    }

    public function getPressureIn(): ?float {
        return Helpers::pressureHPaToIn($this->pressureHPa);
    }

    public function getPressureInMin(): ?float {
        return Helpers::pressureHPaToIn($this->pressureHPaMin);
    }

    public function getPressureInMax(): ?float {
        return Helpers::pressureHPaToIn($this->pressureHPaMax);
    }

    public function getAltitudeBarometric(): ?float {
        return $this->altitudeBarometric;
    }

    ///




    public function setPressure(?float $pressure) {
        $this->pressureHPa = $pressure;
        return $this;
    }

    public function setPressureMin(?float $pressureMin) {
        $this->pressureHPaMin = $pressureMin;
        return $this;
    }

    public function setPressureMax(?float $pressureMax) {
        $this->pressureHPaMax = $pressureMax;
        return $this;
    }

    public function setPressureHPa(?float $pressureHPa) {
        $this->pressureHPa = $pressureHPa;
        return $this;
    }

    public function setPressureHPaMin(?float $pressureHPaMin) {
        $this->pressureHPaMin = $pressureHPaMin;
        return $this;
    }

    public function setPressureHPaMax(?float $pressureHPaMax) {
        $this->pressureHPaMax = $pressureHPaMax;
        return $this;
    }

    public function setPressureIn(?float $pressureIn) {
        $this->pressureHPa = Helpers::pressureInToHPa($pressureIn);
        return $this;
    }

    public function setPressureInMin(?float $pressureInMin) {
        $this->pressureHPaMin = Helpers::pressureInToHPa($pressureInMin);
        return $this;
    }

    public function setPressureInMax(?float $pressureInMax) {
        $this->pressureHPaMax = Helpers::pressureInToHPa($pressureInMax);
        return $this;
    }

    public function setAltitudeBarometric(?float $altitudeBarometric) {
        $this->altitudeBarometric = $altitudeBarometric;
        return $this;
    }

}
