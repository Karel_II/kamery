<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Weather\Sections;

use Nette;

/**
 *
 * @author Karel
 * 
 * @property float|null $AqNO [ NO (nitric oxide) ppb ]
 * @property float|null $AqNO2T (nitrogen dioxide), true measure ppb
 * @property float|null $AqNO2 NO2 computed, NOx-NO ppb
 * @property float|null $AqNO2Y NO2 computed, NOy-NO ppb
 * @property float|null $AqNOX NOx (nitrogen oxides) - ppb
 * @property float|null $AqNOY NOy (total reactive nitrogen) - ppb
 * @property float|null $AqNO3 NO3 ion (nitrate, not adjusted for ammonium ion) UG/M3
 * @property float|null $AqSO4 SO4 ion (sulfate, not adjusted for ammonium ion) UG/M3
 * @property float|null $AqSO2 (sulfur dioxide), conventional ppb
 * @property float|null $AqSO2T trace levels ppb
 * @property float|null $AqCO CO (carbon monoxide), conventional ppm
 * @property float|null $AqCOT CO trace levels ppb
 * @property float|null $AqEC EC (elemental carbon) – PM2.5 UG/M3
 * @property float|null $AqOC OC (organic carbon, not adjusted for oxygen and hydrogen) – PM2.5 UG/M3
 * @property float|null $AqBC BC (black carbon at 880 nm) UG/M3
 * @property float|null $AqUV-AETH  UV-AETH (second channel of Aethalometer at 370 nm) UG/M3
 * @property float|null $AqPM2.5 PM2.5 mass - UG/M3
 * @property float|null $AqPM10 PM10 mass - PM10 mass
 * @property float|null $AqOZONE Ozone - ppb
 */
trait TPolution {


    /**
     * [NO (nitric oxide) ppb]
     * @var float|null 
     */
    protected $AqNO = NULL;

    /**
     * (nitrogen dioxide), true measure ppb
     * @var float|null 
     */
    protected $AqNO2T = NULL;

    /**
     * NO2 computed, NOx-NO ppb
     * @var float|null 
     */
    protected $AqNO2 = NULL;

    /**
     * NO2 computed, NOy-NO ppb
     * @var float|null 
     */
    protected $AqNO2Y = NULL;

    /**
     * NOx (nitrogen oxides) - ppb
     * @var float|null 
     */
    protected $AqNOX = NULL;

    /**
     * NOy (total reactive nitrogen) - ppb
     * @var float|null 
     */
    protected $AqNOY = NULL;

    /**
     * NO3 ion (nitrate, not adjusted for ammonium ion) UG/M3
     * @var float|null 
     */
    protected $AqNO3 = NULL;

    /**
     * SO4 ion (sulfate, not adjusted for ammonium ion) UG/M3
     * @var float|null 
     */
    protected $AqSO4 = NULL;

    /**
     * (sulfur dioxide), conventional ppb
     * @var float|null 
     */
    protected $AqSO2 = NULL;

    /**
     * trace levels ppb
     * @var float|null 
     */
    protected $AqSO2T = NULL;

    /**
     * CO (carbon monoxide), conventional ppm
     * @var float|null 
     */
    protected $AqCO = NULL;

    /**
     * CO trace levels ppb
     * @var float|null 
     */
    protected $AqCOT = NULL;

    /**
     * EC (elemental carbon) – PM2.5 UG/M3
     * @var float|null 
     */
    protected $AqEC = NULL;

    /**
     * OC (organic carbon, not adjusted for oxygen and hydrogen) – PM2.5 UG/M3
     * @var float|null 
     */
    protected $AqOC = NULL;

    /**
     * BC (black carbon at 880 nm) UG/M3
     * @var float|null 
     */
    protected $AqBC = NULL;

    /**
     * UV-AETH (second channel of Aethalometer at 370 nm) UG/M3
     * @var float|null 
     */
    protected $AqUV_AETH = NULL;

    /**
     * PM2.5 mass - UG/M3
     * @var float|null 
     */
    protected $AqPM2_5 = NULL;

    /**
     * PM10 mass - PM10 mass
     * @var float|null 
     */
    protected $AqPM10 = NULL;

    /**
     * Ozone - ppb
     * @var float|null 
     */
    protected $AqOZONE = NULL;

    public function getAqNO(): ?float {
        return $this->AqNO;
    }

    public function getAqNO2T(): ?float {
        return $this->AqNO2T;
    }

    public function getAqNO2(): ?float {
        return $this->AqNO2;
    }

    public function getAqNO2Y(): ?float {
        return $this->AqNO2Y;
    }

    public function getAqNOX(): ?float {
        return $this->AqNOX;
    }

    public function getAqNOY(): ?float {
        return $this->AqNOY;
    }

    public function getAqNO3(): ?float {
        return $this->AqNO3;
    }

    public function getAqSO4(): ?float {
        return $this->AqSO4;
    }

    public function getAqSO2(): ?float {
        return $this->AqSO2;
    }

    public function getAqSO2T(): ?float {
        return $this->AqSO2T;
    }

    public function getAqCO(): ?float {
        return $this->AqCO;
    }

    public function getAqCOT(): ?float {
        return $this->AqCOT;
    }

    public function getAqEC(): ?float {
        return $this->AqEC;
    }

    public function getAqOC(): ?float {
        return $this->AqOC;
    }

    public function getAqBC(): ?float {
        return $this->AqBC;
    }

    public function getAqUV_AETH(): ?float {
        return $this->AqUV_AETH;
    }

    public function getAqPM2_5(): ?float {
        return $this->AqPM2_5;
    }

    public function getAqPM10(): ?float {
        return $this->AqPM10;
    }

    public function getAqOZONE(): ?float {
        return $this->AqOZONE;
    }

    public function setAqNO(?float $AqNO) {
        $this->AqNO = $AqNO;
        return $this;
    }

    public function setAqNO2T(?float $AqNO2T) {
        $this->AqNO2T = $AqNO2T;
        return $this;
    }

    public function setAqNO2(?float $AqNO2) {
        $this->AqNO2 = $AqNO2;
        return $this;
    }

    public function setAqNO2Y(?float $AqNO2Y) {
        $this->AqNO2Y = $AqNO2Y;
        return $this;
    }

    public function setAqNOX(?float $AqNOX) {
        $this->AqNOX = $AqNOX;
        return $this;
    }

    public function setAqNOY(?float $AqNOY) {
        $this->AqNOY = $AqNOY;
        return $this;
    }

    public function setAqNO3(?float $AqNO3) {
        $this->AqNO3 = $AqNO3;
        return $this;
    }

    public function setAqSO4(?float $AqSO4) {
        $this->AqSO4 = $AqSO4;
        return $this;
    }

    public function setAqSO2(?float $AqSO2) {
        $this->AqSO2 = $AqSO2;
        return $this;
    }

    public function setAqSO2T(?float $AqSO2T) {
        $this->AqSO2T = $AqSO2T;
        return $this;
    }

    public function setAqCO(?float $AqCO) {
        $this->AqCO = $AqCO;
        return $this;
    }

    public function setAqCOT(?float $AqCOT) {
        $this->AqCOT = $AqCOT;
        return $this;
    }

    public function setAqEC(?float $AqEC) {
        $this->AqEC = $AqEC;
        return $this;
    }

    public function setAqOC(?float $AqOC) {
        $this->AqOC = $AqOC;
        return $this;
    }

    public function setAqBC(?float $AqBC) {
        $this->AqBC = $AqBC;
        return $this;
    }

    public function setAqUV_AETH(?float $AqUV_AETH) {
        $this->AqUV_AETH = $AqUV_AETH;
        return $this;
    }

    public function setAqPM2_5(?float $AqPM2_5) {
        $this->AqPM2_5 = $AqPM2_5;
        return $this;
    }

    public function setAqPM10(?float $AqPM10) {
        $this->AqPM10 = $AqPM10;
        return $this;
    }

    public function setAqOZONE(?float $AqOZONE) {
        $this->AqOZONE = $AqOZONE;
        return $this;
    }

}
