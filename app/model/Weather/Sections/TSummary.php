<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Weather\Sections;

use App\Model\Weather\Helpers;

/**
 *
 * @author Karel
 * 
 * 
 * @property int|null $season
 * @property string|null $seasonText
 * @property int|null $period
 * @property-read string|null $periodText
 * @property int|null $periodUnit
 * @property-read string|null $periodUnitText
 * 
 * @property string|null $weather [text] -- metar style (+RA)
 * @property string|null $clouds [text] -- SKC, FEW, SCT, BKN, OVC
 * @property float|null $visibility [nm visibility]
 * 
 */
trait TSummary {

    /**
     *
     * @var int|null
     */
    protected $season = NULL;

    /**
     *
     * @var int|null
     */
    protected $period = null;

    /**
     *
     * @var int|null
     */
    protected $periodUnit = 0;

    /**
     *
     * @var string|null
     */
    protected $weather = null;

    /**
     *
     * @var string|null
     */
    protected $clouds = null;

    /**
     *
     * @var float|null
     */
    protected $visibility = null;

    public function getSeason(): ?int {
        return $this->season;
    }

    public function getSeasonText(): ?string {

        return Helpers::seasonIndexToName($this->season);
    }

    public function getPeriod(): ?int {
        return $this->period;
    }

    public function getPeriodText() {
        return Helpers::periodUnitIndexToName($this->periodUnit) . ': ' . round($this->period);
    }

    public function getPeriodUnit(): ?int {
        return $this->periodUnit;
    }

    public function getPeriodUnitText() {
        return Helpers::periodUnitIndexToName($this->periodUnit);
    }

    public function getWeather(): ?string {
        return $this->weather;
    }

    public function getClouds(): ?string {
        return $this->clouds;
    }

    public function getVisibility(): ?float {
        return $this->visibility;
    }

    public function setSeason(?int $season) {
        $this->season = $season;
        return $this;
    }

    public function setSeasonText(?string $seasonText) {
        $this->season = Helpers::seasonNameToIndex($seasonText);
        return $this;
    }

    public function setPeriod(?int $period) {
        $this->period = $period;
        return $this;
    }

    public function setPeriodUnit(?int $periodUnit) {
        $this->periodUnit = $periodUnit;
        return $this;
    }

    public function setWeather(?string $weather) {
        $this->weather = $weather;
        return $this;
    }

    public function setClouds(?string $clouds) {
        $this->clouds = $clouds;
        return $this;
    }

    public function setVisibility(?float $visibility) {
        $this->visibility = $visibility;
        return $this;
    }

}
