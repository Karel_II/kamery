<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Weather\Sections;

use App\Model\Weather\Helpers;

/**
 *
 * @author Karel
 * 
 * @property float|null $temperatureC [°C outdoor temperature]
 * @property float|null $temperatureC2 [°C outdoor temperature 2]
 * @property float|null $temperatureC3 [°C outdoor temperature 3]
 * @property float|null $temperatureC4 [°C outdoor temperature 4]
 * @property float|null $temperatureC5 [°C outdoor temperature 5] 
 * 
 * @property float|null $temperatureF [F outdoor temperature]
 * @property float|null $temperatureF2 [F outdoor temperature 2]
 * @property float|null $temperatureF3 [F outdoor temperature 2]
 * @property float|null $temperatureF4 [Foutdoor temperature 2]
 * @property float|null $temperatureF5 [Foutdoor temperature 2]
 * 
 * @property float|null $soilMoisture [%]
 * @property float|null $soilMoisture2 [%]
 * @property float|null $soilMoisture3 [%]
 * @property float|null $soilMoisture4 [%]
 * @property float|null $soilMoisture5 [%] 
 * 
 * @property float|null $leafWetness [%]
 * @property float|null $leafWetness2 [%]
 * @property float|null $leafWetness3 [%]
 * @property float|null $leafWetness4 [%]
 * @property float|null $leafWetness5 [%]
 */
trait TAgriculture {

    /**
     * [°C soil temperature]
     * @var float|null 
     */
    protected $soilTemperatureC = NULL;

    /**
     * [°C soil temperature]
     * @var float|null 
     */
    protected $soilTemperatureC2 = NULL;

    /**
     * [°C soil temperature]
     * @var float|null 
     */
    protected $soilTemperatureC3 = NULL;

    /**
     * [°C soil temperature]
     * @var float|null 
     */
    protected $soilTemperatureC4 = NULL;

    /**
     * [°C soil temperature]
     * @var float|null 
     */
    protected $soilTemperatureC5 = NULL;

    /**
     * [%]
     * @var float|null 
     */
    protected $soilMoisture = NULL;

    /**
     * [%]
     * @var float|null 
     */
    protected $soilMoisture2 = NULL;

    /**
     * [%]
     * @var float|null 
     */
    protected $soilMoisture3 = NULL;

    /**
     * [%]
     * @var float|null 
     */
    protected $soilMoisture4 = NULL;

    /**
     * [%]
     * @var float|null 
     */
    protected $soilMoisture5 = NULL;

    /**
     * [%]
     * @var float|null 
     */
    protected $leafWetness = NULL;

    /**
     * [%]
     * @var float|null 
     */
    protected $leafWetness2 = NULL;

    /**
     * [%]
     * @var float|null 
     */
    protected $leafWetness3 = NULL;

    /**
     * [%]
     * @var float|null 
     */
    protected $leafWetness4 = NULL;

    /**
     * [%]
     * @var float|null 
     */
    protected $leafWetness5 = NULL;

    public function getSoilTemperatureC(): ?float {
        return $this->soilTemperatureC;
    }

    public function getSoilTemperatureC2(): ?float {
        return $this->soilTemperatureC2;
    }

    public function getSoilTemperatureC3(): ?float {
        return $this->soilTemperatureC3;
    }

    public function getSoilTemperatureC4(): ?float {
        return $this->soilTemperatureC4;
    }

    public function getSoilTemperatureC5(): ?float {
        return $this->soilTemperatureC5;
    }

    public function getSoilTemperatureF(): ?float {
        return Helpers::celsiusToFahrenheit($this->soilTemperatureC);
    }

    public function getSoilTemperatureF2(): ?float {
        return Helpers::celsiusToFahrenheit($this->soilTemperatureC2);
    }

    public function getSoilTemperatureF3(): ?float {
        return Helpers::celsiusToFahrenheit($this->soilTemperatureC3);
    }

    public function getSoilTemperatureF4(): ?float {
        return Helpers::celsiusToFahrenheit($this->soilTemperatureC4);
    }

    public function getSoilTemperatureF5(): ?float {
        return Helpers::celsiusToFahrenheit($this->soilTemperatureC5);
    }

    public function getSoilMoisture(): ?float {
        return $this->soilMoisture;
    }

    public function getSoilMoisture2(): ?float {
        return $this->soilMoisture2;
    }

    public function getSoilMoisture3(): ?float {
        return $this->soilMoisture3;
    }

    public function getSoilMoisture4(): ?float {
        return $this->soilMoisture4;
    }

    public function getSoilMoisture5(): ?float {
        return $this->soilMoisture5;
    }

    public function getLeafWetness(): ?float {
        return $this->leafWetness;
    }

    public function getLeafWetness2(): ?float {
        return $this->leafWetness2;
    }

    public function getLeafWetness3(): ?float {
        return $this->leafWetness3;
    }

    public function getLeafWetness4(): ?float {
        return $this->leafWetness4;
    }

    public function getLeafWetness5(): ?float {
        return $this->leafWetness5;
    }

    ///

    public function setSoilTemperatureC(?float $soilTemperatureC) {
        $this->soilTemperatureC = $soilTemperatureC;
        return $this;
    }

    public function setSoilTemperatureC2(?float $soilTemperatureC2) {
        $this->soilTemperatureC2 = $soilTemperatureC2;
        return $this;
    }

    public function setSoilTemperatureC3(?float $soilTemperatureC3) {
        $this->soilTemperatureC3 = $soilTemperatureC3;
        return $this;
    }

    public function setSoilTemperatureC4(?float $soilTemperatureC4) {
        $this->soilTemperatureC4 = $soilTemperatureC4;
        return $this;
    }

    public function setSoilTemperatureC5(?float $soilTemperatureC5) {
        $this->soilTemperatureC5 = $soilTemperatureC5;
        return $this;
    }

    public function setSoilTemperatureF(?float $soilTemperatureF) {
        $this->soilTemperatureC = Helpers::fahrenheitToCelsius($soilTemperatureF);
        return $this;
    }

    public function setSoilTemperatureF2(?float $soilTemperatureF2) {
        $this->soilTemperatureC2 = Helpers::fahrenheitToCelsius($soilTemperatureF2);
        return $this;
    }

    public function setSoilTemperatureF3(?float $soilTemperatureF3) {
        $this->soilTemperatureC3 = Helpers::fahrenheitToCelsius($soilTemperatureF3);
        return $this;
    }

    public function setSoilTemperatureF4(?float $soilTemperatureF4) {
        $this->soilTemperatureC4 = Helpers::fahrenheitToCelsius($soilTemperatureF4);
        return $this;
    }

    public function setSoilTemperatureF5(?float $soilTemperatureF5) {
        $this->soilTemperatureC5 = Helpers::fahrenheitToCelsius($soilTemperatureF5);
        return $this;
    }

    public function setSoilMoisture(?float $soilMoisture) {
        $this->soilMoisture = $soilMoisture;
        return $this;
    }

    public function setSoilMoisture2(?float $soilMoisture2) {
        $this->soilMoisture2 = $soilMoisture2;
        return $this;
    }

    public function setSoilMoisture3(?float $soilMoisture3) {
        $this->soilMoisture3 = $soilMoisture3;
        return $this;
    }

    public function setSoilMoisture4(?float $soilMoisture4) {
        $this->soilMoisture4 = $soilMoisture4;
        return $this;
    }

    public function setSoilMoisture5(?float $soilMoisture5) {
        $this->soilMoisture5 = $soilMoisture5;
        return $this;
    }

    public function setLeafWetness(?float $leafWetness) {
        $this->leafWetness = $leafWetness;
        return $this;
    }

    public function setLeafWetness2(?float $leafWetness2) {
        $this->leafWetness2 = $leafWetness2;
        return $this;
    }

    public function setLeafWetness3(?float $leafWetness3) {
        $this->leafWetness3 = $leafWetness3;
        return $this;
    }

    public function setLeafWetness4(?float $leafWetness4) {
        $this->leafWetness4 = $leafWetness4;
        return $this;
    }

    public function setLeafWetness5(?float $leafWetness5) {
        $this->leafWetness5 = $leafWetness5;
        return $this;
    }

}
