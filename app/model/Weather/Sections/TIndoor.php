<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Weather\Sections;

use App\Model\Weather\Helpers;

/**
 *
 * @author Karel
 * 
 * @property float|null $indoorTemperatureC [°C indoor temperature]
 * @property float|null $indoorTemperatureF [F indoor temperature]
 * @property float|null $indoorHumidityRelative [% indoor humidity 0-100%]
 */
trait TIndoor {

    /**
     * [°C indoor temperature]
     * @var float|null  
     */
    protected $indoorTemperatureC = NULL;

    /**
     * [% indoor humidity 0-100%]
     * @var float|null 
     */
    protected $indoorHumidityRelative = NULL;

    public function getIndoorTemperatureC(): ?float {
        return $this->indoorTemperatureC;
    }

    public function getIndoorTemperatureF(): ?float {
        return Helpers::celsiusToFahrenheit($this->indoorTemperatureC);
    }

    public function setIndoorTemperatureC(?float $indoorTemperatureC) {
        $this->indoorTemperatureC = $indoorTemperatureC;
        return $this;
    }

    public function setIndoorTemperatureF(?float $indoorTemperatureF) {
        $this->indoorTemperatureC = Helpers::fahrenheitToCelsius($indoorTemperatureF);
        return $this;
    }

}
