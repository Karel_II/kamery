<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Weather\Sections;

use App\Model\Weather\Helpers;

/**
 *
 * @author Karel
 * 
 * @property float|null $rainMm [rain mm over the past hour)] -- the accumulated rainfall in the past 60 min
 * @property float|null $dailyRainMm [rain mm so far today in local time]
 * 
 * @property float|null $rainIn [rain inches over the past hour)] -- the accumulated rainfall in the past 60 min
 * @property float|null $dailyRainIn [rain inches so far today in local time]
 * 
 */
trait TRain {

    /**
     * [rain mm over the past hour)] -- the accumulated rainfall in the past 60 min
     * @var float|null 
     */
    protected $rainMm = NULL;

    /**
     * [rain mm so far today in local time]
     * @var float|null 
     */
    protected $dailyRainMm = NULL;

    public function getRainMm(): ?float {
        return $this->rainMm;
    }

    public function getDailyRainMm(): ?float {
        return $this->dailyRainMm;
    }

    public function getRainIn(): ?float {
        return Helpers::sizeMmToIn($this->rainMm);
    }

    public function getDailyRainIn(): ?float {
        return Helpers::sizeMmToIn($this->dailyRainMm);
    }

    public function setRainIn(?float $rainIn) {
        $this->rainMm = Helpers::sizeInToMM($rainIn);
        return $this;
    }

    public function setDailyRainIn(?float $dailyRainIn) {
        $this->dailyRainMm = Helpers::sizeInToMM($dailyRainIn);
        return $this;
    }

}
