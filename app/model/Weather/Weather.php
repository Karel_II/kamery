<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Weather;

use Nette;
use Nette\Utils\DateTime;

/**
 * Description of Weather
 *
 * @author Karel
 * 
 * @property Nette\Utils\DateTime|null $datetime
 */
class Weather {

    use \Nette\SmartObject;

    use Sections\TAgriculture,
        Sections\THumidity,
        Sections\TIndoor,
        Sections\TPolution,
        Sections\TPressure,
        Sections\TRain,
        Sections\TSummary,
        Sections\TSun,
        Sections\TSystem,
        Sections\TTemperature,
        Sections\TWind;

    /**
     *
     * @var Nette\Utils\DateTime|null
     */
    private $datetime = null;

    public function getDatetime() {
        return $this->datetime;
    }

    public function setDatetime(DateTime $datetime) {
        $this->datetime = $datetime;
        return $this;
    }

    public static function newWeather($class, $dataFile) {
        $newClass = static::class . '\\' . $class;
        $newFunction = 'new' . $class;
        return $newClass::$newFunction($dataFile);
    }

}
