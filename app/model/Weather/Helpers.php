<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Weather;

/**
 * Description of Helpers
 *
 * @author Karel
 */
class Helpers {

    private const WIND_STEP_DEG = 22.5;
    private const WIND_DIRECTION_TEXT = ['S', 'SSV', 'SV', 'VSV', 'V', 'VJV', 'JV', 'JJV', 'J', 'JJZ', 'JZ', 'ZJZ', 'Z', 'ZSZ', 'SZ', 'SSZ'];
    private const WIND_BEAUFORT_SCALE_TEXT = ['Calm', 'Light air', 'Light breeze', 'Gentle breeze', 'Moderate breeze', 'Fresh breeze', 'Strong breeze', 'High wind,moderate gale,near gale', 'Gale,fresh gale', 'Strong/severe gale', 'Storm,whole gale', 'Violent storm', 'Light Hurricane', 'Moderate Hurricane', 'Strong Hurricane', 'Very Strong Hurricane', 'Devastating Hurricane'];
    private const SPEED_MS_MPH = 2.23694;
    private const PRESSURE_IN_HPA = 33.863886666667;
    private const SIZE_IN_MM = 25.4;
    private const SEASON_NAMES = ['winter', 'spring', 'summer', 'autumn'];
    private const PERIOD_NAMES = ['Day', 'Sol'];

    /**
     * Fahrenheit to Celsius conversion
     * @param float|null $value
     * @return float|null
     */
    public static function fahrenheitToCelsius(?float $value): ?float {
        return ($value === NULL) ? NULL : (($value - 32) * 5) / 9;
    }

    /**
     * Celsius to Fahrenheit conversion
     * @param float|null $value
     * @return float|null
     */
    public static function celsiusToFahrenheit(?float $value): ?float {
        return ($value === NULL) ? NULL : (($value * 9) / 5) + 32;
    }

    /**
     * mm to inch conversion
     * @param float|null $sizeMm
     * @return float|null
     */
    public static function sizeMmToIn(?float $sizeMm): ?float {
        return ($sizeMm === NULL) ? NULL : $sizeMm * self::SIZE_IN_MM;
    }

    /**
     * inch to mm conversion
     * @param float|null $sizeIn
     * @return float|null
     */
    public static function sizeInToMM(?float $sizeIn): ?float {
        return ($sizeIn === NULL) ? NULL : $sizeIn / self::SIZE_IN_MM;
    }

    /**
     * Convert hPa to In
     * @param float|null $pressureHPa
     * @return float|null
     */
    public static function pressureHPaToIn(?float $pressureHPa): ?float {
        return ($pressureHPa === NULL) ? NULL : $pressureHPa / self::PRESSURE_IN_HPA;
    }

    /**
     * Convert In to hPa
     * @param float|null $pressureIn
     * @return float|null
     */
    public static function pressureInToHPa(?float $pressureIn): ?float {
        return ($pressureIn === NULL) ? NULL : $pressureIn * self::PRESSURE_IN_HPA;
    }

    /**
     * Convert [0-360] degrees to [0-16] steps
     * @param float $directionDegree
     * @return int
     */
    public static function directionDegreeToStep(float $directionDegree): int {
        return (int) (($directionDegree + self::WIND_STEP_DEG / 2) / self::WIND_STEP_DEG);
    }

    /**
     * Convert [0-360] degrees to S, SSV, SV, VSV, V, VJV, JV, JJV, J, JJZ, JZ, ZJZ, Z, ZSZ, SZ, SSZ steps
     * @param float $directionDegree
     * @return string
     */
    public static function directionDegreeToStepText(?float $directionDegree): ?string {
        return ($directionDegree === NULL) ? NULL : self::WIND_DIRECTION_TEXT[self::directionDegreeToStep($directionDegree)];
    }

    /**
     * Convert [0-16] steps to [0-360] degrees
     * @param int $directionStep
     * @return float
     */
    public static function directionStepToDegree(int $directionStep): float {
        return ($directionStep * self::WIND_STEP_DEG);
    }

    /**
     * Convert m/s to mph
     * @param float|null $speedMs
     * @return float|null
     */
    public static function speedMsToMph(?float $speedMs): ?float {
        return ($speedMs === NULL) ? NULL : ($speedMs * self::SPEED_MS_MPH);
    }

    /**
     * Convert mph to m/s
     * @param float|null $speedMph
     * @return float|null
     */
    public static function speedMphToMs(?float $speedMph): ?float {
        return ($speedMph === NULL) ? NULL : ($speedMph / self::SPEED_MS_MPH);
    }

    /**
     * Get season name ['winter', 'spring', 'summer', 'autumn'] by index 
     * @param int|null $seasonIndex
     * @return string|null
     */
    public static function seasonIndexToName(?int $seasonIndex): ?string {
        return ($seasonIndex === NULL || !isset(self::SEASON_NAMES[$seasonIndex])) ? NULL : ucfirst(self::SEASON_NAMES[$seasonIndex]);
    }

    /**
     * Get season index from  name ['winter', 'spring', 'summer', 'autumn'] 
     * @param string|null $seasonName
     * @return int|null
     */
    public static function seasonNameToIndex(?string $seasonName): ?int {
        if ($seasonName !== NULL && ($key = array_search(strtolower($seasonName), self::SEASON_NAMES))) {
            return $key;
        }
        return NULL;
    }

    /**
     * Get period unit name ['Day', 'Sol'] by index 
     * @param int|null $periodUnitIndex
     * @return string|null
     */
    public static function periodUnitIndexToName(?int $periodUnitIndex): ?string {
        return ($periodUnitIndex === NULL || !isset(self::PERIOD_NAMES[$periodUnitIndex])) ? NULL : ucfirst(self::PERIOD_NAMES[$periodUnitIndex]);
    }

    /**
     * The Beaufort scale is an empirical measure that relates wind speed to observed conditions at sea or on land.
     * @param float $windSpeed
     * @return int
     */
    public static function beaufortIndex(?float $windSpeed): ?int {
        switch ($windSpeed) {
            case NULL: return NULL;
            case $windSpeed < 0.29: return 0;
            case $windSpeed <= 1.5: return 1;
            case $windSpeed <= 3.3: return 2;
            case $windSpeed <= 5.4: return 3;
            case $windSpeed <= 7.9: return 4;
            case $windSpeed <= 10.7: return 5;
            case $windSpeed <= 13.8: return 6;
            case $windSpeed <= 17.1: return 7;
            case $windSpeed <= 20.7: return 8;
            case $windSpeed <= 24.4: return 9;
            case $windSpeed <= 28.4: return 10;
            case $windSpeed <= 32.6: return 11;
            case $windSpeed <= 36.6: return 12;
            case $windSpeed <= 42.6: return 13;
            case $windSpeed <= 49.5: return 14;
            case $windSpeed <= 58.5: return 15;
            case $windSpeed <= 69.4: return 16;
            case $windSpeed > 69.4: return 17;
        }
    }

    /**
     * The Beaufort scale is an empirical measure that relates wind speed to observed conditions at sea or on land.
     * @param float|null $windSpeed
     * @return string|null
     */
    public static function beaufortIndexText(?float $windSpeed): ?string {
        return ($windSpeed === NULL) ? NULL : self::WIND_BEAUFORT_SCALE_TEXT[self::beaufortIndex($windSpeed)];
    }

    /**
     * Windchild calculation - windspeed in m/s
     * @param float $temp
     * @param float $windSpeed
     * @return float
     */
    function getWindchill($temp, $windSpeed) {
        if (($temp < 80) && ($windSpeed > 2) && $windSpeed <= 50) {
            return round(0.045 * (5.2735 * pow(($windSpeed * 3.6), 0.5) + 10.45 - 0.2778 * $windSpeed * 3.6) * ($temp - 33) + 33, 1);
        } else {
            return round($temp, 1);
        }
    }

    /**
     * Windchild calculation - windspeed in m/s
     * @param float $temp
     * @param float $windSpeed
     * @return float
     */
    function getWindchillNew($temp, $windSpeed) {
        If (($temp < 10) And ($windSpeed > 1.4) And $windSpeed <= 50) {
            return round(13.13 + 0.6215 * $temp - 13.95 * pow($windSpeed, 0.16) + 0.486 * $temp * $windSpeed ^ 0.16, 1);
        } else {
            return round($temp, 1);
        }
    }

    /**
     * 
     * @param float $temp
     * @param float $RH
     * @return float
     */
    function getAbsHumidity($temp, $RH) {
//saturation vapor pressure
        $Es = 6.1078 * pow(10, (($temp * 7.5) / ($temp + 237.3)));
        //vapor pressure
        $E = $RH * $Es / 100;
        return round(100000 * $E / ((273.16 + $temp) * 461.5), 2);
    }

    function getDewPoint($temp, $RH) {
        return round(((pow(($RH / 100), 0.125)) * (112 + 0.9 * $temp) + (0.1 * $temp) - 112), 1);
    }

}
