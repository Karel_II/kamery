<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model;

use Nette;

/**
 * Description of Apache
 *
 * @author karel.novak
 * 
 * @property string $serverName Description
 * @property string $serverAlias Description
 * @property string $serverAdmin Description
 * @property int $port Description
 * @property int $portSSL Description
 * @property string $documentRoot Description
 * @property bool $webServer Description
 * @property bool $SSL Description
 * @property string $serverStatus Description
 * @property string $configDir Description
 * @property bool $cache Description
 * @property string $cacheDefaultExpire Description
 * @property string $cacheMaxExpire Description
 */
class Apache {

    use \Nette\SmartObject;

    private $port = 80;
    private $portSSL = 443;
    private $serverName;
    private $serverAlias;
    private $serverAdmin;
    private $webServer;
    private $documentRoot;
    private $serverStatus;
    private $SSL;
    private $configDir;
    private $cache = false;
    private $cacheDefaultExpire = NULL;
    private $cacheMaxExpire = NULL;

    public function getPort() {
        return $this->port;
    }

    public function getPortSSL() {
        return $this->portSSL;
    }

    public function getServerName() {
        return $this->serverName;
    }

    public function getServerAlias() {
        return $this->serverAlias;
    }

    public function getServerAdmin() {
        return $this->serverAdmin;
    }

    public function getWebServer() {
        return $this->webServer;
    }

    public function getDocumentRoot() {
        return $this->documentRoot;
    }

    public function getServerStatus() {
        return $this->serverStatus;
    }

    public function getSSL() {
        return $this->SSL;
    }

    public function getConfigDir() {
        return $this->configDir;
    }

    function getCache() {
        return $this->cache;
    }

    function getCacheDefaultExpire() {
        return $this->cacheDefaultExpire;
    }

    function getCacheMaxExpire() {
        return $this->cacheMaxExpire;
    }

    public function setPort($port) {
        $this->port = $port;
    }

    public function setPortSSL($portSSL) {
        $this->portSSL = $portSSL;
    }

    public function setServerName($serverName) {
        $this->serverName = $serverName;
    }

    public function setServerAlias($serverAlias) {
        $this->serverAlias = $serverAlias;
    }

    public function setServerAdmin($serverAdmin) {
        $this->serverAdmin = $serverAdmin;
    }

    public function setWebServer($webServer) {
        $this->webServer = $webServer;
    }

    public function setDocumentRoot($documentRoot) {
        $this->documentRoot = $documentRoot;
    }

    public function setServerStatus($serverStatus) {
        $this->serverStatus = $serverStatus;
    }

    public function setSSL($SSL) {
        $this->SSL = $SSL;
    }

    public function setConfigDir($configDir) {
        $this->configDir = $configDir;
    }

    function setCache($cache) {
        $this->cache = $cache;
    }

    function setCacheDefaultExpire($cacheDefaultExpire) {
        $this->cacheDefaultExpire = $cacheDefaultExpire;
    }

    function setCacheMaxExpire($cacheMaxExpire) {
        $this->cacheMaxExpire = $cacheMaxExpire;
    }

}
