<?php

namespace App\Model\Repository;

use Nette,
    NetteAddons\Http\Url;
use \NetteAddons\Repository\Repository;
use App\Model\Objects\VideoCameras,
    App\Model\Objects\VideoCamera,
    App\Model\Objects\GeographicCoordinateSystem;

/**
 * Description of KameryRepositoryLite
 *
 * @author Karel_II
 */
class VideoCamerasRepositoryLite extends Repository {

    public function insertVideoCameraBatch($name, $description, $url, $private, $snmpCommunity) {
        $urlNette = new Url($url);
        $kamera = VideoCamera::newVideoCamera(NULL, $name, $description, NULL, $urlNette, NULL, FALSE, $private, $snmpCommunity, NULL);
        $this->insertVideoCamera($kamera);
    }

    public function insertVideoCamera(VideoCamera $videoCamera) {
        $videoCameraInsert = clone $videoCamera;
        $videoCameraInsert->url->setSesurityOveride(TRUE);
        $insert = array();
        if ($videoCameraInsert->hasId()) {
            $insert['id'] = $videoCameraInsert->getId();
        }
        $insert['name'] = $videoCameraInsert->name;
        $insert['description'] = $videoCameraInsert->description;
        $insert['instalationDescription'] = $videoCameraInsert->instalationDescription;
        $insert['url'] = (string) $videoCameraInsert->url;
        $insert['urlSource'] = (string) $videoCameraInsert->urlSource;
        $insert['private'] = $videoCameraInsert->private;
        $insert['watermark'] = $videoCameraInsert->watermark;
        $insert['snmpCommunity'] = empty($videoCameraInsert->snmpCommunity) ? NULL : $videoCameraInsert->snmpCommunity;
        $insert['geographicCoordinates'] = empty($videoCameraInsert->geographicCoordinates) ? NULL : $videoCameraInsert->geographicCoordinates->dec;
        $insert['archiveDays'] = $videoCameraInsert->archiveDays;
        $insert['previewMaxWidth'] = $videoCameraInsert->previewMaxWidth;
        $insert['previewMaxHeight'] = $videoCameraInsert->previewMaxHeight;
        $insert['archiveMaxWidth'] = $videoCameraInsert->archiveMaxWidth;
        $insert['archiveMaxHeight'] = $videoCameraInsert->archiveMaxHeight;
        $insert['rrdDataDriver'] = $videoCameraInsert->rrdDataDriver;
        $insert['rrdStep'] = $videoCameraInsert->rrdStep;
        $insert['rrdGraphs'] = $videoCameraInsert->rrdGraphsText;
        $this->getTable()->insert($insert);
    }

    public function updateVideoCamera(VideoCamera $videoCamera) {
        $videoCameraUpdate = clone $videoCamera;
        $videoCameraUpdate->url->setSesurityOveride(TRUE);
        $update = array();
        $update['name'] = $videoCameraUpdate->name;
        $update['description'] = $videoCameraUpdate->description;
        $update['instalationDescription'] = $videoCameraUpdate->instalationDescription;
        $update['url'] = (string) $videoCameraUpdate->url;
        $update['urlSource'] = (string) $videoCameraUpdate->urlSource;
        $update['watermark'] = $videoCameraUpdate->watermark;
        $update['private'] = $videoCameraUpdate->private;
        $update['snmpCommunity'] = empty($videoCameraUpdate->snmpCommunity) ? NULL : $videoCameraUpdate->snmpCommunity;
        $update['geographicCoordinates'] = empty($videoCameraUpdate->geographicCoordinates) ? NULL : $videoCameraUpdate->geographicCoordinates->dec;
        $update['archiveDays'] = $videoCameraUpdate->archiveDays;
        $update['previewMaxWidth'] = $videoCameraUpdate->previewMaxWidth;
        $update['previewMaxHeight'] = $videoCameraUpdate->previewMaxHeight;
        $update['archiveMaxWidth'] = $videoCameraUpdate->archiveMaxWidth;
        $update['archiveMaxHeight'] = $videoCameraUpdate->archiveMaxHeight;
        $update['rrdDataDriver'] = $videoCameraUpdate->rrdDataDriver;
        $update['rrdStep'] = $videoCameraUpdate->rrdStep;
        $update['rrdGraphs'] = $videoCameraUpdate->rrdGraphsText;
        $this->getTable()->where('id', $videoCameraUpdate->id)->update($update);
    }

    public function getVideoCameras($isPrivate = NULL, $hasArchive = NULL) {
        $return = new VideoCameras();
        $result = $this->getTable()->select('*');
        if ($isPrivate === TRUE || $isPrivate === FALSE) {
            $result->where('private', $isPrivate);
        }
        if ($hasArchive === TRUE) {
            $result->where('archiveDays > ?', 0);
        } elseif ($hasArchive === FALSE) {
            $result->where('archiveDays ', 0);
        }
        foreach ($result as $row) {
            $return[] = VideoCamera::newVideoCamera($row->id, $row->name, $row->description, $row->instalationDescription, new Url($row->url), (empty($row->urlSource) ? NULL : new Url($row->urlSource)), $row->watermark, $row->private, $row->snmpCommunity, new GeographicCoordinateSystem($row->geographicCoordinates), $row->archiveDays, $row->previewMaxWidth, $row->previewMaxHeight, $row->archiveMaxWidth, $row->archiveMaxHeight)->
                    setRrd($row->rrdDataDriver, $row->rrdStep, $row->rrdGraphs);
        }
        return $return;
    }

    public function fetchVideoCamera(VideoCamera $videoCamera = null) {
        $result = $this->getTable()->select('*')->where('id', $videoCamera->id);
        if (($row = $result->fetch())) {
            $videoCamera->name = $row->name;
            $videoCamera->description = $row->description;
            $videoCamera->instalationDescription = $row->instalationDescription;
            $videoCamera->url = new Url($row->url);
            $videoCamera->urlSource = (empty($row->urlSource) ? NULL : new Url($row->urlSource));
            $videoCamera->private = $row->private;
            $videoCamera->watermark = $row->watermark;
            $videoCamera->snmpCommunity = $row->snmpCommunity;
            $videoCamera->geographicCoordinates = new GeographicCoordinateSystem($row->geographicCoordinates);
            $videoCamera->archiveDays = $row->archiveDays;
            $videoCamera->previewMaxWidth = $row->previewMaxWidth;
            $videoCamera->previewMaxHeight = $row->previewMaxHeight;
            $videoCamera->archiveMaxWidth = $row->archiveMaxWidth;
            $videoCamera->archiveMaxHeight = $row->archiveMaxHeight;
            $videoCamera->setRrd($row->rrdDataDriver, $row->rrdStep, $row->rrdGraphs);
            return true;
        }
        return false;
    }

}
