<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Repository;

use Nette\Utils\Paginator;
use NetteAddons\Utils\IObjectId;
use App\Model\Objects\ArchiveRecord;
use NetteAddons\Repository\Repository;

/**
 * Description of ArchiveRepositoryLite
 *
 * @author Karel
 */
class ArchiveRepositoryLite extends Repository {

    const TABLE_METEO_STATION = 'meteoStationArchive';
    const TABLE_VIDEO_CAMERA = 'videoCameraArchive';
    const CLEANUP_COUNT = 120;

    private static function selectTable(IObjectId $device) {
        if ($device instanceof \App\Model\Objects\MeteoStation) {
            return self::TABLE_METEO_STATION . '_' . $device->id;
        }
        if ($device instanceof \App\Model\Objects\VideoCamera) {
            return self::TABLE_VIDEO_CAMERA . '_' . $device->id;
        }
        throw new \Exception;
    }

    public function insertArchive(ArchiveRecord $archive) {
        if (!$archive->processed) {
            return FALSE;
        }
        $tableName = self::selectTable($archive->device);
        $insert = array();
        $insert['id'] = $archive->datetime->getTimestamp();
        $insert['datetime'] = $archive->datetime->format(DATE_W3C);
        $insert['year'] = $archive->year;
        $insert['datetimeYear'] = $archive->datetimeNewYear->format(DATE_W3C);
        $this->getTable($tableName)->insert($insert);
    }

    public function getArchive(ArchiveRecord $archiveRecordFinder, Paginator $paginator = NULL) {
        $tableName = self::selectTable($archiveRecordFinder->device);
        $return = array();
        $result = $this->getTable($tableName)->select('*');
        $result->where('datetime > DATE(\'NOW\',\'-' . $archiveRecordFinder->device->archiveDays . ' DAY\')');
        $result->where('expired', 0);
        if (isset($paginator) && !empty($paginator->pageCount)) {
            $result->limit($paginator->getLength(), $paginator->getOffset());
        }
        $result->order('datetime DESC');
        foreach ($result as $row) {
            if (isset($archiveRecordFinder->device)) {
                $return[$row->id] = ArchiveRecord::newArchiveRecord(clone $archiveRecordFinder->device, $row->datetime);
            }
        }
        return $return;
    }

    public function getArchiveExpired(ArchiveRecord $archiveRecordFinder) {
        $tableName = self::selectTable($archiveRecordFinder->device);
        $return = array();
        $result = $this->getTable($tableName)->select('*');
        $result->where('expired > ?', 0);
        foreach ($result as $row) {
            if (isset($archiveRecordFinder->device)) {
                $return[$row->id] = ArchiveRecord::newArchiveRecord(clone $archiveRecordFinder->device, $row->datetime);
            }
        }
        return $return;
    }

    public function getArchiveYears(IObjectId $device) {
        $tableName = self::selectTable($device);
        $return = array();
        $result = $this->getTable($tableName)->select('DISTINCT year,datetimeYear')->order('year DESC');
        foreach ($result as $row) {
            $return[$row->year] = ArchiveRecord::newArchiveRecord($device, $row->datetimeYear);
        }
        return $return;
    }

    public function countArchive(ArchiveRecord $archiveRecordFinder) {
        $tableName = self::selectTable($archiveRecordFinder->device);
        $result = $this->getTable($tableName);
        $result->where('datetime > DATE(\'NOW\',\'-' . $archiveRecordFinder->device->archiveDays . ' DAY\')');
        $result->where('expired', 0);
        $return = $result->count('id');
        if ($return) {
            return $return;
        }
        return FALSE;
    }

    public function expireArchives(ArchiveRecord $archiveRecordFinder) {
        if (!$archiveRecordFinder->processed) {
            return FALSE;
        }
        $tableName = self::selectTable($archiveRecordFinder->device);
        $result = $this->getTable($tableName);
        if (isset($archiveRecordFinder->device)) {
            $result->where('datetime < DATE(\'NOW\',\'-' . $archiveRecordFinder->device->archiveDays . ' DAY\')');
        }
        $result->update(['expired+=' => 1]);
        $result->where('expired > ?', self::CLEANUP_COUNT);
        $result->delete();
    }

}
