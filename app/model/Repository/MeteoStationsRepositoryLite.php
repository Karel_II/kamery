<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Repository;

use NetteAddons\Http\Url;
use \NetteAddons\Repository\Repository;
use App\Model\Objects\MeteoStation,
    App\Model\Objects\MeteoStations,
    App\Model\Objects\GeographicCoordinateSystem;

/**
 * Description of MeteoStationsRepositoryLite
 *
 * @author Karel
 */
class MeteoStationsRepositoryLite extends Repository {

    public function insertMeteoStationBatch($name, $description, $url, $private, $snmpCommunity) {
        $urlNette = new Url($url);
        $metoStation = MeteoStation::newMeteoStation(NULL, $name, $description, NULL, $urlNette, NULL, FALSE, $private, $snmpCommunity, NULL);
        $this->insertMeteoStation($metoStation);
    }

    public function insertMeteoStation(MeteoStation $metoStation) {
        $metoStationInsert = clone $metoStation;
        $metoStationInsert->url->setSesurityOveride(TRUE);
        $insert = array();
        if ($metoStationInsert->hasId()) {
            $insert['id'] = $metoStationInsert->getId();
        }
        $insert['name'] = $metoStationInsert->name;
        $insert['description'] = $metoStationInsert->description;
        $insert['instalationDescription'] = $metoStationInsert->instalationDescription;
        $insert['url'] = $metoStationInsert->url;
        $insert['urlSource'] = $metoStationInsert->urlSource;
        $insert['private'] = $metoStationInsert->private;
        $insert['watermark'] = $metoStationInsert->watermark;
        $insert['snmpCommunity'] = empty($metoStationInsert->snmpCommunity) ? NULL : $metoStationInsert->snmpCommunity;
        $insert['geographicCoordinates'] = empty($metoStationInsert->geographicCoordinates) ? NULL : $metoStationInsert->geographicCoordinates->dec;
        $insert['archiveDays'] = $metoStationInsert->archiveDays;
        $insert['rrdDataDriver'] = $metoStationInsert->rrdDataDriver;
        $insert['rrdStep'] = $metoStationInsert->rrdStep;
        $insert['rrdGraphs'] = $metoStationInsert->rrdGraphsText;


        $this->getTable()->insert($insert);
    }

    public function updateMeteoStation(MeteoStation $metoStation) {
        $metoStationUpdate = clone $metoStation;
        $metoStationUpdate->url->setSesurityOveride(TRUE);
        $update = array();
        $update['name'] = $metoStationUpdate->name;
        $update['description'] = $metoStationUpdate->description;
        $update['instalationDescription'] = $metoStationUpdate->instalationDescription;
        $update['url'] = $metoStationUpdate->url;
        $update['urlSource'] = $metoStationUpdate->urlSource;
        $update['private'] = $metoStationUpdate->private;
        $update['watermark'] = $metoStationUpdate->watermark;
        $update['snmpCommunity'] = empty($metoStationUpdate->snmpCommunity) ? NULL : $metoStationUpdate->snmpCommunity;
        $update['geographicCoordinates'] = empty($metoStationUpdate->geographicCoordinates) ? NULL : $metoStationUpdate->geographicCoordinates->dec;
        $update['archiveDays'] = $metoStationUpdate->archiveDays;
        $update['rrdDataDriver'] = $metoStationUpdate->rrdDataDriver;
        $update['rrdStep'] = $metoStationUpdate->rrdStep;
        $update['rrdGraphs'] = $metoStationUpdate->rrdGraphsText;
        $this->getTable()->where('id', $metoStationUpdate->id)->update($update);
    }

    public function getMeteoStations($isPrivate = NULL, $hasArchive = NULL) {
        $return = new MeteoStations();
        $result = $this->getTable()->select('*');
        if ($isPrivate === TRUE || $isPrivate === FALSE) {
            $result->where('private', $isPrivate);
        }
        if ($hasArchive === TRUE) {
            $result->where('archiveDays > ?', 0);
        } elseif ($hasArchive === FALSE) {
            $result->where('archiveDays ', 0);
        }
        foreach ($result as $row) {
            $return[] = MeteoStation::newMeteoStation($row->id, $row->name, $row->description, $row->instalationDescription, new Url($row->url), (empty($row->urlSource) ? NULL : new Url($row->urlSource)), $row->watermark, $row->private, $row->snmpCommunity, new GeographicCoordinateSystem($row->geographicCoordinates), $row->archiveDays)->
                    setRrd($row->rrdDataDriver, $row->rrdStep, $row->rrdGraphs);
        }
        return $return;
    }

    public function fetchMeteoStation(MeteoStation $metoStation = null) {
        $result = $this->getTable()->select('*')->where('id', $metoStation->id);
        if (($row = $result->fetch())) {
            $metoStation->name = $row->name;
            $metoStation->description = $row->description;
            $metoStation->instalationDescription = $row->instalationDescription;
            $metoStation->url = new Url($row->url);
            $metoStation->urlSource = empty($row->urlSource) ? NULL : new Url($row->urlSource);
            $metoStation->private = $row->private;
            $metoStation->watermark = $row->watermark;
            $metoStation->snmpCommunity = $row->snmpCommunity;
            $metoStation->geographicCoordinates = new GeographicCoordinateSystem($row->geographicCoordinates);
            $metoStation->archiveDays = $row->archiveDays;
            $metoStation->setRrd($row->rrdDataDriver, $row->rrdStep, $row->rrdGraphs);
            return true;
        }
        return false;
    }

}
