<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Repository\Create;

use \NetteAddons\Repository;

/**
 * Description of VideoCamerasRepositoryLite
 *
 * @author Karel
 */
class VideoCamerasRepositoryLite extends Repository\Repository implements Repository\IRepositoryCreate {

    use Repository\TRepositoryCreate;

    public function createTable($tableName = NULL) {
        $tableName = ((empty($tableName)) ? $this->getTableName() : $tableName);
        $this->context->beginTransaction();

        $this->context->query("CREATE TABLE IF NOT EXISTS `" . $tableName . "` ( " .
                "`id`                     INTEGER " . $this->getUnsigned() . "NOT NULL " . $this->getColumnPrimaryKey(true) . ", " .
                '`name`                   CHAR(100) UNIQUE,' .
                '`description`            CHAR(100),' .
                '`instalationDescription` TEXT,' .
                '`url`                    CHAR(100),' .
                '`urlSource`              CHAR(100) DEFAULT NULL,' .
                '`snmpCommunity`          CHAR(30),' .
                '`geographicCoordinates`  CHAR(30),' .
                '`archiveDays`	          INTEGER NOT NULL DEFAULT 365, ' .
                '`previewMaxWidth`	  INTEGER NOT NULL DEFAULT 1024, ' .
                '`previewMaxHeight`	  INTEGER NOT NULL DEFAULT 1024, ' .
                '`archiveMaxWidth`	  INTEGER NOT NULL DEFAULT 1024, ' .
                '`archiveMaxHeight`	  INTEGER NOT NULL DEFAULT 1024, ' .
                '`watermark`              TINYINT,' .
                '`rrdDataDriver`          CHAR(30),' .
                '`rrdStep`                CHAR(10),' .
                '`rrdGraphs`              CHAR(255),' .
                '`private`                TINYINT' .
                (($result = $this->addKey('PRIMARY_AUTO_INCREMENT', 'id', ['id'])) . ($result ? '' : '')) .
                ");"
        );
        $this->context->commit();
    }

}
