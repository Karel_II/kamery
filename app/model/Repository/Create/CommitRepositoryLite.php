<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Repository\Create;

use \NetteAddons\Repository;

/**
 * Description of VisitorsRepository
 *
 * @author Karel
 */
class CommitRepositoryLite extends Repository\Repository implements Repository\IRepositoryCreate {

    use Repository\TRepositoryCreate;

    public function createTable($tableName = NULL) {
        $tableName = ((empty($tableName)) ? $this->getTableName() : $tableName);
        $this->context->beginTransaction();

        $this->context->query("CREATE TABLE IF NOT EXISTS `" . $tableName . "` ( " .
                "`id`                INTEGER " . $this->getUnsigned() . "NOT NULL " . $this->getColumnPrimaryKey(true) . ", " .
                "`datetimeInserted`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP, " .
                "`datetimeFinished`  TIMESTAMP DEFAULT NULL, " .
                "`committed`         TINYINT NOT NULL DEFAULT 0, " .
                "`finished`          TINYINT NOT NULL DEFAULT 0 " .
                (($result = $this->addKey('PRIMARY_AUTO_INCREMENT', 'id', ['id'])) . ($result ? '' : '')) .
                ");"
        );
        $this->addTriggers($tableName, 'meteoStations');
        $this->addTriggers($tableName, 'videoCameras');
        $this->context->commit();
    }

    private function addTriggers($tableName = NULL, $tableNameTarget = NULL) {
        $name = ((empty($tableName)) ? $this->getTableName() : $tableName);
        $this->context->query('CREATE TRIGGER IF NOT EXISTS ' . $tableNameTarget . 'Delete AFTER DELETE ON `' . $tableNameTarget . '` BEGIN INSERT INTO `' . $name . '`(`id`,`committed`,`finished`) VALUES (NULL,0,0); END');
        $this->context->query('CREATE TRIGGER IF NOT EXISTS ' . $tableNameTarget . 'Insert AFTER INSERT ON `' . $tableNameTarget . '` BEGIN INSERT INTO `' . $name . '`(`id`,`committed`,`finished`) VALUES (NULL,0,0); END');
        $this->context->query('CREATE TRIGGER IF NOT EXISTS ' . $tableNameTarget . 'Update AFTER UPDATE ON `' . $tableNameTarget . '` BEGIN INSERT INTO `' . $name . '`(`id`,`committed`,`finished`) VALUES (NULL,0,0); END ');
    }

}
