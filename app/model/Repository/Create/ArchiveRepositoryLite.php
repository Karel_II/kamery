<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Repository\Create;

use \NetteAddons\Repository;

/**
 * Description of ArchiveRepositoryLite
 *
 * @author Karel
 */
class ArchiveRepositoryLite extends Repository\Repository implements Repository\IRepositoryCreate {

    use Repository\TRepositoryCreate;

    public function createTable($tableName = NULL) {

        $tableName = ((empty($tableName)) ? $this->getTableName() : $tableName);
        $this->context->beginTransaction();
        $this->context->query("CREATE TABLE IF NOT EXISTS `" . $tableName . "` ( " .
                "`id`            INTEGER " . $this->getUnsigned() . "NOT NULL " . $this->getColumnPrimaryKey(true) . ", " .
                "`datetime`      DATETIME NOT NULL, " .
                "`expired`       TINYINT NOT NULL DEFAULT 0, " .
                "`year`          INTEGER, " .
                "`datetimeYear`  DATETIME" .
                (($result = $this->addKey('PRIMARY_AUTO_INCREMENT', 'id', ['id'])) . ($result ? '' : '')) .
                ");"
        );
        $this->context->commit();
    }

}
