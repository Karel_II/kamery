<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Repository;

use NetteBootstapMenu\Menu\Menu,
    NetteBootstapMenu\Menu\MenuRoot,
    NetteBootstapMenu\Menu\MenuLink,
    NetteBootstapMenu\Menu\MenuUrl,
    NetteBootstapMenu\Menu\MenuSeparator,
    NetteBootstapMenu\Menu\MenuSearch,
    NetteBootstapMenu\Menu\MenuCounter,
    NetteBootstapMenu\Menu\MenuCheckBox,
    NetteBootstapMenu\Menu\MenuDropdownMessage;
use NetteBootstapMenu\Menu\Interfaces\IRobots,
    \NetteBootstapMenu\Menu\Interfaces\IMenuSiteMap;
use NetteAddons\Repository\UsersRepository,
    NetteAddons\Repository\RolesRepository;
use NetteAddons\Http\Url;
use \Nette\Utils\DateTime;
use App\Model\Objects\MeteoStations,
    App\Model\Objects\VideoCameras;

/**
 * Description of MenuRepository
 *
 * @author Karel_II
 */
class MenuRepository {

    public function getMenu() {
        $menu = new Menu();
        $menu[] = MenuRoot::newMenuRoot(0, NULL, 'Spolek Křivonet - Kamery a meteostanice', 'MENU', NULL, NULL)
                ->setMetaRobots(array(IRobots::META_ROBOTS_ALL));
        $menu[] = MenuLink::newMenuLink(1, 0, 'Spolek Křivonet - Kamery a meteostanice', NULL, NULL, ':Homepage:default')
                ->setSiteMap(1.0, IMenuSiteMap::SITEMAP_WEEKLY, (new DateTime())->format(DateTime::W3C));
        $menu[] = MenuLink::newMenuLink(2, 0, 'Administration', NULL, NULL, ':Administration:default')
                ->setMetaRobots(array(IRobots::META_ROBOTS_NOINDEX));
        $menu[] = MenuLink::newMenuLink(3, 0, 'Sign', NULL, NULL, ':Sign:default')
                ->setMetaRobots(array(IRobots::META_ROBOTS_NOINDEX));

        //Web Kamery
        $menu[] = MenuLink::newMenuLink(5, 0, 'Web Kamery', NULL, NULL, ':Show:videocamera');
        $menu[] = MenuLink::newMenuLink(6, 0, 'Web Kamery Full', NULL, NULL, ':Show:videocameraFull');
        $menu[] = MenuLink::newMenuLink(7, 0, 'Web Kamery Archive', NULL, NULL, ':Archive:videocamera')
                ->setMetaRobots(array(IRobots::META_ROBOTS_NOFOLOW));
        $menu[] = MenuLink::newMenuLink(8, 0, 'Web Kamery MRTG', NULL, NULL, ':Mrtg:meteostationFull');

        //Meteo Stanice
        $menu[] = MenuLink::newMenuLink(10, 0, 'Meteo Stanice', NULL, NULL, ':Show:meteostation');
        $menu[] = MenuLink::newMenuLink(11, 0, 'Meteo Stanice Archive', NULL, NULL, ':Archive:meteostation')
                ->setMetaRobots(array(IRobots::META_ROBOTS_NOFOLOW));
        $menu[] = MenuLink::newMenuLink(12, 0, 'Meteo Stanice MRTG', NULL, NULL, ':Mrtg:meteostation');
        return $menu;
    }

    public function getHeaderMenuBrand() {
        $menu = new Menu();
        $menu[] = MenuRoot::newMenuRoot(0, NULL, NULL, 'BRAND', NULL, NULL);
        $menu[] = MenuLink::newMenuLink(1, 0, 'Spolek Křivonet - Kamery a meteostanice', NULL, NULL, 'Homepage:default');
        return $menu;
    }

    public function getHorizontalSearchMenu() {
        $menu = new Menu();
        $menu[] = MenuRoot::newMenuRoot(0, NULL, NULL, 'SEARCH', NULL, NULL);
//        $menu[] = MenuSearch::newMenuSearch(1, 0, 'Search...', NULL, 'fa fa-search', 'Homepage:search');
        return $menu;
    }

    public function getHorizontalLeftMenu() {
        $menu = new Menu();
        $menu[] = MenuRoot::newMenuRoot(0, NULL, NULL, 'HORIZONTALLEFT', NULL, NULL);
        return $menu;
    }

    public function getHorizontalRightMenu() {
        $menu = new Menu();
        $menu[] = MenuRoot::newMenuRoot(0, NULL, NULL, 'HORIZONTALRIGHT', NULL);
        $menu[] = MenuLink::newMenuLink(5, 0, 'Settings', NULL, 'fa fa-fw fa-cog', NULL)
                ->isAllowed('settings')
                ->setARel(array(IRobots::A_REL_NOFOLLOW));
        $menu[] = MenuLink::newMenuLink(51, 5, 'System Info', NULL, 'fas fa-info', 'Administration:info')
                ->isAllowed('administration')
                ->setARel(array(IRobots::A_REL_NOFOLLOW));
        $menu[] = MenuLink::newMenuLink(52, 5, 'Video Cameras', NULL, 'fa fa-fw fa-video', 'VideoCameras:default')
                ->isAllowed('videoCameras', 'edit')
                ->setARel(array(IRobots::A_REL_NOFOLLOW));
        $menu[] = MenuLink::newMenuLink(53, 5, 'Meteo Stations', NULL, 'fa fa-fw fa-cloud', 'MeteoStations:default')
                ->isAllowed('meteoStations', 'edit')
                ->setARel(array(IRobots::A_REL_NOFOLLOW));
        $menu[] = MenuSeparator::newMenuSeparator(54, 5, NULL, NULL, NULL, FALSE);
        $menu[] = MenuLink::newMenuLink(55, 5, 'Waiting for *', NULL, NULL, NULL, NULL, FALSE);
        $menu[] = MenuLink::newMenuLink(56, 5, 'Commit', NULL, 'fa fa-fw fa-share-square', 'commit!', NULL, FALSE)
                ->setARel(array(IRobots::A_REL_NOFOLLOW));
        $menu[] = MenuLink::newMenuLink(6, 0, NULL, NULL, 'fa fa-fw fa-user', NULL);
        $menu[] = MenuLink::newMenuLink(61, 6, 'Users List', NULL, 'fa fa-fw fa-user', 'Administration:UsersList')
                ->isAllowed(UsersRepository::RESOURCE_MANAGEMENT_USER, UsersRepository::RESOURCE_MANAGEMENT_USER_PRIVILEDGE_OTHER)
                ->setARel(array(IRobots::A_REL_NOFOLLOW));
        $menu[] = MenuLink::newMenuLink(62, 6, 'User Profile', NULL, 'fa fa-fw fa-user', 'Administration:UserProfile')
                ->isAllowed(UsersRepository::RESOURCE_MANAGEMENT_USER, UsersRepository::RESOURCE_MANAGEMENT_USER_PRIVILEDGE_SELF)
                ->setARel(array(IRobots::A_REL_NOFOLLOW));
        $menu[] = MenuSeparator::newMenuSeparator(63, 6, NULL, NULL, NULL)
                ->isLoggedIn(TRUE);
        $menu[] = MenuLink::newMenuLink(64, 6, 'Login', NULL, 'fa fa-fw fa-sign-in', "Sign:in")
                ->isLoggedIn(FALSE)
                ->setARel(array(IRobots::A_REL_NOFOLLOW));
        $menu[] = MenuLink::newMenuLink(65, 6, 'Logout', NULL, 'fa fa-fw fa-sign-out', "Sign:out")
                ->isLoggedIn(TRUE)
                ->setARel(array(IRobots::A_REL_NOFOLLOW));
        return $menu;
    }

    public function getFooterMenu() {
        $menu = new Menu();
        $menu[] = MenuRoot::newMenuRoot(0, NULL, NULL, 'FOOTER', NULL);
        $menu[] = MenuLink::newMenuLink(1, 0, '2019', NULL, 'far fa-copyright', NULL);
        $menu[] = MenuUrl::newMenuUrl(10, 1, 'Spolek Krivonet', NULL, NULL, new Url('https://krivonet.cz/'))
                ->setARel(array(IRobots::A_REL_NOFOLLOW));
        $menu[] = MenuLink::newMenuLink(2, 0, 'Web created by', NULL, NULL, NULL);
        $menu[] = MenuUrl::newMenuUrl(20, 2, 'Karel_II', NULL, NULL, new Url('https://bitbucket.org/Karel_II/kamery'))
                ->setARel(array(IRobots::A_REL_AUTHOR, IRobots::A_REL_NOFOLLOW));
        $menu[] = MenuLink::newMenuLink(3, 0, 'Powered by', NULL, NULL, NULL);
        $menu[] = MenuUrl::newMenuUrl(30, 3, 'Nette', NULL, NULL, new Url('https://nette.org/'))
                ->setARel(array(IRobots::A_REL_NOREFERRER, IRobots::A_REL_NOFOLLOW));
        $menu[] = MenuUrl::newMenuUrl(31, 3, 'Bootstrap', NULL, NULL, new Url('https://getbootstrap.com/'))
                ->setARel(array(IRobots::A_REL_NOREFERRER, IRobots::A_REL_NOFOLLOW));
        $menu[] = MenuUrl::newMenuUrl(32, 3, 'MRTG', NULL, NULL, new Url('https://oss.oetiker.ch/mrtg/'))
                ->setARel(array(IRobots::A_REL_NOREFERRER, IRobots::A_REL_NOFOLLOW));
        return $menu;
    }

    public static function addVideoCamerasMenu(Menu $menu, VideoCameras $videoCameras) {
        foreach ($videoCameras as $videoCamera) {
            $menu[] = MenuLink::newMenuLink(500 + $videoCamera->id, 5, $videoCamera->description, NULL, NULL, ':Show:videocamera', array($videoCamera->id))
                    ->setSiteMap(0.9, IMenuSiteMap::SITEMAP_WEEKLY, NULL);
            $menu[] = MenuLink::newMenuLink(600 + $videoCamera->id, 6, $videoCamera->description, NULL, NULL, ':Show:videocameraFull', array($videoCamera->id))
                    ->setSiteMap(0.8, IMenuSiteMap::SITEMAP_WEEKLY, NULL);
            if ($videoCamera->hasArchive) {
                $menu[] = MenuLink::newMenuLink(700 + $videoCamera->id, 7, $videoCamera->description, NULL, NULL, ':Archive:videocamera', array($videoCamera->id))
                        ->setSiteMap(0.3, IMenuSiteMap::SITEMAP_WEEKLY, NULL);
            }
            if (!empty($videoCamera->snmpCommunity)) {
                $menu[] = MenuLink::newMenuLink(800 + $videoCamera->id, 8, $videoCamera->description, NULL, NULL, ':Mrtg:videocamera', array($videoCamera->id))
                        ->setSiteMap(0.75, IMenuSiteMap::SITEMAP_WEEKLY, NULL);
            }
        }
    }

    public static function addMeteoStationsMenu(Menu $menu, MeteoStations $meteoStations) {
        foreach ($meteoStations as $meteoStation) {
            $menu[] = MenuLink::newMenuLink(1000 + $meteoStation->id, 10, $meteoStation->description, NULL, NULL, ':Show:meteostation', array($meteoStation->id))
                    ->setSiteMap(0.9, IMenuSiteMap::SITEMAP_WEEKLY, NULL);
            if ($meteoStation->hasArchive) {
                $menu[] = MenuLink::newMenuLink(1100 + $meteoStation->id, 11, $meteoStation->description, NULL, NULL, ':Archive:meteostation', array($meteoStation->id))
                        ->setSiteMap(0.3, IMenuSiteMap::SITEMAP_WEEKLY, NULL);
            }
            if (!empty($meteoStation->snmpCommunity)) {
                $menu[] = MenuLink::newMenuLink(1200 + $meteoStation->id, 12, $meteoStation->description, NULL, NULL, ':Mrtg:meteostation', array($meteoStation->id))
                        ->setSiteMap(0.75, IMenuSiteMap::SITEMAP_WEEKLY, NULL);
            }
        }
    }

}
