<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Repository;

use Nette\Database\Connection,
    Nette\Database\SqlLiteral;
use \NetteAddons\Repository\Repository;

/**
 * Description of CommitRepository
 *
 * @author karel.novak
 */
class CommitRepositoryLite extends Repository {

    public function finishCommit() {
        $this->getTable()
                ->where('datetimeFinished < ', new SqlLiteral("datetime('now')"))
                ->where('finished', 1)
                ->where('committed', 1)
                ->delete();
        $this->getTable()
                ->where('datetimeInserted < ', new SqlLiteral("datetime('now')"))
                ->where('finished', 0)
                ->where('committed', 1)
                ->update(array(
                    'finished' => 1,
                    'datetimeFinished' => new SqlLiteral("datetime('now')")
        ));
    }

    public function setCommit() {
        $this->getTable()
                ->where('datetimeInserted < ', new SqlLiteral("datetime('now')"))
                ->where('finished', 0)
                ->where('committed', 0)
                ->update(array(
                    'committed' => 1
        ));
    }

    public function getInserted() {
        $result = $this->getTable()
                ->where('finished', 0)
                ->where('committed', 0);
        $return = $result->count('id');
        if ($return) {
            return $return;
        }
        return FALSE;
    }

    public function getCommitted() {
        $result = $this->getTable()
                ->where('finished', 0)
                ->where('committed', 1);
        $return = $result->count('id');
        if ($return) {
            return $return;
        }
        return FALSE;
    }

}
