<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Repository;

use \NetteAddons\Repository\Repository;
use App\Model\Objects\Device;
use \App\Model\Rrd\Options\OptionsGraph,
    \App\Model\Rrd\Options\OptionsFetch,
    \App\Model\Rrd\Options\OptionsCreate;

/**
 * Description of RrdSetupRepositoryLite
 *
 * @author Karel
 */
class RrdSetupRepositoryLite extends Repository {

    const
            TABLE_RRDGRAPH_NAME = 'rrdGraphs',
            COLUMN_RRDGRAPH_ID = 'id',
            COLUMN_RRDGRAPH_NAME = 'name';
    const
            TABLE_RRDCOLLECTINTERVALS_NAME = 'rrdCollectIntervals',
            COLUMN_RRDCOLLECTINTERVALS_ID = 'id',
            COLUMN_RRDCOLLECTINTERVALS_NAME = 'name',
            COLUMN_RRDCOLLECTINTERVALS_START = 'start',
            COLUMN_RRDCOLLECTINTERVALS_STEP = 'step',
            COLUMN_RRDCOLLECTINTERVALS_HEARTHBEAT = 'heartBeat';
    const
            TABLE_RRDARCHIVEINTERVALS_NAME = 'rrdArchiveIntervals',
            COLUMN_RRDARCHIVEINTERVALS_ID = 'id',
            COLUMN_RRDARCHIVEINTERVALS_NAME = 'name',
            COLUMN_RRDARCHIVEINTERVALS_CONSOLIDATIONFUNCTION = 'consolidationFunction',
            COLUMN_RRDARCHIVEINTERVALS_XFF = 'xff',
            COLUMN_RRDARCHIVEINTERVALS_STEP = 'step',
            COLUMN_RRDARCHIVEINTERVALS_PERIOD = 'period';
    const
            TABLE_RRDCOLLECTARCHIVE_NAME = 'rrdCollectArchive',
            COLUMN_RRDCOLLECTARCHIVE_RRDARCHIVE = 'rrdArchive',
            COLUMN_RRDCOLLECTARCHIVE_RRDCOLECT = 'rrdCollect';

    /**
     * 
     * @return array
     */
    public function getRrdGraphs() {
        $return = array();
        $result = $this->getTable(self::TABLE_RRDGRAPH_NAME)->select('*');
        foreach ($result as $row) {
            $return[$row[self::COLUMN_RRDGRAPH_ID]] = $row[self::COLUMN_RRDGRAPH_NAME];
        }
        return $return;
    }

    /**
     * 
     * @param Device $device
     * @return array
     */
    public function getRrdArchiveIntervalNames(Device $device = NULL) {
        $return = array();
        $result = $this->getTable(self::TABLE_RRDARCHIVEINTERVALS_NAME)->
                select(self::TABLE_RRDARCHIVEINTERVALS_NAME . "." . self::COLUMN_RRDARCHIVEINTERVALS_ID)->
                select(self::TABLE_RRDARCHIVEINTERVALS_NAME . "." . self::COLUMN_RRDARCHIVEINTERVALS_NAME);
        if ($device !== NULL) {
            $result->where(":" . self::TABLE_RRDCOLLECTARCHIVE_NAME . "." . self::COLUMN_RRDCOLLECTARCHIVE_RRDCOLECT . "." . self::COLUMN_RRDCOLLECTINTERVALS_ID, $device->rrdStep);
        }
        foreach ($result as $row) {
            $return[$row[self::COLUMN_RRDARCHIVEINTERVALS_ID]] = $row[self::COLUMN_RRDARCHIVEINTERVALS_NAME];
        }
        return $return;
    }

    /**
     * 
     * @return array
     */
    public function getRrdCollectIntervalNames() {
        $return = array();
        $result = $this->getTable(self::TABLE_RRDCOLLECTINTERVALS_NAME)->select('*');
        foreach ($result as $row) {
            $return[$row[self::COLUMN_RRDCOLLECTINTERVALS_ID]] = $row[self::COLUMN_RRDCOLLECTINTERVALS_NAME];
        }
        return $return;
    }

    /**
     * 
     * @param OptionsGraph $options
     * @param Device $device
     * @param string $archiveInterval
     * @return boolean
     */
    public function fetchRrdGraphIntervalOption(OptionsGraph $options, Device $device, $archiveInterval = NULL) {
        $result = $this->getTable(self::TABLE_RRDARCHIVEINTERVALS_NAME)->
                select(self::TABLE_RRDARCHIVEINTERVALS_NAME . "." . self::COLUMN_RRDARCHIVEINTERVALS_ID)->
                select(self::TABLE_RRDARCHIVEINTERVALS_NAME . "." . self::COLUMN_RRDARCHIVEINTERVALS_NAME)->
                select(self::TABLE_RRDARCHIVEINTERVALS_NAME . "." . self::COLUMN_RRDARCHIVEINTERVALS_STEP)->
                select(self::TABLE_RRDARCHIVEINTERVALS_NAME . "." . self::COLUMN_RRDARCHIVEINTERVALS_PERIOD);
        $result->where(":" . self::TABLE_RRDCOLLECTARCHIVE_NAME . "." . self::COLUMN_RRDCOLLECTARCHIVE_RRDCOLECT . "." . self::COLUMN_RRDCOLLECTINTERVALS_ID, $device->rrdStep);
        if ($archiveInterval !== NULL) {
            $result->where(self::TABLE_RRDARCHIVEINTERVALS_NAME . "." . self::COLUMN_RRDCOLLECTINTERVALS_ID, $archiveInterval);
        }
        if (($row = $result->fetch())) {
            $options->setTimeRange("N -" . $row->period, "N", $row->step);
            return TRUE;
        }
        return FALSE;
    }

    /**
     * 
     * @param OptionsFetch $options
     * @param Device $device
     * @param string $archiveInterval
     * @return boolean
     */
    public function fetchRrdArchiveIntervalOption(OptionsFetch $options, Device $device, $archiveInterval = NULL) {
        $result = $this->getTable(self::TABLE_RRDARCHIVEINTERVALS_NAME)->
                select(self::TABLE_RRDARCHIVEINTERVALS_NAME . "." . self::COLUMN_RRDARCHIVEINTERVALS_ID)->
                select(self::TABLE_RRDARCHIVEINTERVALS_NAME . "." . self::COLUMN_RRDARCHIVEINTERVALS_NAME)->
                select(self::TABLE_RRDARCHIVEINTERVALS_NAME . "." . self::COLUMN_RRDARCHIVEINTERVALS_CONSOLIDATIONFUNCTION)->
                select(self::TABLE_RRDARCHIVEINTERVALS_NAME . "." . self::COLUMN_RRDARCHIVEINTERVALS_STEP)->
                select(self::TABLE_RRDARCHIVEINTERVALS_NAME . "." . self::COLUMN_RRDARCHIVEINTERVALS_PERIOD);
        $result->where(":" . self::TABLE_RRDCOLLECTARCHIVE_NAME . "." . self::COLUMN_RRDCOLLECTARCHIVE_RRDCOLECT . "." . self::COLUMN_RRDCOLLECTINTERVALS_ID, $device->rrdStep);
        if ($archiveInterval !== NULL) {
            $result->where(self::TABLE_RRDARCHIVEINTERVALS_NAME . "." . self::COLUMN_RRDCOLLECTINTERVALS_ID, $archiveInterval);
        }
        if (($row = $result->fetch())) {
            $options->
                    setStart("N -" . $row[self::COLUMN_RRDARCHIVEINTERVALS_PERIOD])->
                    setEnd("N")->
                    setResolution($row[self::COLUMN_RRDARCHIVEINTERVALS_STEP])->
                    setConsolidationFunction($row[self::COLUMN_RRDARCHIVEINTERVALS_CONSOLIDATIONFUNCTION]);
            return TRUE;
        }
        return FALSE;
    }

    /**
     * 
     * @param OptionsCreate $options
     * @param Device $device
     * @return boolean
     */
    public function fetchRrdCreateIntervalOption(OptionsCreate $options, Device $device) {
        $result = $this->getTable(self::TABLE_RRDCOLLECTINTERVALS_NAME)->
                select(self::TABLE_RRDCOLLECTINTERVALS_NAME . "." . self::COLUMN_RRDCOLLECTINTERVALS_ID)->
                select(self::TABLE_RRDCOLLECTINTERVALS_NAME . "." . self::COLUMN_RRDCOLLECTINTERVALS_START)->
                select(self::TABLE_RRDCOLLECTINTERVALS_NAME . "." . self::COLUMN_RRDCOLLECTINTERVALS_STEP);
        $result->where(self::TABLE_RRDCOLLECTINTERVALS_NAME . "." . self::COLUMN_RRDCOLLECTINTERVALS_ID, $device->rrdStep);
        if (($row = $result->fetch())) {
            $options->
                    setStep($row[self::COLUMN_RRDCOLLECTINTERVALS_STEP])->
                    setStart($row[self::COLUMN_RRDCOLLECTINTERVALS_START]);
            return TRUE;
        }
        return FALSE;
    }

    /**
     * 
     * @param Device $device
     * @return boolean|string
     */
    public function getRrdCreateHearthBeat(Device $device) {
        $result = $this->getTable(self::TABLE_RRDCOLLECTINTERVALS_NAME)->
                select(self::TABLE_RRDCOLLECTINTERVALS_NAME . "." . self::COLUMN_RRDCOLLECTINTERVALS_ID)->
                select(self::TABLE_RRDCOLLECTINTERVALS_NAME . "." . self::COLUMN_RRDCOLLECTINTERVALS_HEARTHBEAT);
        $result->where(self::TABLE_RRDCOLLECTINTERVALS_NAME . "." . self::COLUMN_RRDCOLLECTINTERVALS_ID, $device->rrdStep);
        if (($row = $result->fetch())) {
            return $row[self::COLUMN_RRDCOLLECTINTERVALS_HEARTHBEAT];
        }
        return FALSE;
    }

    /**
     * 
     * @param Device $device
     * @return array
     */
    public function fetchRrdCreateRra(OptionsCreate $options, Device $device) {
        $return = array();
        $result = $this->getTable(self::TABLE_RRDARCHIVEINTERVALS_NAME)->
                select(self::TABLE_RRDARCHIVEINTERVALS_NAME . "." . self::COLUMN_RRDARCHIVEINTERVALS_ID)->
                select(self::TABLE_RRDARCHIVEINTERVALS_NAME . "." . self::COLUMN_RRDARCHIVEINTERVALS_CONSOLIDATIONFUNCTION)->
                select(self::TABLE_RRDARCHIVEINTERVALS_NAME . "." . self::COLUMN_RRDARCHIVEINTERVALS_XFF)->
                select(self::TABLE_RRDARCHIVEINTERVALS_NAME . "." . self::COLUMN_RRDARCHIVEINTERVALS_STEP)->
                select(self::TABLE_RRDARCHIVEINTERVALS_NAME . "." . self::COLUMN_RRDARCHIVEINTERVALS_PERIOD);
        $result->where(":" . self::TABLE_RRDCOLLECTARCHIVE_NAME . "." . self::COLUMN_RRDCOLLECTARCHIVE_RRDCOLECT . "." . self::COLUMN_RRDCOLLECTINTERVALS_ID, $device->rrdStep);
        foreach ($result as $row) {
            $options->rraCf($row[self::COLUMN_RRDARCHIVEINTERVALS_CONSOLIDATIONFUNCTION], $row[self::COLUMN_RRDARCHIVEINTERVALS_XFF], $row[self::COLUMN_RRDARCHIVEINTERVALS_STEP], $row[self::COLUMN_RRDARCHIVEINTERVALS_PERIOD]);
        }
        return $return;
    }

}
