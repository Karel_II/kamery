<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model;

/**
 * Description of LetsEncrypt
 *
 * @author Karel
 * @property string $documentRoot Description
 * @property string $configDir Description
 * 
 * @property string $logDir Description
 * @property string $binFile Description
 */
class LetsEncrypt {

    use \Nette\SmartObject;

    private $documentRoot;
    private $configDir;
    private $logDir;
    private $binFile;

    public function getDocumentRoot() {
        return $this->documentRoot;
    }

    public function getConfigDir() {
        return $this->configDir;
    }

    public function getLogDir() {
        return $this->logDir;
    }

    public function getBinFile() {
        return $this->binFile;
    }

    public function setDocumentRoot($documentRoot) {
        $this->documentRoot = $documentRoot;
    }

    public function setConfigDir($configDir) {
        $this->configDir = $configDir;
    }

    public function setLogDir($logDir) {
        $this->logDir = $logDir;
    }

    public function setBinFile($binFile) {
        $this->binFile = $binFile;
    }

}
