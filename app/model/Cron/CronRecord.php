<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Cron;

use Nette;

/**
 * Description of CronRecord
 *
 * @author Karel
 * @property string $minute Description
 * @property string $hour Description
 * @property string $day Description
 * @property string $month Description
 * @property string $dayOfWeek Description
 * @property string $user Description
 * @property string $command Description
 */
class CronRecord {

    use \Nette\SmartObject;

    private $minute;
    private $hour;
    private $day;
    private $month;
    private $dayOfWeek;
    private $user;
    private $command;

    function getMinute() {
        return $this->minute;
    }

    function getHour() {
        return $this->hour;
    }

    function getDay() {
        return $this->day;
    }

    function getMonth() {
        return $this->month;
    }

    function getDayOfWeek() {
        return $this->dayOfWeek;
    }

    function getUser() {
        return $this->user;
    }

    function getCommand() {
        return $this->command;
    }

    function setMinute($minute) {
        $this->minute = $minute;
    }

    function setHour($hour) {
        $this->hour = $hour;
    }

    function setDay($day) {
        $this->day = $day;
    }

    function setMonth($month) {
        $this->month = $month;
    }

    function setDayOfWeek($dayOfWeek) {
        $this->dayOfWeek = $dayOfWeek;
    }

    function setUser($user) {
        $this->user = $user;
    }

    function setCommand($command) {
        $this->command = $command;
    }

    public static function newCronRecord($minute = '*', $hour = '*', $day = '*', $month = '*', $dayOfWeek = '*', $user = 'root', $command = NULL) {
        $return = new static;
        $return->setMinute($minute);
        $return->setHour($hour);
        $return->setDay($day);
        $return->setMonth($month);
        $return->setDayOfWeek($dayOfWeek);
        $return->setUser($user);
        $return->setCommand($command);
        return $return;
    }

}
