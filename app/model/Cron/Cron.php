<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Cron;

use Nette;

/**
 * Description of Cron
 *
 * @author Karel
 * @property string $name Description
 * @property string $configDir Description
 */
class Cron {

    use \Nette\SmartObject;

    private $configDir;
    private $name;

    function getConfigDir() {
        return $this->configDir;
    }

    function getName() {
        return $this->name;
    }

    function setConfigDir($configDir) {
        $this->configDir = $configDir;
    }

    function setName($name) {
        $this->name = $name;
    }

}
