<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Storage;

use \SplFileInfo;
use Nette\Utils\FileSystem;
use NetteAddons\Utils\Image;
use App\Model\Objects\ArchiveRecord;

/**
 * Description of ImageStorage
 *
 * @author Karel
 */
class ImageStorage {

    private $directoryStorage = NULL;

    function setDirectoryStorage($directoryStorage) {
        $this->directoryStorage = $directoryStorage;
    }

    private function getTempFile($source, $storageName) {
        if ($source instanceof \Nette\Http\Url) {
            return new SplFileInfo($this->directoryStorage . DIRECTORY_SEPARATOR . 'temp' . DIRECTORY_SEPARATOR . $storageName . DIRECTORY_SEPARATOR . basename($source->path));
        }
    }

    public function getFullFile($storageName) {
        return new SplFileInfo($this->directoryStorage . DIRECTORY_SEPARATOR . 'live' . DIRECTORY_SEPARATOR . $storageName . DIRECTORY_SEPARATOR . 'full.jpg');
    }

    public function getThumbFile($storageName) {
        return new SplFileInfo($this->directoryStorage . DIRECTORY_SEPARATOR . 'live' . DIRECTORY_SEPARATOR . $storageName . DIRECTORY_SEPARATOR . 'thumb.jpg');
    }

    public function getArchiveFile($storageName, ArchiveRecord $archiveRecord) {
        return new SplFileInfo($this->directoryStorage . DIRECTORY_SEPARATOR . 'archive' . DIRECTORY_SEPARATOR . $storageName . DIRECTORY_SEPARATOR . $archiveRecord->datehour . '.jpg');
    }

    private static function convertImage(SplFileInfo $source, SplFileInfo $targetFull = NULL, SplFileInfo $targetResize = NULL, $width = 100, $height = 100) {
        $image = Image::fromFile((string) $source);
        if ($targetFull !== NULL) {
            FileSystem::createDir($targetFull->getPath());
            $image->save((string) $targetFull, NULL, Image::JPEG);
        }
        if ($targetResize !== NULL) {
            FileSystem::createDir($targetResize->getPath());
            $image->resize($width, $height, Image::SHRINK_ONLY);
            $image->save((string) $targetResize, NULL, Image::JPEG);
        }
    }

    public function insertImage(\Nette\Http\Url $url, $storageName, $widthThumb = 100, $heightThumb = 100, $hasArchive = FALSE, ArchiveRecord $archiveRecord = NULL, $widthArchive = 100, $heightArchive = 100) {
        \Tracy\Debugger::timer('insertImage');
        try {
            $temp = $this->getTempFile($url, $storageName);
            $full = $this->getFullFile($storageName);
            $thum = $this->getThumbFile($storageName);
            $arch = $this->getArchiveFile($storageName, $archiveRecord);
            FileSystem::copy($url->absoluteUrl, $temp);
            if ($temp->isFile()) {
                self::convertImage($temp, $full, $thum, $widthThumb, $heightThumb);
            }
            if ($temp->isFile() && $hasArchive && !$arch->isReadable()) {
                self::convertImage($temp, NULL, $arch, $widthArchive, $heightArchive);
                $archiveRecord->processed = TRUE;
            }
            return \Tracy\Debugger::timer('insertImage');
        } catch (\Exception $e) {
            $archiveRecord->processed = FALSE;
            return false;
        }
    }

    public function deleteImageArchive($storageName, ArchiveRecord $archiveRecord) {
        try {
            $arch = $this->getArchiveFile($storageName, $archiveRecord);
            FileSystem::delete($arch);
            $archiveRecord->processed = TRUE;
            return TRUE;
        } catch (\Exception $ex) {
            $archiveRecord->processed = FALSE;
            return false;
        }
    }

}
