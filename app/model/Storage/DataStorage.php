<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Storage;

use Nette\Utils\FileSystem;
use \NetteAddons\Utils\Image,
    \NetteAddons\Http\Url;
use \SplFileInfo;

/**
 * Description of ImagesArchive
 *
 * @author karel.novak
 */
class DataStorage {

    public static function copyFile(Url $source, SplFileInfo $target, $overwrite = FALSE) {
        if ($target->isFile() && !$overwrite) {
            return FALSE;
        }
        try {
            FileSystem::copy($source->absoluteUrl, $target);
            return true;
        } catch (\Exception $e) {
            FileSystem::delete($target);
            return false;
        }
    }

    public static function copyImage(Url $source, SplFileInfo $target, $overwrite = FALSE, $width = NULL, $height = NULL) {
        if ($target->isFile() && !$overwrite) {
            return FALSE;
        }
        try {
            $videoCameraImage = Image::fromFile($source->absoluteUrl);
            $videoCameraImage->resize($width, $height, Image::SHRINK_ONLY);
            FileSystem::createDir($target->getPath());
            $videoCameraImage->save((string) $target);
            return TRUE;
        } catch (\Exception $e) {
            FileSystem::delete($target);
            return FALSE;
        }
    }

    public static function deleteFile(SplFileInfo $target) {
        if (!$target->isFile()) {
            return FALSE;
        }
        try {
            FileSystem::delete($target);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

}
