<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\PresentersCli;

use Nette\Application\UI\Presenter,
    Nette\Application\IResponse,
    Nette\Utils\FileSystem;
use App\Model\Repository\VideoCamerasRepositoryLite,
    App\Model\Repository\ArchiveRepositoryLite,
    App\Model\Repository\MeteoStationsRepositoryLite,
    App\Model\Repository\CommitRepositoryLite,
    App\Model\Apache,
    App\Model\LetsEncrypt,
    App\Model\Mrtg,
    App\Model\Cron\Cron;
use App\Model\Repository\MenuRepository;

/**
 * Description of SetupPresenter
 *
 * @author karel.novak
 */
class SetupPresenter extends BasePresenter {

    /**
     * @var \App\Model\Repository\MenuRepository 
     * @inject
     */
    public $menuRepository;

    /**
     * @inject
     * @var VideoCamerasRepositoryLite 
     */
    public $videoCamerasRepositoryLite;

    /**
     * @inject
     * @var MeteoStationsRepositoryLite
     */
    public $meteoStationsRepositoryLite;

    /**
     * @var \App\Model\Apache 
     * @inject 
     */
    public $apache = NULL;

    /**
     * @var LetsEncrypt 
     * @inject 
     */
    public $letsEncrypt;

    /**
     * @var Mrtg 
     * @inject 
     */
    public $mrtg;

    /**
     * @var Cron 
     * @inject 
     */
    public $cron;

    public function renderCron($file, $restartService) {
        $target = $this->cron->configDir . DIRECTORY_SEPARATOR . $this->cron->name;
        $cronRecords[] = \App\Model\Cron\CronRecord::newCronRecord('*', '*', '*', '*', '*', 'root', 'php ' . $this->apache->documentRoot . '/index.php Cli:Cron:rrd');
        $cronRecords[] = \App\Model\Cron\CronRecord::newCronRecord('*', '*', '*', '*', '*', 'root', 'sleep 15; php ' . $this->apache->documentRoot . '/index.php Cli:Cron:images');
        $cronRecords[] = \App\Model\Cron\CronRecord::newCronRecord('*', '*', '*', '*', '*', 'root', 'sleep 30; php ' . $this->apache->documentRoot . '/index.php Cli:Cron:images');
        $cronRecords[] = \App\Model\Cron\CronRecord::newCronRecord('*', '*', '*', '*', '*', 'root', 'sleep 45; php ' . $this->apache->documentRoot . '/index.php Cli:Cron:images');
        $cronRecords[] = \App\Model\Cron\CronRecord::newCronRecord('*', '*', '*', '*', '*', 'root', 'sleep 60; php ' . $this->apache->documentRoot . '/index.php Cli:Cron:images');
        $cronRecords[] = \App\Model\Cron\CronRecord::newCronRecord('0', '6,9,12,15,18,21', '*', '*', '*', 'root', 'php ' . $this->apache->documentRoot . '/index.php Cli:Cron:archive');
        $cronRecords[] = \App\Model\Cron\CronRecord::newCronRecord('0', '3', '*', '*', '*', 'root', 'php ' . $this->apache->documentRoot . '/index.php Cli:Cron:cleanup');
        $cronRecords[] = \App\Model\Cron\CronRecord::newCronRecord('*/5', '*', '*', '*', '*', 'root', 'php ' . $this->apache->documentRoot . '/index.php Cli:Cron:commit');
        $cronRecords[] = \App\Model\Cron\CronRecord::newCronRecord('*/5', '*', '*', '*', '*', 'root', $this->mrtg->binFile . ' ' . $this->mrtg->configDir . DIRECTORY_SEPARATOR . $this->apache->serverName . 'MeteoStations.cfg --logging ' . $this->mrtg->logDir . DIRECTORY_SEPARATOR . $this->apache->serverName . 'MeteoStations.log');
        $cronRecords[] = \App\Model\Cron\CronRecord::newCronRecord('*/5', '*', '*', '*', '*', 'root', $this->mrtg->binFile . ' ' . $this->mrtg->configDir . DIRECTORY_SEPARATOR . $this->apache->serverName . 'VideoCameras.cfg --logging ' . $this->mrtg->logDir . DIRECTORY_SEPARATOR . $this->apache->serverName . 'VideoCameras.log');
        $this->template->cronRecords = $cronRecords;
        if ($file === TRUE) {
            $this->onShutdown[] = function (Presenter $sender, IResponse $response = null) use ($target) {
                \Nette\Utils\FileSystem::write($target, $response->getSource(), 0644);
            };
        }
        if ($restartService === TRUE) {
            $this->onShutdown[] = function (Presenter $sender, IResponse $response = null) use ($target) {
                exec('service cron restart');
            };
        }
    }

    public function renderApache2($file, $restartService) {
        $this->template->apache = $this->apache;

        $this->template->letsEncrypt = $this->letsEncrypt;

        $this->template->videoCameras = $this->videoCamerasRepositoryLite->getVideoCameras();
        $this->template->meteoStations = $this->meteoStationsRepositoryLite->getMeteoStations();
        $target = $this->apache->configDir . DIRECTORY_SEPARATOR . $this->apache->serverName . '.conf';
        if ($file === TRUE) {
            $this->onShutdown[] = function (Presenter $sender, IResponse $response = null) use ($target) {
                \Nette\Utils\FileSystem::write($target, $response->getSource(), 0644);
            };
        }
        if ($restartService === TRUE) {
            $this->onShutdown[] = function (Presenter $sender, IResponse $response = null) use ($target) {
                exec('service apache2 restart');
            };
        }
    }

    public function renderLetsEncrypt($file, $restartService, $dryRun) {
        $this->template->apache = $this->apache;
        $this->template->letsEncrypt = $this->letsEncrypt;
        $target = $this->letsEncrypt->configDir . DIRECTORY_SEPARATOR . 'cli.ini';
        if ($file === TRUE) {
            $this->onShutdown[] = function (Presenter $sender, IResponse $response = null) use ($target) {
                \Nette\Utils\FileSystem::write($target, $response->getSource(), 0644);
            };
        }
        if ($restartService === TRUE) {
            if ($restartService === TRUE) {
                $this->onShutdown[] = function (Presenter $sender, IResponse $response = null) use ($target) {
                    exec($this->letsEncrypt->binFile . ' certonly --authenticator webroot --installer apache -d "' . $this->apache->serverName . '" -m "' . $this->apache->serverAdmin . '" --webroot-path ' . $this->letsEncrypt->documentRoot . ' --agree-tos --dry-run');
                };
            } else {
                $this->onShutdown[] = function (Presenter $sender, IResponse $response = null) use ($target) {
                    exec($this->letsEncrypt->binFile . ' certonly --authenticator webroot --installer apache -d "' . $this->apache->serverName . '" -m "' . $this->apache->serverAdmin . '" --webroot-path ' . $this->letsEncrypt->documentRoot . ' --agree-tos --dry-run');
                };
            }
        }
    }

    public function renderSiteMap($file) {
        $this->template->getLatte()->addProvider('uiControl', $this->linkGenerator);

        $this->template->sitemap = $sitemap = $this->menuRepository->getMenu();

        $videoCameras = $this->videoCamerasRepositoryLite->getVideoCameras(FALSE);
        MenuRepository::addVideoCamerasMenu($sitemap, $videoCameras);

        $meteoStations = $this->meteoStationsRepositoryLite->getMeteoStations(FALSE);
        MenuRepository::addMeteoStationsMenu($sitemap, $meteoStations);

        $target = WWW_DIR . DIRECTORY_SEPARATOR . 'sitemap.xml';
        if ($file === TRUE) {
            $this->onShutdown[] = function (Presenter $sender, IResponse $response = null) use ($target) {
                \Nette\Utils\FileSystem::write($target, $response->getSource(), 0644);
            };
        }
    }

    public function renderRobots($file) {
        $this->template->robots = $robots = $this->menuRepository->getMenu();

        $this->template->sitemap = $this->linkGenerator->link('Cli:Setup:siteMap');

        $videoCameras = $this->videoCamerasRepositoryLite->getVideoCameras(FALSE);
        MenuRepository::addVideoCamerasMenu($robots, $videoCameras);

        $meteoStations = $this->meteoStationsRepositoryLite->getMeteoStations(FALSE);
        MenuRepository::addMeteoStationsMenu($robots, $meteoStations);

        $target = WWW_DIR . DIRECTORY_SEPARATOR . 'robots.txt';
        if ($file === TRUE) {
            $this->onShutdown[] = function (Presenter $sender, IResponse $response = null) use ($target) {
                \Nette\Utils\FileSystem::write($target, $response->getSource(), 0644);
            };
        }
    }

    public function renderDatabase() {
        
    }

}
