<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\PresentersCli;

use Tracy\Debugger;
use App\Model\Repository\VideoCamerasRepositoryLite,
    App\Model\Repository\ArchiveRepositoryLite,
    App\Model\Repository\MeteoStationsRepositoryLite,
    App\Model\Repository\CommitRepositoryLite,
    App\Model\RepositoryRrd\MeteoStationsRepositoryRrd,
    App\Model\Apache,
    App\Model\Mrtg;
use App\Model\Storage\ImageStorage;
use App\Model\Archive,
    App\Model\Data\Loader,
    App\Model\Storage\ImagesArchive;
use \NetteAddons\Http\Url;
use \SplFileInfo;
use App\Model\Objects\ArchiveRecord;

/**
 * Description of CronPresenter
 *
 * @author karel.novak
 */
class CronPresenter extends BasePresenter {

    /**
     * @inject 
     * @var ArchiveRepositoryLite 
     */
    public $archiveRepositoryLite;

    /**
     * @inject 
     * @var VideoCamerasRepositoryLite 
     */
    public $videoCamerasRepositoryLite;

    /**
     * @inject 
     * @var MeteoStationsRepositoryLite 
     */
    public $meteoStationsRepositoryLite;

    /**
     * @inject 
     * @var MeteoStationsRepositoryRrd 
     */
    public $meteoStationsRepositoryRrd;

    /**
     *
     * @var CommitRepositoryLite
     * @inject  
     */
    public $commitRepositoryLite;

    /**
     *
     * @var ImageStorage
     * @inject  
     */
    public $imageStorage;

    /**
     * @var Apache 
     * @inject 
     */
    public $apache;

    /**
     * @var Mrtg 
     * @inject 
     */
    public $mrtg;

    public function actionRrd() {
        $meteoStations = $this->meteoStationsRepositoryLite->getMeteoStations(FALSE);
        foreach ($meteoStations as $meteoStation) {
            try {
                $this->meteoStationsRepositoryRrd->setDevice($meteoStation);
                if ($this->meteoStationsRepositoryRrd->isUpdated()) {
                    Debugger::dump('SKIP ' . $meteoStation->name);
                    continue;
                }
                Debugger::dump('UPDATE ' . $meteoStation->name);
                $sourceUrl = new \SplFileInfo('http' . (($this->apache->SSL) ? 's' : '') . '://' . $this->apache->serverName . $this->link(':MeteoStations:xml', $meteoStation->name, time()));
                $data = Loader::getWeather($meteoStation->rrdDataDriver, $sourceUrl);
                $this->meteoStationsRepositoryRrd->updateList($meteoStation, $data);
            } catch (\Exception $e) {
                Debugger::dump('FAILUPDATE ' . $meteoStation->name . ' by reason :' . $e->getMessage());
            }
        }
        $this->terminate();
    }

    public function actionImages() {
        $videoCameras = $this->videoCamerasRepositoryLite->getVideoCameras(FALSE);
        foreach ($videoCameras as $videoCamera) {
            $sourceUrl = new Url('http' . (($this->apache->SSL) ? 's' : '') . '://' . $this->apache->serverName . $this->link(':VideoCameras:video', $videoCamera->name, time()));
            $archiveRecord = ArchiveRecord::newArchiveRecord($videoCamera, new \Nette\Utils\DateTime());
            if (($timer = $this->imageStorage->insertImage($sourceUrl, $videoCamera->name, $videoCamera->previewMaxWidth, $videoCamera->previewMaxHeight, $videoCamera->hasArchive, $archiveRecord, $videoCamera->archiveMaxWidth, $videoCamera->archiveMaxHeight))) {
                $this->archiveRepositoryLite->insertArchive($archiveRecord);
                $this->archiveRepositoryLite->expireArchives($archiveRecord);
                Debugger::dump('COPY OK ' . $sourceUrl . ' takes: ' . round($timer * 1000) . 'ms');
            } else {
                Debugger::dump('COPY FAIL ' . $sourceUrl);
            }
        }
        $this->terminate();
    }

    public function actionCommit() {
        if (($committed = $this->commitRepositoryLite->getCommitted()) > 0) {
            Debugger::timer();
            Debugger::log($committed . ' records waiting for write');
            exec('service cron stop');
            Debugger::log('Service cron stoped !');
            exec('php ' . $this->apache->documentRoot . '/index.php Cli:Setup:apache2 --file --restartService');
            Debugger::log('Apache 2 restarted !');
            exec('php ' . $this->apache->documentRoot . '/index.php Cli:SetupMrtg:meteoStations --file  --restartService');
            Debugger::log('MRTG MeteoStations restarted !');
            exec('php ' . $this->apache->documentRoot . '/index.php Cli:SetupMrtg:videoCameras  --file  --restartService');
            Debugger::log('MRTG VideoCameras restarted !');
            exec('php ' . $this->apache->documentRoot . '/index.php Cli:SetupRrd:createDatabase');
            Debugger::log('RRD MeteoStations Created !');
            exec('php ' . $this->apache->documentRoot . '/index.php Cli:Setup:siteMap --file');
            Debugger::log('SiteMap Has Been Written');
            exec('php ' . $this->apache->documentRoot . '/index.php Cli:Setup:robots --file');
            Debugger::log('Robots Has Been Writte');
            exec('service cron start');
            Debugger::log('Service cron started');
            $this->commitRepositoryLite->finishCommit();
            Debugger::log($committed . ' records has been writen and this action takes :' . Debugger::timer() . 's');
        }
        $this->terminate();
    }

    public function actionCleanup() {
        $videoCameras = $this->videoCamerasRepositoryLite->getVideoCameras();
        foreach ($videoCameras as $videoCamera) {
            $archiveRecordFinder = ArchiveRecord::newArchiveRecord($videoCamera, new \Nette\Utils\DateTime());
            $archive = $this->archiveRepositoryLite->getArchiveExpired($archiveRecordFinder);
            foreach ($archive as $archiveRecord) {
                if ($this->imageStorage->deleteImageArchive($videoCamera->name, $archiveRecord)) {
                    Debugger::dump('CLEANUP OK ');
                } else {
                    Debugger::dump('CLEANUP FAIL ');
                }
            }
            $this->terminate();
        }
    }

}
