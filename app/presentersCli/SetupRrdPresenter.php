<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\PresentersCli;

use App\Model\Repository\VideoCamerasRepositoryLite,
    App\Model\Repository\MeteoStationsRepositoryLite;
use App\Model\RepositoryRrd\Create\MeteoStationsRepositoryRrd,
    App\Model\RepositoryRrd\Create\VideoCamerasRepositoryRrd;
use Tracy\Debugger;

/**
 * Description of SetupRrd
 *
 * @author Karel
 */
class SetupRrdPresenter extends BasePresenter {

    /**
     * @inject 
     * @var VideoCamerasRepositoryLite 
     */
    public $videoCamerasRepositoryLite;

    /**
     * @inject 
     * @var VideoCamerasRepositoryRrd 
     */
    public $videoCamerasRepositoryRrd;

    /**
     * @inject 
     * @var MeteoStationsRepositoryLite 
     */
    public $meteoStationsRepositoryLite;

    /**
     * @inject 
     * @var MeteoStationsRepositoryRrd 
     */
    public $meteoStationsRepositoryRrd;

    public function renderCreateDatabase($overWriteFile = FALSE) {
        $meteoStations = $this->meteoStationsRepositoryLite->getMeteoStations(FALSE);
        foreach ($meteoStations as $meteoStation) {
            try {
                $this->meteoStationsRepositoryRrd->create($meteoStation, ($overWriteFile === TRUE));
                Debugger::dump('CREATED ' . $meteoStation->name);
            } catch (\Exception $e) {
                Debugger::dump('FAIL ' . $meteoStation->name . ' by reason :' . $e->getMessage());
            }
        }

        $videoCameras = $this->videoCamerasRepositoryLite->getVideoCameras(FALSE);
        foreach ($videoCameras as $videoCamera) {
            try {
                $this->videoCamerasRepositoryRrd->create($videoCamera, ($overWriteFile === TRUE));
                Debugger::dump('CREATED ' . $videoCamera->name);
            } catch (\Exception $e) {
                Debugger::dump('FAIL ' . $videoCamera->name . ' by reason :' . $e->getMessage());
            }
        }
        $this->terminate();
    }

}
