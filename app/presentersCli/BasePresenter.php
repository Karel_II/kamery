<?php

namespace App\PresentersCli;

use Nette;

/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter {

    /**
     * 
     * @var \Nette\Application\LinkGenerator 
     * @inject
     */
    public $linkGenerator;

    /**
     *
     * @var \NetteRunLock\Run\Lock
     * @inject 
     */
    public $lock;

    function startup() {
        if (PHP_SAPI !== 'cli') {
            throw new \Exception('This must be run in console mode !');
        }
        $this->lock->setLock($this->presenter);
        parent::startup();
    }

}
