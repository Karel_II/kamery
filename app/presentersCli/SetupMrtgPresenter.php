<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\PresentersCli;

use Nette\Application\UI\Presenter,
    Nette\Application\IResponse,
    Nette\Utils\FileSystem;

/**
 * Description of SetupMrtgPresenter
 *
 * @author karel.novak
 */
class SetupMrtgPresenter extends BasePresenter {

    /**
     * @var \App\Model\Mrtg 
     * @inject 
     */
    public $mrtg = NULL;

    /**
     * @var \App\Model\Apache 
     * @inject 
     */
    public $apache = NULL;

    /**
     * @inject
     * @var \App\Model\Repository\VideoCamerasRepositoryLite 
     */
    public $videoCamerasRepositoryLite = NULL;

    /**
     * @inject
     * @var \App\Model\Repository\MeteoStationsRepositoryLite
     */
    public $meteoStationsRepositoryLite = NULL;

    public function renderVideoCameras($file, $restartService) {
        $this->template->mrtg = $this->mrtg;
        $this->template->videoCameras = $this->videoCamerasRepositoryLite->getVideoCameras();
        $target = $this->mrtg->configDir . DIRECTORY_SEPARATOR . $this->apache->serverName . 'VideoCameras.cfg';
        if ($file === TRUE) {
            $this->onShutdown[] = function (Presenter $sender, IResponse $response = null) use ($target) {
                $context = iconv('UTF-8', 'windows-1250//TRANSLIT', $response->getSource());
                FileSystem::write($target, $context, 0644);
            };
        }
        if ($restartService === TRUE) {
            $this->onShutdown[] = function (Presenter $sender, IResponse $response = null) use ($target) {
                exec($this->mrtg->binFile . ' ' . $target . ' --debug=fork,time,snpo --logging ' . $this->mrtg->logDir . DIRECTORY_SEPARATOR . $this->apache->serverName . 'VideoCameras.log');
            };
        }
    }

    public function renderMeteoStations($file, $restartService) {

        $this->template->mrtg = $this->mrtg;
        $this->template->meteoStations = $this->meteoStationsRepositoryLite->getMeteoStations();
        $target = $this->mrtg->configDir . DIRECTORY_SEPARATOR . $this->apache->serverName . 'MeteoStations.cfg';
        if ($file === TRUE) {
            $this->onShutdown[] = function (Presenter $sender, IResponse $response = null) use ($target) {
                $context = iconv('UTF-8', 'windows-1250//TRANSLIT', $response->getSource());
                FileSystem::write($target, $context, 0644);
            };
        }
        if ($restartService === TRUE) {
            $this->onShutdown[] = function (Presenter $sender, IResponse $response = null) use ($target) {
                exec($this->mrtg->binFile . ' ' . $target . ' --debug=fork,time,snpo --logging ' . $this->mrtg->logDir . DIRECTORY_SEPARATOR . $this->apache->serverName . 'MeteoStations.log');
            };
        }
    }

}
