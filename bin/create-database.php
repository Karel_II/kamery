<?php

if (!isset($_SERVER['argv'][0])) {
    echo '
Create database.

Usage: create-database.php
';
    exit(1);
}



$container = require __DIR__ . '/../app/bootstrap.php';

$archiveRepositoryLite = $container->getByType(App\Model\Repository\Create\ArchiveRepositoryLite::class);
$meteoStationsRepositoryLite = $container->getByType(App\Model\Repository\Create\MeteoStationsRepositoryLite::class);
$videoCamerasRepositoryLite = $container->getByType(App\Model\Repository\Create\VideoCamerasRepositoryLite::class);
$commitRepositoryLite = $container->getByType(App\Model\Repository\Create\CommitRepositoryLite::class);


try {
    $archiveRepositoryLite->createTable();
    $meteoStationsRepositoryLite->createTable();
    $videoCamerasRepositoryLite->createTable();
    $commitRepositoryLite->createTable();

    echo "Data tables created \n";
} catch (\Exception $e) {
    echo "Error: create data tables.\n";
    exit(1);
}

