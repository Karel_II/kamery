<?php

if (!isset($_SERVER['argv'][3])) {
    echo '
Add new user to database.

Usage: create-user.php <name> <e-mail> <password> [<role>]
';
    exit(1);
}

list(, $username, $email, $password, $role) = $_SERVER['argv'];

$container = require __DIR__ . '/../app/bootstrap.php';

$repositoryResourcesCreate = $container->getByType(NetteAddons\Repository\Create\ResourcesRepository::class);
$repositoryRolesCreate = $container->getByType(NetteAddons\Repository\Create\RolesRepository::class);
$repositoryUsersCreate = $container->getByType(NetteAddons\Repository\Create\UsersRepository::class);
$repositoryVisitorsCreate = $container->getByType(NetteAddons\Repository\Create\VisitorsRepository::class);

$archiveRepositoryLite = $container->getByType(App\Model\Repository\Create\ArchiveRepositoryLite::class);
$meteoStationsRepositoryLite = $container->getByType(App\Model\Repository\Create\MeteoStationsRepositoryLite::class);
$videoCamerasRepositoryLite = $container->getByType(App\Model\Repository\Create\VideoCamerasRepositoryLite::class);
$commitRepositoryLite = $container->getByType(App\Model\Repository\Create\CommitRepositoryLite::class);


try {
    $repositoryResourcesCreate->createTable();
    $repositoryRolesCreate->createTable();
    $repositoryUsersCreate->createTable();
    $repositoryVisitorsCreate->createTable();

    echo "Securtity tables created \n";
} catch (\Exception $e) {
    echo "Error: create security tables.\n";
    exit(1);
}

//try {
$archiveRepositoryLite->createTable();
$meteoStationsRepositoryLite->createTable();
$videoCamerasRepositoryLite->createTable();
$commitRepositoryLite->createTable();

//    echo "Data tables created \n";
//} catch (\Exception $e) {
//    echo "Error: create data tables.\n";
//    exit(1);
//}


/** @var NetteAddons\Repository\UsersRepository $manager */
$manager = $container->getByType(NetteAddons\Repository\UsersRepository::class);

try {
    $manager->addUser($username, $email, $password, $role);
    echo "User $name was added.\n";
} catch (App\Model\DuplicateNameException $e) {
    echo "Error: duplicate name.\n";
    exit(1);
}
