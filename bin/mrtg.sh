#/bin/bash

php /srv/www/kamery2/www/index.php Cli:Setup:mrtgMeteoStations --file
php /srv/www/kamery2/www/index.php Cli:Setup:mrtgVideoCameras  --file

mrtg /etc/mrtg/kamery2MeteoStations.cfg --debug=fork,time,snpo
mrtg /etc/mrtg/kamery2VideoCameras.cfg --debug=fork,time,snpo
