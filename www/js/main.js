$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $.nette.init();
});

function updatePage(targetUrl, refreshImage) {
    $.ajax({
        url: targetUrl,
        type: "POST",
        data: {
            refreshImageId: refreshImage.id,
        },
        context: document.body
    }).done(function (payload) {
        for (var idSnippets in payload.snippets) {
            $("#" + idSnippets).html(payload.snippets[idSnippets]);
        }
        for (var refreshImageId in payload.refreshImageId) {
            refreshImage = $("#" + refreshImageId);
            if (refreshImage.attr('data-src-refresh') !== undefined) {
                source = refreshImage.data('srcRefresh');
                refreshImage.attr('src', source);
            }
        }
        for (var idTime in payload.time) {
            console.log(idTime);
            objectTime = $("#" + idTime);
            if (objectTime.attr('data-src-base') !== undefined) {
                source = objectTime.data('srcBase') + '?time=' + payload.time[idTime];
                objectTime.attr('src', source);
            }


        }
    });
}